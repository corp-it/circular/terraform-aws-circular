resource "aws_sns_topic" "developer_notification" {
  name = "developer-notification"
}

resource "aws_sns_topic_subscription" "user_updates_sqs_target" {
  topic_arn = aws_sns_topic.developer_notification.arn
  protocol  = "email"
  endpoint  = "desarrollo@appcircular.com"
}
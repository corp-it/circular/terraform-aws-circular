
module "database_logs_requests" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "DatabaseLogs"
  name    = "all-requests-count"
  pattern = "\"*\"" 
  metric_transformation_name  = "All database calls"
  metric_transformation_namespace = "LAMP"
}

module "database_logs_requests-duration" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "DatabaseLogs"
  name    = "all-requests-duration"
  pattern = "{$.datetime != \"\"}" 
  metric_transformation_name  = "Database call duration"
  metric_transformation_namespace = "LAMP"
  metric_transformation_value = "$.runtime"
}

module "database_logs_requests-affected" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "DatabaseLogs"
  name    = "all-requests-affected"
  pattern = "{$.datetime != \"\"}" 
  metric_transformation_name  = "Database call affected rows"
  metric_transformation_namespace = "LAMP"
  metric_transformation_value = "$.affected"
}



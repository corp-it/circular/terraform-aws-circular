
variable "plant_location_ids" {
    type = list(object({ name=string, locationId=number }))
}
variable "workspace_name_prefix" {
}
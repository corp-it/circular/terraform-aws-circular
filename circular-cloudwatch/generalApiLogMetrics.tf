
module "apiLogs_requests" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  name    = "all-requests-count"
  pattern = "\"*\"" 
  metric_transformation_name  = "All api calls"
  metric_transformation_namespace = "LAMP"
  metric_transformation_default_value = 0
}

module "apiLogs_requests-duration" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  name    = "all-requests-duration"
  pattern = "{$.datetime != \"\"}" 
  metric_transformation_name  = "Api call duration"
  metric_transformation_namespace = "LAMP"
  metric_transformation_value = "$.duration"
}

module "apiLogs_requests_forbidden" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  name    = "forbidden-requests"
  pattern = "{$.response.body.type = \"FORBIDDEN\"}" 
  metric_transformation_name  = "Forbidden api calls"
  metric_transformation_namespace = "LAMP"
  metric_transformation_default_value = 0
}

module "apiLogs_requests_forbidden_alarm" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  alarm_name          = "apiLogs_requests_forbidden_alarm"
  alarm_description   = "Intentos de acceder a datos prohibidos"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  threshold           = 1
  period              = 60
  namespace   = "LAMP"
  metric_name = "Forbidden api calls"
  statistic   = "Sum"

  alarm_actions = [aws_sns_topic.developer_notification.arn]
}


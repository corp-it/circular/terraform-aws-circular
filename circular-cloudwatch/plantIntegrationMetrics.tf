
module "plantIntegration_200" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  for_each = {for plant in var.plant_location_ids : plant.locationId => plant}
  name    = "plantIntegration-200-${each.value.name}-metric"
  pattern = "{($.url = \"*plantIntegration*\") && ($.authorizedId = ${each.value.locationId}) && ($.responseCode >= 200)  && ($.responseCode < 300)}" 
  metric_transformation_name  = "200-${each.value.name}"
  metric_transformation_namespace = "PlantIntegration"
  metric_transformation_default_value = 0
}
module "plantIntegration_300" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  for_each = {for plant in var.plant_location_ids : plant.locationId => plant}
  name    = "plantIntegration-300-${each.value.name}-metric"
  pattern = "{($.url = \"*plantIntegration*\") && ($.authorizedId = ${each.value.locationId}) && ($.responseCode >= 300)  && ($.responseCode < 400)}" 
  metric_transformation_name  = "300-${each.value.name}"
  metric_transformation_namespace = "PlantIntegration"
  metric_transformation_default_value = 0
}
module "plantIntegration_400" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  for_each = {for plant in var.plant_location_ids : plant.locationId => plant}
  name    = "plantIntegration-400-${each.value.name}-metric"
  pattern = "{($.url = \"*plantIntegration*\") && ($.authorizedId = ${each.value.locationId}) && ($.responseCode >= 400)  && ($.responseCode < 500)}" 
  metric_transformation_name  = "400-${each.value.name}"
  metric_transformation_namespace = "PlantIntegration"
  metric_transformation_default_value = 0
}
module "plantIntegration_500" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  for_each = {for plant in var.plant_location_ids : plant.locationId => plant}
  name    = "plantIntegration-500-${each.value.name}-metric"
  pattern = "{($.url = \"*plantIntegration*\") && ($.authorizedId = ${each.value.locationId}) && ($.responseCode >= 500)  && ($.responseCode < 600)}" 
  metric_transformation_name  = "500-${each.value.name}"
  metric_transformation_namespace = "PlantIntegration"
  metric_transformation_default_value = 0
}
module "plantIntegration_all_requests" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/log-metric-filter"  
  log_group_name = "APILogs"
  for_each = {for plant in var.plant_location_ids : plant.locationId => plant}
  name    = "plantIntegration-all-${each.value.name}-metric"
  pattern = "{($.url = \"*plantIntegration*\") && ($.authorizedId = ${each.value.locationId})}" 
  metric_transformation_name  = "all-requests-${each.value.name}"
  metric_transformation_namespace = "PlantIntegration"
  metric_transformation_default_value = 0
}
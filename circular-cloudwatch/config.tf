terraform {
  required_version = ">= 0.12.25"
  backend "remote" {
    organization = "Circular-qa"
    workspaces {
      prefix = "circular-cloudwatch-"
    }
  }
}

provider "aws" {
  region  = local.region
  # region  = "us-east-1"
  # profile = "tfcloud-dev"
  # profile = "tfcloud-prod" 
}


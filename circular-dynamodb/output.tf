

output "Updates-Doc-Rto-Ruta_id" {
  value = "${module.Updates-Doc-Rto-Ruta.id}"
}

output "STOP-updates_id" {
  value = "${module.STOP-updates.id}"
}

output "Data-Extraction-Results_id" {
  value = "${module.Data-Extraction-Results.id}"
}

####################################ID

output "conversationsTable" {
  value = "${module.circular-chat-conversations.id}"
}

output "messagesTable" {
  value = "${module.circular-chat-messages.id}"
}

output "userConversationsTable" {
  value = "${module.circular-chat-userConversations.id}"
}

output "usersTable" {
  value = "${module.circular-chat-users.id}"
}

output "GeofenceEvents" {
  value = "${module.Circular-Geofence-Events.id}"
}

#####################################Stream Arn

output "conversationsTable_stream" {
  value = "${module.circular-chat-conversations.stream_arn}"
}

output "messagesTable_stream" {
  value = "${module.circular-chat-messages.stream_arn}"
}

output "userConversationsTable_stream" {
  value = "${module.circular-chat-userConversations.stream_arn}"
}

output "usersTable_stream" {
  value = "${module.circular-chat-users.stream_arn}"
}

##################Arn

output "conversationsTable_arn" {
  value = "${module.circular-chat-conversations.arn}"
}

output "messagesTable_arn" {
  value = "${module.circular-chat-messages.arn}"
}

output "userConversationsTable_arn" {
  value = "${module.circular-chat-userConversations.arn}"
}

output "usersTable_arn" {
  value = "${module.circular-chat-users.arn}"
}


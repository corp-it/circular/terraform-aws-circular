

###############################

module "Updates-Doc-Rto-Ruta" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params-02["name"]}"
  billing_mode     = "${var.dynamodb_params-02["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params-02["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params-02["write_capacity"]}"
  hash_key         = "${var.dynamodb_params-02["hash_key"]}"
  attribute        = "${var.attribute-02}"
}

################################

module "STOP-updates" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params-03["name"]}"
  billing_mode     = "${var.dynamodb_params-03["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params-03["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params-03["write_capacity"]}"
  hash_key         = "${var.dynamodb_params-03["hash_key"]}"
  range_key        = "${var.dynamodb_params-03["range_key"]}"
  stream_enabled   = "${var.dynamodb_params-03["stream_enabled"]}"
  stream_view_type = "${var.dynamodb_params-03["stream_view_type"]}"
  attribute        = "${var.attribute-03}"
  global_index_name               = "${var.global_index_params-03["global_index_name"]}"
  global_index_hash_key           = "${var.global_index_params-03["global_index_hash_key"]}"
  global_index_range_key          = "${var.global_index_params-03["global_index_range_key"]}"
  global_index_read_capacity      = "${var.global_index_params-03["global_index_read_capacity"]}"
  global_index_write_capacity     = "${var.global_index_params-03["global_index_write_capacity"]}"
  global_index_projection_type    = "${var.global_index_params-03["global_index_projection_type"]}"
  has_global_secondary_index      = "true"
}

################################

module "Data-Extraction-Results" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params-04["name"]}"
  billing_mode     = "${var.dynamodb_params-04["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params-04["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params-04["write_capacity"]}"
  hash_key         = "${var.dynamodb_params-04["hash_key"]}"
  stream_enabled   = "${var.dynamodb_params-04["stream_enabled"]}"
  stream_view_type = "${var.dynamodb_params-04["stream_view_type"]}"
  attribute        = "${var.attribute-04}"
}

####################################

module "Circular-Geofence-Events" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params_05["name"]}"
  billing_mode     = "${var.dynamodb_params_05["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params_05["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params_05["write_capacity"]}"
  hash_key         = "${var.dynamodb_params_05["hash_key"]}"
  range_key        = "${var.dynamodb_params_05["range_key"]}"
  attribute        = "${var.attribute_05}"
}

##################################

module "circular-chat-conversations" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params_06["name"]}"
  billing_mode     = "${var.dynamodb_params_06["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params_06["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params_06["write_capacity"]}"
  hash_key         = "${var.dynamodb_params_06["hash_key"]}"
  attribute        = "${var.attribute_06}"
}

##################################

module "circular-chat-messages" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params_07["name"]}"
  billing_mode     = "${var.dynamodb_params_07["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params_07["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params_07["write_capacity"]}"
  hash_key         = "${var.dynamodb_params_07["hash_key"]}"
  range_key        = "${var.dynamodb_params_07["range_key"]}"
  stream_enabled   = "${var.dynamodb_params_07["stream_enabled"]}"
  stream_view_type = "${var.dynamodb_params_07["stream_view_type"]}"
  attribute        = "${var.attribute_07}"
  global_index_name               = "${var.global_index_params_07["global_index_name"]}"
  global_index_hash_key           = "${var.global_index_params_07["global_index_hash_key"]}"
  global_index_range_key          = "${var.global_index_params_07["global_index_range_key"]}"
  global_index_read_capacity      = "${var.global_index_params_07["global_index_read_capacity"]}"
  global_index_write_capacity     = "${var.global_index_params_07["global_index_write_capacity"]}"
  global_index_projection_type    = "${var.global_index_params_07["global_index_projection_type"]}"
  has_global_secondary_index      = "true"
}

##################################

module "circular-chat-userConversations" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params_08["name"]}"
  billing_mode     = "${var.dynamodb_params_08["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params_08["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params_08["write_capacity"]}"
  hash_key         = "${var.dynamodb_params_08["hash_key"]}"
  range_key        = "${var.dynamodb_params_08["range_key"]}"
  stream_enabled   = "${var.dynamodb_params_08["stream_enabled"]}"
  stream_view_type = "${var.dynamodb_params_08["stream_view_type"]}"
  attribute        = "${var.attribute_08}"
  global_index_name               = "${var.global_index_params_08["global_index_name"]}"
  global_index_hash_key           = "${var.global_index_params_08["global_index_hash_key"]}"
  global_index_read_capacity      = "${var.global_index_params_08["global_index_read_capacity"]}"
  global_index_write_capacity     = "${var.global_index_params_08["global_index_write_capacity"]}"
  global_index_projection_type    = "${var.global_index_params_08["global_index_projection_type"]}"
  has_global_secondary_index      = "true"
}

##################################

module "circular-chat-users" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params_09["name"]}"
  billing_mode     = "${var.dynamodb_params_09["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params_09["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params_09["write_capacity"]}"
  hash_key         = "${var.dynamodb_params_09["hash_key"]}"
  attribute        = "${var.attribute_09}"
}
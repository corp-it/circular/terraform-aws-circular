terraform {
    required_version = ">= 0.11.14"
    backend "remote" {
        organization = "Circular-qa"
        workspaces {
            prefix = "circular-dynamodb-"
        }
    }    
}

provider "aws" {
    #region = "${local.region}"
    region  = "us-east-1"
    profile = "tfcloud-dev"
    version = "v2.70.0"
}

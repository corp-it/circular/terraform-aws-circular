########################################Updates-Doc-Rto-Ruta

variable "dynamodb_params-02" {
  type    = "map"
  default = {
     name             = "Updates-Doc-Rto-Ruta"
     billing_mode     = "PAY_PER_REQUEST"
     read_capacity    = 2
     write_capacity   = 2
     hash_key         = "plates"
  }
}

variable "attribute-02" {
  type    = "list"
  default = [
    {
      name = "plates"
      type = "S"
    }
  ]
}

######################################## STOP-updates

variable "dynamodb_params-03" {
  type    = "map"
  default = {
     name           = "STOP-updates"
     billing_mode   = "PAY_PER_REQUEST"
     read_capacity  = 0
     write_capacity = 0
     hash_key       = "idCupo"
     range_key      = "modificado"
     stream_enabled   = "true"
     stream_view_type = "NEW_AND_OLD_IMAGES"
  }
}

variable "attribute-03" {
  type    = "list"
  default = [
    {
      name = "idCupo"
      type = "N"
    }, {
      name = "fecha"
      type = "S"
    },
      {
      name = "modificado"
      type = "S"
    }
  ]
}

variable "global_index_params-03" {
  type    = "map"
  default = {
    global_index_name               = "fecha-modificado-index"
    global_index_hash_key           = "fecha"
    global_index_range_key          = "modificado"
    global_index_read_capacity      = 0
    global_index_write_capacity     = 0
    global_index_projection_type    = "ALL"
  } 
}

######################################## Data-Extraction-Results

variable "dynamodb_params-04" {
  type    = "map"
  default = {
     name           = "Data-Extraction-Results"
     billing_mode   = "PAY_PER_REQUEST"
     read_capacity  = 0
     write_capacity = 0
     hash_key       = "documentId"
     stream_enabled   = "true"
     stream_view_type = "NEW_AND_OLD_IMAGES"
  }
}

variable "attribute-04" {
  type    = "list"
  default = [
    {
      name = "documentId"
      type = "S"
    }
  ]
}
###################################Circular-Geofence-Events

variable "dynamodb_params_05" {
  type    = "map"
  default = {
     name           = "Circular-Geofence-Events"
     billing_mode   = "PAY_PER_REQUEST"
     read_capacity  = 5
     write_capacity = 5
     hash_key       = "geofence"
     range_key      = "locatedDate"
  }
}

variable "attribute_05" {
  type    = "list"
  default = [
    {
      name = "geofence"
      type = "S"
    },
    {
      name = "locatedDate"
      type = "S"
    }, 
  ]
}

###################################circular-chat-conversations

variable "dynamodb_params_06" {
  type    = "map"
  default = {
     name           = "circular-chat-conversations"
     billing_mode   = "PAY_PER_REQUEST"
     read_capacity  = 5
     write_capacity = 5
     hash_key       = "id"
  }
}

variable "attribute_06" {
  type    = "list"
  default = [
    {
      name = "id"
      type = "S"
    },
  ]
}

########################################circular-chat-messages

variable "dynamodb_params_07" {
  type    = "map"
  default = {
     name           = "circular-chat-messages"
     billing_mode   = "PAY_PER_REQUEST"
     read_capacity  = 0
     write_capacity = 0
     hash_key       = "conversationId"
     range_key      = "createdAt"
     stream_enabled   = "true"
     stream_view_type = "NEW_AND_OLD_IMAGES"
  }
}

variable "attribute_07" {
  type    = "list"
  default = [
    {
      name = "conversationId"
      type = "S"
    }, {
      name = "createdAt"
      type = "S"
    },
      {
      name = "dateHour"
      type = "S"
    }
  ]
}

variable "global_index_params_07" {
  type    = "map"
  default = {
    global_index_name               = "dateHour-createdAt-index"
    global_index_hash_key           = "dateHour"
    global_index_range_key          = "createdAt"
    global_index_read_capacity      = 0
    global_index_write_capacity     = 0
    global_index_projection_type    = "ALL"
  } 
}

########################################circular-chat-userConversations

variable "dynamodb_params_08" {
  type    = "map"
  default = {
     name           = "circular-chat-userConversations"
     billing_mode   = "PAY_PER_REQUEST"
     read_capacity  = 0
     write_capacity = 0
     hash_key       = "userId"
     range_key      = "conversationId"
     stream_enabled   = "true"
     stream_view_type = "NEW_AND_OLD_IMAGES"
  }
}

variable "attribute_08" {
  type    = "list"
  default = [
    {
      name = "conversationId"
      type = "S"
    }, {
      name = "userId"
      type = "S"
    }
  ]
}

variable "global_index_params_08" {
  type    = "map"
  default = {
    global_index_name               = "conversationId-index"
    global_index_hash_key           = "conversationId"
    global_index_read_capacity      = 0
    global_index_write_capacity     = 0
    global_index_projection_type    = "ALL"
  } 
}

###################################circular-chat-users

variable "dynamodb_params_09" {
  type    = "map"
  default = {
     name           = "circular-chat-users"
     billing_mode   = "PAY_PER_REQUEST"
     read_capacity  = 5
     write_capacity = 5
     hash_key       = "cognitoId"
  }
}

variable "attribute_09" {
  type    = "list"
  default = [
    {
      name = "cognitoId"
      type = "S"
    },
  ]
}

output "asg_sg" {
  value = "${module.sg_asg_circular.this_security_group_id}"
}

output "nlb_arn" {
  value = "${module.circular-alb.arn}"
}

output "wildcard_cert_id"{
  value = "${aws_iam_server_certificate.circular_certificate.id}"
}

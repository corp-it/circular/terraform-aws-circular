##################SG_ASG

variable sg_asg_circular_params {
    type = "map"
    default = {
      name = "asg-sg-dev"
      description = "security group for autoscaling group"
    }
}

variable sg_asg_circular_cidr {
    type = "map"
    default = {
      ingress_cidr_blocks = ["0.0.0.0/0"]
   ##   ingress_cidr_blocks = ["181.45.75.229/32","181.230.132.242/32","190.16.55.228/32", "24.232.63.187/32"]
      ingress_rules = ["http-80-tcp","https-443-tcp"] 
      egress_cidr_blocks = ["0.0.0.0/0"]
      egress_rules = ["all-all"]
    }
}


###########################################alb for corpit

##variable sg_alb_params {
##    type = "map"
##    default = {
##      name        = "aplication_load_balancer_sg"
##      description = "security group for aplication load balancer"
##    }
##}
##
##variable sg_alb_cidr {
##    type = "map"
##    default = {
##      ingress_cidr_blocks = ["181.45.75.229/32"]
##      ingress_rules = ["http-80-tcp","https-443-tcp"] 
##    }
##}


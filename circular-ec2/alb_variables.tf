#############################################Ciarcular

variable http_tcp_listener {
    type = "list"
    default = [{ 
        port = "80", 
        protocol = "TCP"
    }]
        
}

variable http_tcp_listeners_count {
    default = "1"
}

## Depende de los Listeners definidos como Locals.
variable https_listeners_count {
    default = "1"
}


variable target_groups {
    type = "list"
    default = [{
        name = "PUBLIC-HTTP-TARGET-GROUP" , 
        backend_port = "80",
        backend_protocol = "TCP",
        deregistration_delay = "0"
      },
      {
        name = "PUBLIC-HTTPS-TARGET-GROUP",
        backend_port = "443",
        backend_protocol = "TCP",
        deregistration_delay = "0"
      },
    ]
}

variable target_groups_count {
    default = "1"
}

variable alb_tags {
    default = {}
}


###############################################Corpit
variable asg_params {
    default = {
      name = "circular-app-asg"
      health_check_type = "EC2"
      health_check_grace_period = "120"
      min_size                  = 1
      max_size                  = 4
      desired_capacity          = 2
      wait_for_capacity_timeout = "30m"
    }
}

variable lc_params {
    default = {
      lc_name         = "circular-asg-lc"
      image_id        = "ami-096fb08fdc2dc0ab0"
      instance_type   = "t2.small"
      key_name        = "aws_circular_ssh_internal"
    }
}

variable asg_tags {
    default = [
      {
        "key"                   = "Name"
        "value"                 = "circular-app-instance"
        "propagate_at_launch"   = "true"
      },
    ]
}
#####################################################Ciarcular


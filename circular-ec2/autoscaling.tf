data "template_cloudinit_config" "userdata_config" {
    part {
        content_type = "text/x-shellscript"
        content      = "yum install -y nginx && service nginx start"
    }
}
   ##  data.terraform_remote_state.corpit_bastion.bastion_sg,
locals {
  circular_asg_sg = "${list(
    data.terraform_remote_state.circular_bastion.bastion_sg,
    module.sg_asg_circular.this_security_group_id
  )}"
}

module "circular_asg" {
  source    = "corpit-consulting-public/autoscaling/aws"
  version   = "v2.9.2"

  name   = "Circular"

  # Launch configuration
  lc_name         = "${var.lc_params["lc_name"]}"
  image_id        = "${var.image_id}" 
  instance_type   = "${var.instance_type}"
  key_name        = "${var.key_name}"
  security_groups = "${local.circular_asg_sg}"
  user_data       = "${data.template_cloudinit_config.userdata_config.rendered}"

  # Auto scaling group
  asg_name                  = "${var.asg_params["name"]}"
  vpc_zone_identifier       = "${data.terraform_remote_state.circular_vpc.vpc_private_subnets}"
  health_check_type         = "${var.health_check_type}"
  health_check_grace_period = "${var.asg_params["health_check_grace_period"]}"
  min_size                  = "${var.asg_params["min_size"]}"
  max_size                  = "${var.asg_params["max_size"]}"
  desired_capacity          = "${var.asg_params["desired_capacity"]}"
  wait_for_capacity_timeout = "${var.asg_params["wait_for_capacity_timeout"]}"
  iam_instance_profile      = "${aws_iam_instance_profile.jenkins-instance-profile.arn}"

  target_group_arns = "${module.circular-alb.target_group_arns}"
  tags = "${concat(var.asg_tags,var.asg_global_tags)}"

}


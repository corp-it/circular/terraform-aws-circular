
################role policy for Jenkins

resource "aws_iam_role" "jenkins-instance-role" {
  name = "Jenkins-Instance-Role"
  assume_role_policy = "${data.aws_iam_policy_document.jenkins-instance-role-policy.json}"
}

data "aws_iam_policy_document" "jenkins-instance-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "ec2.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  } 
}

##########################

resource "aws_iam_role_policy_attachment" "jenkins-instance-role-policy" {
  role       = "${aws_iam_role.jenkins-instance-role.name}"
  policy_arn = "${aws_iam_policy.Jenkins-Instance-policy.arn}"
}

resource "aws_iam_policy" "Jenkins-Instance-policy" {
  name    = "Jenkins-Instance-policy"
  policy  = "${data.aws_iam_policy_document.Jenkins-Instance-policy.json}"
}

data "aws_iam_policy_document" "Jenkins-Instance-policy" {
  statement {
    effect  = "Allow"
    actions = [
            "autoscaling:Describe*",
            "cloudformation:Describe*",
            "cloudformation:GetTemplate",
            "s3:Get*",
            "events:*"
    ]
  resources = [
      "*"
  ]
 }
}

############################################Lambda Invoke

resource "aws_iam_role_policy_attachment" "jenkins-instance-role-invoke-policy" {
  role       = "${aws_iam_role.jenkins-instance-role.name}"
  policy_arn = "${aws_iam_policy.Jenkins_Invoke_Lambda_policy.arn}"
}

resource "aws_iam_policy" "Jenkins_Invoke_Lambda_policy" {
  name    = "Jenkins_Invoke_Lambda_policy"
  policy  = "${data.aws_iam_policy_document.Jenkins_Invoke_Lambda_policy.json}"
}

data "aws_iam_policy_document" "Jenkins_Invoke_Lambda_policy" {
  statement {
    effect  = "Allow"
    actions = [
            "lambda:InvokeFunction",
    ]
  resources = [
      "*"
  ]
 } 
}

############################################

resource "aws_iam_instance_profile" "jenkins-instance-profile" {
  name = "JenkinsCodeDeploy-InstanceRoleInstanceProfile"
  role = "${aws_iam_role.jenkins-instance-role.name}"
}

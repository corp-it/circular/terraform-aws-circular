variable global_tags {
    default = {
      "Owner" = "Cliente"
    }
}

variable asg_global_tags {
    default = [
      {
        "key" = "Owner"
        "value" = "Cliente"
        "propagate_at_launch" = "true"
      },
    ]
}

variable "workspace_name_prefix" {
}

variable "circular_cert_body" {
}

variable "circular_cert_key" {
}

variable "circular_cert_chain" {
}

variable "image_id" {
}

variable "instance_type" {
}

variable "key_name" {
}

variable "health_check_type" {
}

variable "ingress_cidr_blocks" {
  type = "list"
}

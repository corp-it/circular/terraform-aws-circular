resource "aws_iam_server_certificate" "circular_certificate" {
  name_prefix       = "CIRCULAR-CERTIFICATE-ALB"
  certificate_body  = "${var.circular_cert_body}"
  certificate_chain = "${var.circular_cert_chain}"
  private_key       = "${var.circular_cert_key}"

  lifecycle {
    create_before_destroy = true
  }
}

locals {
  https_listeners   = [{
    certificate_arn = "${aws_iam_server_certificate.circular_certificate.arn}",
    port            = 443
    protocol        = "TLS"
  }]

  extra_ssl_certs   = [{
    certificate_arn = "${aws_iam_server_certificate.circular_certificate.arn}",
    index                 = 0
    https_listener_index  = 0
  }]

}

############################################Circular alb
module "circular-alb" {
  source                        = "corpit-consulting-public/elb/aws"
  version                       = "v3.12.2"
  load_balancer_type            = "network"
  logging_enabled               = "false"
  load_balancer_name            = "circular-alb"
  subnets                       = "${data.terraform_remote_state.circular_vpc.vpc_public_subnets}"
  tags                          = "${merge(var.alb_tags,var.global_tags)}"
  vpc_id                        = "${data.terraform_remote_state.circular_vpc.vpc_id}"
  https_listeners               = "${local.https_listeners}"
  https_listeners_count         = "${var.https_listeners_count}"
  http_tcp_listeners            = "${var.http_tcp_listener}"
  http_tcp_listeners_count      = "${var.http_tcp_listeners_count}"
  target_groups                 = "${var.target_groups}"
  target_groups_count           = "${var.target_groups_count}"
  extra_ssl_certs               = "${local.extra_ssl_certs}"
  extra_ssl_certs_count         = 1
  enable_cross_zone_load_balancing = "true"
}

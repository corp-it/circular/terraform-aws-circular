module "sg_asg_circular" {
  source                = "corpit-consulting-public/security-group/aws"
  version               = "v2.17.0"
  name                 = "${var.sg_asg_circular_params["name"]}"
  use_name_prefix      = "true"
  description          = "${var.sg_asg_circular_params["description"]}"
  vpc_id               = "${data.terraform_remote_state.circular_vpc.vpc_id}"
  ingress_cidr_blocks  = "${concat(var.ingress_cidr_blocks,data.terraform_remote_state.circular_vpc.vpc_public_subnets_cidr_blocks)}"
  ingress_rules        = "${var.sg_asg_circular_cidr["ingress_rules"]}"
  egress_cidr_blocks   = "${var.sg_asg_circular_cidr["egress_cidr_blocks"]}"
  egress_rules         = "${var.sg_asg_circular_cidr["egress_rules"]}"
}

###################SG for Aplication Load Balancer

##module "sg_alb_http" {
##  source                                        = "./modules/terraform-aws-security-group"
##  name                                          = "${var.sg_alb_params["name"]}"
##  description                                   = "${var.sg_alb_params["description"]}"
##  vpc_id                                        = "${data.terraform_remote_state.corpit_vpc.vpc_id}"
##  ingress_cidr_blocks                           = "${var.sg_alb_cidr["ingress_cidr_blocks"]}"
##  ingress_rules                                 = "${var.sg_alb_cidr["ingress_rules"]}"
##  computed_egress_with_source_security_group_id = [
##    {
##      source_security_group_id = "${element(flatten(module.asg_app.this_autoscaling_group_security_groups[0]),0)}"
##      rule = "http-80-tcp"
##    },
##    {
##      source_security_group_id = "${element(flatten(module.asg_app.this_autoscaling_group_security_groups[0]),0)}"
##      rule = "https-443-tcp"
##    }
##
##  ]
##  number_of_computed_egress_with_source_security_group_id = 2
##}


data "terraform_remote_state" "corpit_vpc" {
    backend = "s3"
    workspace = "${local.env}"
    config = {
       bucket = "tf-corpit-state"
       key    = "state"
       dynamodb_table = "tf-corpit-state-lock"
       region = "us-east-1"
       profile = "corpit"
       workspace_key_prefix = "corpit_vpc"
    }
}

data "terraform_remote_state" "corpit_lambda" {
    backend = "s3"
    workspace = "${local.env}"
    config = {
       bucket = "tf-corpit-state"
       key    = "state"
       dynamodb_table = "tf-corpit-state-lock"
       region = "us-east-1"
       profile = "corpit"
       workspace_key_prefix = "corpit_lambda"
    }
}

data "terraform_remote_state" "corpit_cognito" {
    backend = "s3"
    workspace = "${local.env}"
    config = {
       bucket = "tf-corpit-state"
       key    = "state"
       dynamodb_table = "tf-corpit-state-lock"
       region = "us-east-1"
       profile = "corpit"
       workspace_key_prefix = "corpit_cognito"
    }
}

data "terraform_remote_state" "corpit_ec2" {
    backend = "s3"
    workspace = "${local.env}"
    config = {
       bucket = "tf-corpit-state"
       key    = "state"
       dynamodb_table = "tf-corpit-state-lock"
       region = "us-east-1"
       profile = "corpit"
       workspace_key_prefix = "corpit_ec2"
    }
}


##################################Circular Actors API
module "circular_stage_actors" {
  source        = "corpit-consulting-public/api-gateway-stage/aws"
  version       = "v0.1.3"
  stage_name    = "${var.stage_name}"
  rest_api_id   = "${module.gw_circular_rest_api_Circular_Actors_API.id}"
  deployment_id = "${aws_api_gateway_deployment.circular_actors.id}"
  variables      {
    conversalabSiteUrl               = "${var.conversalabSiteUrl}"
    actorsApiLogLambda               = "${module.actorsApiLog.id}"
    getQuicksightDashboardUrlLambda  = "${module.getQuicksightDashboardUrl.id}"
    lnhDriverLambda                  = "${module.lnhDriver.id}"
    processGeofenceEventLambda       = "${module.processGeofenceEvent.id}"
    sendEventsAPIAdapterLambda       = "${module.sendEventsAPIAdapter.id}"
    vpcLinkId                        = "${data.terraform_remote_state.circular_api_gw.vpc_link}"
  }
}


 





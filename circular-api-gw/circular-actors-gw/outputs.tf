output "layer_Parse5_arn" {
  value = "${module.lambda_layer_circular_Parse5.layer_arn}"
}

output "layer_Parse5_version" {
  value = "${module.lambda_layer_circular_Parse5.version}"
}

output "layer_Uuid_arn" {
  value = "${module.lambda_layer_circular_Uuid.layer_arn}"
}

output "layer_Uuid_version" {
  value = "${module.lambda_layer_circular_Uuid.version}"
}

output "layer_AwsSdkJs_arn" {
  value = "${module.lambda_layer_circular_AwsSdkJs.layer_arn}"
}

output "layer_AwsSdkJs_version" {
  value = "${module.lambda_layer_circular_AwsSdkJs.version}"
}

output "layer_FirebaseAdmin_arn" {
  value = "${module.lambda_layer_circular_FirebaseAdmin.layer_arn}"
}

output "layer_FirebaseAdmin_version" {
  value = "${module.lambda_layer_circular_FirebaseAdmin.version}"
}

output "sendNotifications_id" {
  value = "${module.sendNotifications.id}"
}



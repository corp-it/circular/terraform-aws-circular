################role policy for cognito

resource "aws_iam_role" "Cognito_FacilitiesAdminAuth_Role" {
  name = "${var.facilitiesadminauth_role_name}"
  assume_role_policy = "${data.aws_iam_policy_document.Cognito-FacilitiesAdminAuth-assume-role-policy.json}"
}

data "aws_iam_policy_document" "Cognito-FacilitiesAdminAuth-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Federated"
      identifiers = [
        "cognito-identity.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRoleWithWebIdentity",
    ]

    condition {
      test = "StringEquals"
      variable = "cognito-identity.amazonaws.com:aud"
      values   = ["${data.terraform_remote_state.circular_cognito.identity-pool_id}"]
    }
    condition {
      test     = "ForAnyValue:StringLike"
      variable = "cognito-identity.amazonaws.com:amr"
      values   = ["authenticated"]
    }
  }
}

###############################################

resource "aws_iam_role_policy_attachment" "oneClick-Cognito-FacilitiesAdminAuth-Circular-role-policy" {
  role       = "${aws_iam_role.Cognito_FacilitiesAdminAuth_Role.name}"
  policy_arn = "${aws_iam_policy.oneClick-Cognito-FacilitiesAdminAuth-Circular-policy.arn}"
}

resource "aws_iam_policy" "oneClick-Cognito-FacilitiesAdminAuth-Circular-policy" {
  name    = "oneClick-Cognito-FacilitiesAdminAuth-Circular-policy"
  policy  = "${data.aws_iam_policy_document.oneClick-Cognito-FacilitiesAdminAuth-Circular-policy.json}"
}

data "aws_iam_policy_document" "oneClick-Cognito-FacilitiesAdminAuth-Circular-policy" {
  statement {
    effect  = "Allow"
    actions = [
            "mobileanalytics:PutEvents",
            "cognito-sync:*",
            "cognito-identity:*",
    ]
  resources = [
      "*"
  ]
 }
}

###########################################

resource "aws_iam_role_policy_attachment" "Cognito-QuickSightEmbed-role-policy" {
  role       = "${aws_iam_role.Cognito_FacilitiesAdminAuth_Role.name}"
  policy_arn = "${aws_iam_policy.Cognito-QuickSightEmbed-policy.arn}"
}

resource "aws_iam_policy" "Cognito-QuickSightEmbed-policy" {
  name    = "CognitoQuickSightEmbed-Circular-policy"
  policy  = "${data.aws_iam_policy_document.CognitoQuickSightEmbed-Circular-policy.json}"
}

data "aws_iam_policy_document" "CognitoQuickSightEmbed-Circular-policy" {
  statement {
    effect  = "Allow"
    actions = [
            "quicksight:RegisterUser",
    ]
    resources = [
      "*"
    ]
  },
  statement {
    effect  = "Allow"
    actions = [
            "quicksight:GetDashboardEmbedUrl",
    ]
    resources = [
      "*"
    ]
  },
  statement {
    effect  = "Allow"
    actions = [
            "sts:AssumeRole",
    ]
    resources = [
      "*"
    ]
  },
}


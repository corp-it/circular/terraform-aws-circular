################role policy for Dynamodb CRUD

resource "aws_iam_role" "lambdaS3DynamodbCRUD" {
  name = "lambda_DynamoCRUD_Circular_policy"
  assume_role_policy = "${data.aws_iam_policy_document.lambda-assume-role-policy.json}"
}

data "aws_iam_policy_document" "lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

################################################DynamoDB_Circular_policy

resource "aws_iam_role_policy_attachment" "DynamoCRUD-Circular-role-policy-attach" {
  role       = "${aws_iam_role.lambdaS3DynamodbCRUD.name}"
  policy_arn = "${aws_iam_policy.Circular-DynamoDB-policy.arn}"
}

resource "aws_iam_policy" "Circular-DynamoDB-policy" {
  name    = "DynamoDB-Circular-policy"
  policy  = "${data.aws_iam_policy_document.DynamoDB_Circular_policy.json}"
}

data "aws_iam_policy_document" "DynamoDB_Circular_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
        "dynamodb:BatchGetItem",
        "dynamodb:BatchWriteItem",
        "dynamodb:PutItem",
        "dynamodb:DeleteItem",
        "dynamodb:GetItem",
        "dynamodb:Scan",
        "dynamodb:Query",
        "dynamodb:UpdateItem"
    ]
  resources = [
      "*"
  ]
 }
}
##############Policy of AWS
resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole-role-policy-attach" {
  role       = "${aws_iam_role.lambdaS3DynamodbCRUD.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "AmazonS3FullAccess-role-policy-attach" {
  role       = "${aws_iam_role.lambdaS3DynamodbCRUD.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "AWSXrayFullAccess-role-policy-attach" {
  role       = "${aws_iam_role.lambdaS3DynamodbCRUD.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayFullAccess"
}

#############################################


################role policy for Circular-basic-execution-Lambda-Role

resource "aws_iam_role" "Circular-basic-execution-Lambda-Role" {
  name = "${var.lambda_basic_exe_role_name}"
  assume_role_policy = "${data.aws_iam_policy_document.basic-execution-Lambda-assume-role-policy.json}"
}

data "aws_iam_policy_document" "basic-execution-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

#######################################cognito-admin-create-user-attach

resource "aws_iam_role_policy_attachment" "cognito-admin-create-user-attach" {
  role       = "${aws_iam_role.Circular-basic-execution-Lambda-Role.name}"
  policy_arn = "${aws_iam_policy.Cognito-Admin-Create-User-policy.arn}"
}

#######################################Circular-LambdaInvoke-policy

resource "aws_iam_role_policy_attachment" "Circular-Lambda-Invoke" {
  role       = "${aws_iam_role.Circular-basic-execution-Lambda-Role.name}"
  policy_arn = "${aws_iam_policy.LambdaInvoke-policy.arn}"
}

#######################################Circular-oneClick-basic-execution-role-policy

resource "aws_iam_role_policy_attachment" "Circular-oneClick-basic-execution-role-policy" {
  role       = "${aws_iam_role.Circular-basic-execution-Lambda-Role.name}"
  policy_arn = "${aws_iam_policy.Circular-oneClick-basic-execution-policy.arn}"
}

resource "aws_iam_policy" "Circular-oneClick-basic-execution-policy" {
  name    = "${var.Policy_oneclick_name}"
  policy  = "${data.aws_iam_policy_document.Circular-oneClick-basic-execution-policy.json}"
}

data "aws_iam_policy_document" "Circular-oneClick-basic-execution-policy" {
  statement {
    effect  = "Allow"
    actions = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
    ]
  resources = [
      "arn:aws:logs:*:*:*"
  ]
 }
}

#################################sendEventsAPIAdapter

module "sendEventsAPIAdapter" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_1["function_name"]}"
  role               = "${aws_iam_role.AppSyncDynamoReader.arn}"
  memory_size        = "${var.lambda_params_1["memory_size"]}"
  handler            = "${var.lambda_params_1["handler"]}"
  runtime            = "${var.lambda_params_1["runtime"]}"
  timeout            = "${var.lambda_params_1["timeout"]}"
  mode               = "${var.lambda_params_1["mode"]}"
  s3_object_version  = "${var.s3_obj_sendEventsAPIAdapters}"
  s3_bucket          = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key             = "${var.lambda_params_1["s3_key"]}"
  variables          {
    sendNotificationsLambda  = "${module.sendNotifications.id}"
    UsersConversationsTable  = "${data.terraform_remote_state.circular_appsync.userConversationsTable}"
    UsersTable               = "${data.terraform_remote_state.circular_appsync.usersTable}"
  }
  has_variables      = "true"
}

#####################################processGeofenceEvent

module "processGeofenceEvent" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_2["function_name"]}"
  description        = "${var.lambda_params_2["description"]}"
  role               = "${aws_iam_role.AppSyncDynamoReader.arn}"
  memory_size        = "${var.lambda_params_2["memory_size"]}"
  handler            = "${var.lambda_params_2["handler"]}"
  runtime            = "${var.lambda_params_2["runtime"]}"
  timeout            = "${var.lambda_params_2["timeout"]}"
  mode               = "${var.lambda_params_2["mode"]}"
  s3_object_version  = "${var.s3_obj_processGeofenceEvent}"
  s3_bucket          = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key             = "${var.lambda_params_2["s3_key"]}"
  variables          {
    GeofenceEventsTable = "${data.terraform_remote_state.circular_appsync.GeofenceEvents}"
    UsersTable          = "${data.terraform_remote_state.circular_appsync.usersTable}"
  }
  has_variables      = "true"
}

#####################################lnhDriver

module "lnhDriver" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_3["function_name"]}"
  role               = "${aws_iam_role.Circular-basic-execution-Lambda-Role.arn}"
  description        = "${var.lambda_params_3["description"]}"
  memory_size        = "${var.lambda_params_3["memory_size"]}"
  handler            = "${var.lambda_params_3["handler"]}"
  runtime            = "${var.lambda_params_3["runtime"]}"
  timeout            = "${var.lambda_params_3["timeout"]}"
  mode               = "${var.lambda_params_3["mode"]}"
  s3_object_version  = "${var.s3_obj_lnhDriver}"
  s3_bucket          = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key             = "${var.lambda_params_3["s3_key"]}"
  variables          {
    getLnhCnrtLambda = "${data.terraform_remote_state.circular_lambda.getLnhCnrt_id}"
    getLintiLambda   = "${data.terraform_remote_state.circular_lambda.getLinti_id}"
  }
  has_variables      = "true"
}


####################################actorsApiLog

module "actorsApiLog" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_4["function_name"]}"
  role               = "${aws_iam_role.Circular-basic-execution-Lambda-Role.arn}"
  memory_size        = "${var.lambda_params_4["memory_size"]}"
  handler            = "${var.lambda_params_4["handler"]}"
  runtime            = "${var.lambda_params_4["runtime"]}"
  timeout            = "${var.lambda_params_4["timeout"]}"
  mode               = "${var.lambda_params_4["mode"]}"
  s3_object_version  = "${var.s3_obj_actorsApiLog}"
  s3_bucket          = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key             = "${var.lambda_params_4["s3_key"]}"
  has_variables      = "false"
}


####################################getQuicksightDashboardUrl

module "getQuicksightDashboardUrl" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_5["function_name"]}"
  role               = "${aws_iam_role.Circular-default-Lambda-Role.arn}"
  memory_size        = "${var.lambda_params_5["memory_size"]}"
  description        = "${var.lambda_params_5["description"]}"
  handler            = "${var.lambda_params_5["handler"]}"
  runtime            = "${var.lambda_params_5["runtime"]}"
  timeout            = "${var.lambda_params_5["timeout"]}"
  mode               = "${var.lambda_params_5["mode"]}"
  s3_object_version  = "${var.s3_obj_getQuicksightDashboardUrl}"
  s3_bucket          = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key             = "${var.lambda_params_5["s3_key"]}"
  has_variables      = "true"
  variables          {
    IamRoleArn         = "${aws_iam_role.Cognito_FacilitiesAdminAuth_Role.arn}"
    UserPoolId         = "${data.terraform_remote_state.circular_cognito.user_pool_id}"
    IdentityPoolId     = "${data.terraform_remote_state.circular_cognito.identity-pool_id}"
   # IdentityPoolId     = "us-east-1:c54ab302-97cd-48e5-b8fb-2bdef997250b"
  }
  layers             = ["${module.lambda_layer_circular_AwsSdkJs.layer_arn}:${module.lambda_layer_circular_AwsSdkJs.version}"]
}

####################################sendNotifications

module "sendNotifications" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_10["function_name"]}"
  role               = "${aws_iam_role.AppSyncDynamoReader.arn}"
  description        = "${var.lambda_params_10["description"]}"
  memory_size        = "${var.lambda_params_10["memory_size"]}"
  handler            = "${var.lambda_params_10["handler"]}"
  runtime            = "${var.lambda_params_10["runtime"]}"
  timeout            = "${var.lambda_params_10["timeout"]}"
  mode               = "${var.lambda_params_10["mode"]}"
  s3_object_version  = "${var.s3_obj_sendNotifications}"
  s3_bucket          = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key             = "${var.lambda_params_10["s3_key"]}"
  has_variables      = "true"
  variables          {
    FirebaseFile                   = "${var.FirebaseFile}"
    FirebaseUrl                    = "${var.FirebaseUrl}"
    NotificationWebLink            = "${var.NotificationWebLink}"
    deleteNotificationTokensLambda = "${module.deleteNotificationTokens.id}"
  }
  layers             = ["${module.lambda_layer_circular_FirebaseAdmin.layer_arn}:${module.lambda_layer_circular_FirebaseAdmin.version}"]
}

####################################deleteNotificationTokens

module "deleteNotificationTokens" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_11["function_name"]}"
  role               = "${aws_iam_role.AppSyncDynamoReader.arn}"
  description        = "${var.lambda_params_11["description"]}"
  memory_size        = "${var.lambda_params_11["memory_size"]}"
  handler            = "${var.lambda_params_11["handler"]}"
  runtime            = "${var.lambda_params_11["runtime"]}"
  timeout            = "${var.lambda_params_11["timeout"]}"
  mode               = "${var.lambda_params_11["mode"]}"
  s3_object_version  = "${var.s3_obj_deleteNotificationTokens}"
  s3_bucket          = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key             = "${var.lambda_params_11["s3_key"]}"
  has_variables      = "true"
  variables          {
    UsersTable  = "${data.terraform_remote_state.circular_appsync.usersTable}"
  }
}

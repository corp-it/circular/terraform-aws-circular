################################Circular Actors api

data template_file "api_gw_actors_swagger" {
  template = "${file("templates/api_gw_actors_swagger.tpl")}"
  vars {
    region                      = "${local.region}"
    user_pool_id                = "${data.terraform_remote_state.circular_cognito.user_pool_id}"
    account_id                  = "${data.aws_caller_identity.current.account_id}"
  }
}

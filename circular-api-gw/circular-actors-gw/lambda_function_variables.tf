############################################sendEventsAPIAdapter

variable "lambda_params_1" {
  type    = "map"
  default = {
    function_name     = "sendEventsAPIAdapter"
    memory_size       = "512"
    handler           = "index.handler"
    runtime           = "nodejs10.x"
    timeout           = "15"
    mode              = "PassThrough"
    s3_key            = "sendEventsAPIAdapter.zip"
  }
}

###################################processGeofenceEvent

variable "lambda_params_2" {
  type    = "map"
  default = {
    function_name     = "processGeofenceEvent"
    memory_size       = "256"
    handler           = "index.handler"
    description       = "Process a geofence event."
    runtime           = "nodejs10.x"
    timeout           = "5"
    mode              = "PassThrough"
    s3_key            = "processGeofenceEvent.zip"
  }
}

###################################lnhDriver

variable "lambda_params_3" {
  type    = "map"
  default = {
    function_name     = "lnhDriver"
    memory_size       = "1024"
    description       = "Validates if a truck driver has a license enabled"
    handler           = "index.handler"
    runtime           = "nodejs10.x"
    timeout           = "29"
    mode              = "PassThrough"
    s3_key            = "lnhDriver.zip"
  }
}

#################################################actorsApiLog

variable "lambda_params_4" {
  type    = "map"
  default = {
    function_name     = "actorsApiLog"
    memory_size       = "128"
    handler           = "index.handler"
    runtime           = "nodejs10.x"
    timeout           = "5"
    mode              = "PassThrough"
    s3_key            = "actorsApiLog.zip"
  }
}

#################################################getQuicksightDashboardUrl

variable "lambda_params_5" {
  type    = "map"
  default = {
    function_name     = "getQuicksightDashboardUrl"
    description       = "Get a Quicksight Dashboard Url to embed in the Web Planta."
    memory_size       = "256"
    handler           = "index.handler"
    runtime           = "nodejs10.x"
    timeout           = "10"
    mode              = "PassThrough"
    s3_key            = "getQuicksightDashboardUrl.zip"
  }
}

#################################################sendNotifications

variable "lambda_params_10" {
  type    = "map"
  default = {
    function_name     = "sendNotifications"
    description       = "Send notifications via Firebase"
    memory_size       = "512"
    handler           = "index.handler"
    runtime           = "nodejs10.x"
    timeout           = "10"
    mode              = "PassThrough"
    s3_key            = "sendNotifications.zip"
  }
}

#################################################deleteNotificationTokens

variable "lambda_params_11" {
  type    = "map"
  default = {
    function_name     = "deleteNotificationTokens"
    description       = "Delete the notification tokens for each of the informed users"
    memory_size       = "512"
    handler           = "index.handler"
    runtime           = "nodejs10.x"
    timeout           = "3"
    mode              = "PassThrough"
    s3_key            = "deleteNotificationTokens.zip"
  }
}


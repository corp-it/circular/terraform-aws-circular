data "terraform_remote_state" "circular_vpc" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-vpc-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "circular_s3" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-s3-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "circular_lambda" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-lambda-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "circular_api_gw" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-gw-api-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "circular_cognito" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-cognito-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "circular_appsync" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-appsync-${var.workspace_name_prefix}"
       }
    }
}


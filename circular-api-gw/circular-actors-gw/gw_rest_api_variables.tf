################################################Circular Actors API

variable "gw_params" {
  type    = "map"
  default = {
    name           = "Circular Actors API"
    api_key_source = "HEADER"
  }
}

variable "types" {
  type = "list"
  default = ["REGIONAL"]
}

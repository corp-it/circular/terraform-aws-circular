################role policy for AppSyncDynamoReader

resource "aws_iam_role" "AppSyncDynamoReader" {
  name = "lambda-AppSync-DynamoReader_policy"
  assume_role_policy = "${data.aws_iam_policy_document.lambda-AppSyncDynamoReader-assume-role-policy.json}"
}

data "aws_iam_policy_document" "lambda-AppSyncDynamoReader-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]

  }
}

######################LambdaMicroserviceExecutionRole

resource "aws_iam_role_policy_attachment" "LambdaMicroserviceExecution-role-policy-attach" {
  role       = "${aws_iam_role.AppSyncDynamoReader.name}"
  policy_arn = "${aws_iam_policy.DynamoDB-Circular-policy.arn}"
}

resource "aws_iam_policy" "DynamoDB-Circular-policy" {
  name    = "Lambda-Microservice-Execution-Role"
  policy  = "${data.aws_iam_policy_document.DynamoDB-fullAccess-Circular-policy.json}"
}

data "aws_iam_policy_document" "DynamoDB-fullAccess-Circular-policy" {
  statement {
    effect = "Allow"
    actions = [
             "dynamodb:DeleteItem",
             "dynamodb:GetItem",
             "dynamodb:PutItem",
             "dynamodb:Scan",
             "dynamodb:UpdateItem"
    ]
  resources = [
      "arn:aws:dynamodb:us-east-1:${data.aws_caller_identity.current.account_id}:table/*"
  ]
 }
}

######################LambdaBasicExecutionRole

resource "aws_iam_role_policy_attachment" "LambdaBasicExecutionRole-role-policy-attach" {
  role       = "${aws_iam_role.AppSyncDynamoReader.name}"
  policy_arn = "${aws_iam_policy.LambdaBasicExecutionRole-Circular-policy.arn}"
}

resource "aws_iam_policy" "LambdaBasicExecutionRole-Circular-policy" {
  name    = "LambdaBasic_ExecutionRole"
  policy  = "${data.aws_iam_policy_document.Circular-LambdaBasicExecutionRole-policy.json}"
}

data "aws_iam_policy_document" "Circular-LambdaBasicExecutionRole-policy" {
 statement {
   sid      = "VisualEditor1",
   effect   = "Allow",
   actions   = [
              "logs:CreateLogGroup",
   ]
   resources = [
            "*"
   ]
  },
 statement {
   sid      = "VisualEditor0",
   effect   = "Allow",
   actions  = [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
   ]
   resources = [
            "*"
   ]
  }
}

####################Policies of AWS

resource "aws_iam_role_policy_attachment" "AmazonDynamoDBFullAccess-role-policy-attach" {
  role       = "${aws_iam_role.AppSyncDynamoReader.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaRole-role-policy-attach" {
  role       = "${aws_iam_role.AppSyncDynamoReader.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}

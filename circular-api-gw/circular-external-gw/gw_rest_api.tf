#######################################################

module "gw_circular_rest_api_external_api" {
  source         = "corpit-consulting-public/api-gateway/aws"
  version        = "v0.1.1"
  name           = "${var.gw_params["name"]}"
  api_key_source = "${var.gw_params["api_key_source"]}"
  body           = "${data.template_file.api_gw_external_swagger.rendered}"
  types          = ["${var.types}"]
}

resource "aws_api_gateway_deployment" "circular_external" {
  rest_api_id = "${module.gw_circular_rest_api_external_api.id}"
}


############################################Circular External API

module "circular_external_stage" {
  source        = "corpit-consulting-public/api-gateway-stage/aws"
  version       = "v0.1.2"
  stage_name         = "${var.stage_name}"
  cache_cluster_size = "0.5"
  rest_api_id        = "${module.gw_circular_rest_api_external_api.id}"
  deployment_id      = "${aws_api_gateway_deployment.circular_external.id}"
  variables      {
    conversalabSiteUrl  = "${var.conversalabSiteUrl}"
    vpcLinkId           = "${data.terraform_remote_state.circular_api_gw.vpc_link}"
  } 
}



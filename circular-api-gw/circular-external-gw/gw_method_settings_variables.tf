###############################################Circular External API

variable "method_params" {
  type    = "map"
  default = {
    method_path                                = "*/*"
    metrics_enabled                            = "false" 
    logging_level                              = "INFO"
    data_trace_enabled                         = "true"
    throttling_burst_limit                     = "5000"
    throttling_rate_limit                      = "10000.0"
    caching_enabled                            = "false"
    cache_ttl_in_seconds                       = "300"
    cache_data_encrypted                       = "false"
    require_authorization_for_cache_control    = "true"
    unauthorized_cache_control_header_strategy = "SUCCEED_WITH_RESPONSE_HEADER"
  }
}


######################################Circular External API

module "circular_method_settings_external" {
  source                                     = "corpit-consulting-public/api-gateway-method-settings/aws"
  version                                    = "v0.1.2"
  rest_api_id                                = "${module.gw_circular_rest_api_external_api.id}"
  stage_name                                 = "${module.circular_external_stage.name}"
  method_path                                = "${var.method_params["method_path"]}"
    metrics_enabled                            = "${var.method_params["metrics_enabled"]}" 
    logging_level                              = "${var.method_params["logging_level"]}"
    data_trace_enabled                         = "${var.method_params["data_trace_enabled"]}"
    throttling_burst_limit                     = "${var.method_params["throttling_burst_limit"]}"
    throttling_rate_limit                      = "${var.method_params["throttling_rate_limit"]}"
    caching_enabled                            = "${var.method_params["caching_enabled"]}"
    cache_ttl_in_seconds                       = "${var.method_params["cache_ttl_in_seconds"]}"
    cache_data_encrypted                       = "${var.method_params["cache_data_encrypted"]}"
    require_authorization_for_cache_control    = "${var.method_params["require_authorization_for_cache_control"]}"
    unauthorized_cache_control_header_strategy = "${var.method_params["unauthorized_cache_control_header_strategy"]}"
}


#################################Circular External api

data template_file "api_gw_external_swagger" {
  template = "${file("templates/circular-swagger-template/gw_api_external_swagger.tpl")}"
  vars {
    region                      = "${local.region}"
    user_pool_id                = "${data.terraform_remote_state.circular_cognito.user_pool_id}"
    account_id                  = "${data.aws_caller_identity.current.account_id}"
  }
}



{
  "swagger": "2.0",
  "info": {
    "version": "2018-09-21T14:11:57Z",
    "title": "Circular External API"
  },
  "host": "2cvj4iy3h0.execute-api.${region}.amazonaws.com",
  "basePath": "/Prod",
  "schemes": [
    "https"
  ],
  "paths": {
    "/arrivals": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "arrivalsSchema",
            "required": true,
            "schema": {
              "$ref": "#/definitions/arrivalsSchema"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ],
        "x-amazon-apigateway-request-validator": "Validate body",
        "x-amazon-apigateway-integration": {
          "uri": "http://$${stageVariables.conversalabSiteUrl}/mionca/CargilArrival.php",
          "responses": {
            "default": {
              "statusCode": "200"
            }
          },
          "passthroughBehavior": "when_no_match",
          "connectionType": "VPC_LINK",
          "connectionId": "$${stageVariables.vpcLinkId}",
          "httpMethod": "POST",
          "requestTemplates": {
            "application/json": "{\n  \"userCuit\" : $input.json('$.userCuit'),\n  \"cpOwnerCuit\" : $input.json('$.cpOwnerCuit'),\n  \"cpOwnerBussinessName\" : $input.json('$.cpOwnerBussinessName'),\n  \"turnId\" : $input.json('$.turnId'),\n  \"shipmentId\" : $input.json('$.shipmentId'),\n  \"time\" : $input.json('$.time'),\n  \"plates\" : $input.json('$.plates'),\n  \"elevatorId\" : $input.json('$.elevatorId'),\n  \"badgeId\" : $input.json('$.badgeId'),\n  \"productId\" : $input.json('$.productId'),\n  \"turnoId\": $input.json('$.turnId')\n}"
          },
          "type": "http"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'POST,OPTIONS'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    },
    "/state-change": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "stateChangeSchema",
            "required": true,
            "schema": {
              "$ref": "#/definitions/stateChangeSchema"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ],
        "x-amazon-apigateway-request-validator": "Validate body",
        "x-amazon-apigateway-integration": {
          "uri": "http://$${stageVariables.conversalabSiteUrl}/mionca/CargilUpdateState.php",
          "responses": {
            "default": {
              "statusCode": "200"
            }
          },
          "passthroughBehavior": "when_no_match",
          "connectionType": "VPC_LINK",
          "connectionId": "$${stageVariables.vpcLinkId}",
          "httpMethod": "POST",
          "requestTemplates": {
            "application/json": "{\n    \"turnId\":  $input.json('$.turnId'),\n    \"plates\": $input.json('$.plates'),\n    \"badgeId\": $input.json('$.badgeId'),\n    \"plant\":  $input.json('$.plant'),\n    \"state\":  $input.json('$.state'),\n    \"turnoId\": $input.json('$.turnId')\n}"
          },
          "type": "http"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'POST,OPTIONS'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    },
    "/updates": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "updatesMethosSchema",
            "required": true,
            "schema": {
              "$ref": "#/definitions/updatesMethosSchema"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ],
        "x-amazon-apigateway-request-validator": "Validate body",
        "x-amazon-apigateway-integration": {
          "uri": "http://$${stageVariables.conversalabSiteUrl}/mionca/CargilAppointmentChanges.php",
          "responses": {
            "default": {
              "statusCode": "200"
            }
          },
          "passthroughBehavior": "when_no_match",
          "connectionType": "VPC_LINK",
          "connectionId": "$${stageVariables.vpcLinkId}",
          "httpMethod": "POST",
          "type": "http"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'POST,OPTIONS'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    }
  },
  "securityDefinitions": {
    "api_key": {
      "type": "apiKey",
      "name": "x-api-key",
      "in": "header"
    }
  },
  "definitions": {
    "stateChangeSchema": {
      "type": "object",
      "required": [
        "badgeId",
        "plant",
        "plates",
        "state",
        "turnId"
      ],
      "properties": {
        "turnId": {
          "type": "integer"
        },
        "plates": {
          "type": "string"
        },
        "badgeId": {
          "type": "integer"
        },
        "plant": {
          "type": "integer"
        },
        "state": {
          "type": "string"
        }
      }
    },
    "Empty": {
      "type": "object",
      "title": "Empty Schema"
    },
    "updatesMethosSchema": {
      "type": "object",
      "required": [
        "from"
      ],
      "properties": {
        "from": {
          "type": "integer"
        },
        "to": {
          "type": "integer"
        },
        "destinationIds": {
          "type": "array"
        }
      }
    },
    "arrivalsSchema": {
      "type": "object",
      "required": [
        "badgeId",
        "cpOwnerBussinessName",
        "cpOwnerCuit",
        "plates",
        "productId",
        "time",
        "turnId",
        "userCuit"
      ],
      "properties": {
        "userCuit": {
          "type": "string"
        },
        "cpOwnerCuit": {
          "type": "string"
        },
        "cpOwnerBussinessName": {
          "type": "string"
        },
        "turnId": {
          "type": "integer"
        },
        "shipmentId": {
          "type": "integer"
        },
        "time": {
          "type": "integer"
        },
        "plates": {
          "type": "string"
        },
        "elevatorId": {
          "type": "integer"
        },
        "badgeId": {
          "type": "integer"
        },
        "productId": {
          "type": "integer"
        }
      }
    }
  },
  "x-amazon-apigateway-request-validators": {
    "Validate body": {
      "validateRequestParameters": false,
      "validateRequestBody": true
    }
  }
}

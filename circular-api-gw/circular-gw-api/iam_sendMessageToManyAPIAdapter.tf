
################role policy for sendMessageToManyAPIAdapter

resource "aws_iam_role" "sendMessageToManyAPIAdapter" {
  name = "send_Message-To-Many-APIAdapter_Circular_policy"
  assume_role_policy = "${data.aws_iam_policy_document.sendMessageToManyAPIAdapter-assume-role-policy.json}"
}

data "aws_iam_policy_document" "sendMessageToManyAPIAdapter-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}
################################################LambdaBasicExecution-role-policy

resource "aws_iam_role_policy_attachment" "LambdaBasicExecution-role-policy" {
  role       = "${aws_iam_role.sendMessageToManyAPIAdapter.name}"
  policy_arn = "${aws_iam_policy.LambdaBasicExecution-policy.arn}"
}

resource "aws_iam_policy" "LambdaBasicExecution-policy" {
  name    = "lambdaBasic_executionCircular-policy"
  policy  = "${data.aws_iam_policy_document.lambda_basic-ExecutionRole_policy.json}"
}

data "aws_iam_policy_document" "lambda_basic-ExecutionRole_policy" {
  statement {
    effect = "Allow"
    actions = [
            "logs:CreateLogStream",
            "logs:PutLogEvents",
    ]
  resources = [
      "*"
  ]
 }
  statement {
    effect = "Allow"
    actions = [
            "logs:CreateLogGroup",
    ]
  resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*"
  ]
 }
}

################################################### lambdaInvoke

resource "aws_iam_role_policy_attachment" "lambdaInvoke-role-policy" {
  role       = "${aws_iam_role.sendMessageToManyAPIAdapter.name}"
  policy_arn = "${aws_iam_policy.LambdaInvoke-policy.arn}"
}

resource "aws_iam_policy" "LambdaInvoke-policy" {
  name    = "lambda_invokeCircular-policy"
  policy  = "${data.aws_iam_policy_document.lambda-Invoke_policy.json}"
}

data "aws_iam_policy_document" "lambda-Invoke_policy" {
  statement {
    sid     = "VisualEditor0"
    effect  = "Allow"
    actions = [
            "lambda:InvokeFunction",
    ]
  resources = [
      "*"
  ]
 }
}


{
  "swagger": "2.0",
  "info": {
    "version": "2019-07-23T19:05:19Z",
    "title": "${rest_api_name}, Test",
    "description":  "${rest_api_description}"
  },
  "host": "9w8jx32fii.execute-api.${region}.amazonaws.com",
  "basePath": "/Prod",
  "schemes": [
    "https"
  ],
  "paths": {
    "/generate-appointment-voucher": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "GenerateAppointmentVoucherModel",
            "required": true,
            "schema": {
              "$ref": "#/definitions/GenerateAppointmentVoucherModel"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ],
        "x-amazon-apigateway-request-validator": "Validate body",
        "x-amazon-apigateway-integration": {
          "uri": "arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:$${stageVariables.takeWebsiteScreenshotlambda}/invocations",
          "responses": {
            "default": {
              "statusCode": "200"
            },
            "missing": {
              "statusCode": "400"
            },
            "Error": {
              "statusCode": "500"
            }
          },
          "passthroughBehavior": "when_no_templates",
          "httpMethod": "POST",
          "requestTemplates": {
            "application/json": "#set($name =  'turno-'+$input.path('$.appointmentId')+'.png')\r\n#set($url =  $$stageVariables.conversalabPublicSiteUrl + 'GenerateAppointmentVoucher.php?turnId='+$input.path('$.appointmentId'))\r\n#set($endpoint =  $$stageVariables.contentEndpoint + '/comprobantes-turno/')\r\n#set($bucketLocation =  $$stageVariables.contentBucket + '/comprobantes-turno')\r\n{\r\n    \t\r\n      \"url\": \"$url\",\r\n      \"endpoint\": \"$endpoint\",\r\n      \"bucketLocation\": \"$bucketLocation\",\r\n      \"fileName\": \"$name\",\r\n      \"screenHeight\": 1090,\r\n      \"screenWidth\": 1090\r\n\r\n}"
          },
          "contentHandling": "CONVERT_TO_TEXT",
          "type": "aws"
        }
      }
    },
    "/generate-job-voucher": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "GenerateVoucherModel",
            "required": true,
            "schema": {
              "$ref": "#/definitions/GenerateVoucherModel"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "x-amazon-apigateway-request-validator": "Validate body",
        "x-amazon-apigateway-integration": {
          "uri": "arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:$${stageVariables.takeWebsiteScreenshotlambda}/invocations",
          "responses": {
            "missing.*": {
              "statusCode": "400"
            },
            "default": {
              "statusCode": "200"
            },
            "Error.*": {
              "statusCode": "500"
            }
          },
          "passthroughBehavior": "when_no_templates",
          "httpMethod": "POST",
          "requestTemplates": {
            "application/json": "#set($name =  'viaje-'+$input.path('$.jobId')+'.png')\r\n#set($url =  $$stageVariables.conversalabPublicSiteUrl + 'GenerateJobVoucher.php?jobId='+$input.path('$.jobId'))\r\n\r\n#set($endpoint =  $$stageVariables.contentEndpoint + '/ordenes-de-carga/')\r\n#set($bucketLocation =  $$stageVariables.contentBucket + '/ordenes-de-carga')\r\n{\r\n    \t\r\n      \"url\": \"$url\",\r\n      \"endpoint\": \"$endpoint\",\r\n      \"bucketLocation\": \"$bucketLocation\",\r\n      \"fileName\": \"$name\",\r\n      \"screenHeight\": 720,\r\n      \"screenWidth\": 900\r\n\r\n}"
          },
          "contentHandling": "CONVERT_TO_TEXT",
          "type": "aws"
        }
      }
    },
    "/generate-appointment-thanks": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "GenerateAppointmentVoucherModel",
            "required": true,
            "schema": {
              "$ref": "#/definitions/GenerateAppointmentVoucherModel"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "x-amazon-apigateway-request-validator": "Validate body",
        "x-amazon-apigateway-integration": {
          "uri": "arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:$${stageVariables.takeWebsiteScreenshotlambda}/invocations",
          "responses": {
            "missing.*": {
              "statusCode": "400"
            },
            "default": {
              "statusCode": "200"
            },
            "Error.*": {
              "statusCode": "500"
            }
          },
          "passthroughBehavior": "when_no_templates",
          "httpMethod": "POST",
          "requestTemplates": {
            "application/json": "#set($name =  'turno-'+$input.path('$.appointmentId')+'.png')\r\n#set($url =  $$stageVariables.conversalabPublicSiteUrl + 'GenerateAppointmentThanks.php?appointmentId='+$input.path('$.appointmentId'))\r\n\r\n#set($endpoint =  $$stageVariables.contentEndpoint + '/agradecimientos-turno/')\r\n#set($bucketLocation =  $$stageVariables.contentBucket + '/agradecimientos-turno')\r\n{\r\n    \t\r\n      \"url\": \"$url\",\r\n      \"endpoint\": \"$endpoint\",\r\n      \"bucketLocation\": \"$bucketLocation\",\r\n      \"fileName\": \"$name\",\r\n      \"screenHeight\": 859,\r\n      \"screenWidth\": 858\r\n\r\n}"
          },
          "contentHandling": "CONVERT_TO_TEXT",
          "type": "aws"
        }
      }
    },
    "/img-location": {
      "post": {
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          }
        },
        "security": [
          {
            "CircularUserPoolCognitoAuthorizer": []
          }
        ],
        "x-amazon-apigateway-integration": {
          "uri": "arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:$${stageVariables.uploadImageLocationlambda}/invocations",
          "responses": {
            "default": {
              "statusCode": "200"
            }
          },
          "passthroughBehavior": "when_no_match",
          "httpMethod": "POST",
          "contentHandling": "CONVERT_TO_TEXT",
          "type": "aws_proxy"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    },
    "/img-presigned-url": {
      "post": {
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          }
        },
        "security": [
          {
            "CircularUserPoolCognitoAuthorizer": []
          }
        ],
        "x-amazon-apigateway-integration": {
          "uri": "arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:$${stageVariables.s3temporaryPresignedURLlambda}/invocations",
          "responses": {
            "default": {
              "statusCode": "200"
            }
          },
          "passthroughBehavior": "when_no_match",
          "httpMethod": "POST",
          "contentHandling": "CONVERT_TO_TEXT",
          "type": "aws_proxy"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    },
    "/massive-message": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "CircularMassiveMessage",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CircularMassiveMessage"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ],
        "x-amazon-apigateway-request-validator": "Validate body",
        "x-amazon-apigateway-integration": {
          "uri": "arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:$${stageVariables.sendMessageToManyAPIAdapterlambda}/invocations",
          "responses": {
            "default": {
              "statusCode": "200"
            }
          },
          "passthroughBehavior": "when_no_match",
          "httpMethod": "POST",
          "contentHandling": "CONVERT_TO_TEXT",
          "type": "aws_proxy"
        }
      }
    },
    "/register-firebase-token": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "registerFirebaseToken",
            "required": true,
            "schema": {
              "$ref": "#/definitions/registerFirebaseToken"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "400 response",
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "500": {
            "description": "500 response",
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          }
        },
        "security": [
          {
            "CircularUserPoolCognitoAuthorizer": []
          }
        ],
        "x-amazon-apigateway-integration": {
          "uri": "arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:$${stageVariables.registerFirebaseTokenlambda}/invocations",
          "responses": {
            "4\\d{2}": {
              "statusCode": "400",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            },
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            },
            "5\\d{2}": {
              "statusCode": "500",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "httpMethod": "POST",
          "contentHandling": "CONVERT_TO_TEXT",
          "type": "aws"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'POST,OPTIONS'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    },
    "/shipments-for-user": {
      "post": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "400 response",
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "500": {
            "description": "500 response",
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          }
        },
        "security": [
          {
            "CircularUserPoolCognitoAuthorizer": []
          }
        ],
        "x-amazon-apigateway-integration": {
          "uri": "http://$${stageVariables.conversalabSiteUrl}/mionca/ShipmentsForDriver.php",
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_templates",
          "connectionType": "VPC_LINK",
          "connectionId": "$${stageVariables.vpcLinkId}",
          "httpMethod": "POST",
          "requestTemplates": {
            "application/json": "#set($inputBody =  $input.body)\r\n{\r\n    \"body\":$inputBody\r\n    ,\"cognitoId\": \"$context.authorizer.claims.get(\"custom:actorIntegrationId\")\"\r\n}"
          },
          "type": "http"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'POST,OPTIONS'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    },
    "/user-profile": {
      "get": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "400": {
            "description": "400 response"
          },
          "500": {
            "description": "500 response"
          }
        },
        "security": [
          {
            "CircularUserPoolCognitoAuthorizer": []
          }
        ],
        "x-amazon-apigateway-integration": {
          "uri": "http://$${stageVariables.conversalabSiteUrl}/mionca/UserProfile.php",
          "responses": {
            "default": {
              "statusCode": "200"
            }
          },
          "passthroughBehavior": "when_no_templates",
          "connectionType": "VPC_LINK",
          "connectionId": "$${stageVariables.vpcLinkId}",
          "httpMethod": "POST",
          "requestTemplates": {
            "application/json": "#set($inputBody =  $input.body)\r\n{\r\n    \"body\":$inputBody\r\n    ,\"cognitoId\": \"$context.authorizer.claims.get(\"custom:actorIntegrationId\")\"\t\r\n}"
          },
          "type": "http"
        }
      },
      "options": {
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/Empty"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              },
              "Access-Control-Allow-Methods": {
                "type": "string"
              },
              "Access-Control-Allow-Headers": {
                "type": "string"
              }
            }
          }
        },
        "x-amazon-apigateway-integration": {
          "responses": {
            "default": {
              "statusCode": "200",
              "responseParameters": {
                "method.response.header.Access-Control-Allow-Methods": "'GET,OPTIONS'",
                "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
                "method.response.header.Access-Control-Allow-Origin": "'*'"
              }
            }
          },
          "passthroughBehavior": "when_no_match",
          "requestTemplates": {
            "application/json": "{\"statusCode\": 200}"
          },
          "type": "mock"
        }
      }
    }
  },
  "securityDefinitions": {
    "api_key": {
      "type": "apiKey",
      "name": "x-api-key",
      "in": "header"
    },
    "CircularUserPoolCognitoAuthorizer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header",
      "x-amazon-apigateway-authtype": "cognito_user_pools",
      "x-amazon-apigateway-authorizer": {
        "providerARNs": [
          "arn:aws:cognito-idp:${region}:${account_id}:userpool/${user_pool_id}"
        ],
        "type": "cognito_user_pools"
      }
    }
  },
  "definitions": {
    "Empty": {
      "type": "object",
      "title": "Empty Schema"
    },
    "CircularMassiveMessage": {
      "type": "object",
      "required": [
        "conversations",
        "sender",
        "text"
      ],
      "properties": {
        "sender": {
          "type": "string"
        },
        "conversations": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "text": {
          "type": "string"
        }
      }
    },
    "registerFirebaseToken": {
      "type": "object",
      "required": [
        "channel",
        "token",
        "userId"
      ],
      "properties": {
        "userId": {
          "type": "string"
        },
        "token": {
          "type": "string"
        },
        "channel": {
          "type": "string"
        }
      }
    },
    "GenerateAppointmentVoucherModel": {
      "type": "object",
      "properties": {
        "name": {
          "type": "number"
        }
      }
    },
    "GenerateVoucherModel": {
      "type": "object",
      "properties": {
        "name": {
          "type": "number"
        }
      }
    }
  },
  "x-amazon-apigateway-gateway-responses": {
    "DEFAULT_5XX": {
      "responseParameters": {
        "gatewayresponse.header.Access-Control-Allow-Methods": "'POST,OPTIONS'",
        "gatewayresponse.header.Access-Control-Allow-Origin": "'*'",
        "gatewayresponse.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
      },
      "responseTemplates": {
        "application/json": "{\"message\":$context.error.messageString}"
      }
    },
    "DEFAULT_4XX": {
      "responseParameters": {
        "gatewayresponse.header.Access-Control-Allow-Methods": "'POST,OPTIONS'",
        "gatewayresponse.header.Access-Control-Allow-Origin": "'*'",
        "gatewayresponse.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
      },
      "responseTemplates": {
        "application/json": "{\"message\":$context.error.messageString}"
      }
    }
  },
  "x-amazon-apigateway-request-validators": {
    "Validate body": {
      "validateRequestParameters": false,
      "validateRequestBody": true
    }
  }
}


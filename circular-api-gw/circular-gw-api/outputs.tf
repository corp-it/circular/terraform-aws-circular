output "vpc_link" {
  value = "${aws_api_gateway_vpc_link.Circular-vpc-link.id}"
}

output "layer_Sharp_arn" {
  value = "${module.lambda_layer_circular_Sharp.layer_arn}"
}

output "layer_PhantomJsBinnaries_arn" {
  value = "${module.lambda_layer_circular_PhantomJsBinnaries.layer_arn}"
}

output "layer_Sharp_version" {
  value = "${module.lambda_layer_circular_Sharp.version}"
}

output "layer_PhantomJsBinnaries_version" {
  value = "${module.lambda_layer_circular_PhantomJsBinnaries.version}"
}

output "takeWebsiteScreenshot_id" {
  value = "${module.takeWebsiteScreenshot.id}"
}

output "gw_current_deployment_id" {
  value = "${module.gw_circular_rest_api.deployment_id}"
}

module "lambda_layer_circular_Sharp" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v0.1.4"
  layer_name          = "${var.layer_params["layer_name"]}"
  description         = "${var.layer_params["description"]}"
  s3_bucket           = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key              = "${var.layer_params["s3_key"]}"
  s3_object_version   = "${var.s3_obj_Sharp}"
  compatible_runtimes = ["${var.compatible_runtimes}"]
}

#######################Layer PhantomJsBinnaries

module "lambda_layer_circular_PhantomJsBinnaries" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v0.1.4"
  layer_name          = "${var.layer_params_2["layer_name"]}"
  description         = "${var.layer_params_2["description"]}"
  s3_bucket           = "${data.terraform_remote_state.circular_s3.s3_name}"
  s3_key              = "${var.layer_params_2["s3_key"]}"
  s3_object_version   = "${var.s3_obj_PhantomJsBinnaries}"
  compatible_runtimes = ["${var.compatible_runtimes_2}"]
}


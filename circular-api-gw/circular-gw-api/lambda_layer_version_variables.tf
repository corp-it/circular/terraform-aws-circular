variable "layer_params" {
  type    = "map"
  default = {
    layer_name  = "Sharp"
    description = "Image processing library (linux node 8.10)"
    s3_key      = "layers/Sharp.zip"
  }
}

variable "compatible_runtimes" {
  type     = "list"   
   default = ["nodejs10.x"]
}

###########################PhantomJsBinnaries

variable "layer_params_2" {
  type    = "map"
  default = {
    layer_name        = "PhantomJsBinnaries"
    description       = "nodejs/node_module"
    s3_key            = "layers/PhantomJsBinnaries.zip"
    s3_object_version = "kE4hpgc.jl09zRZnGZ8RNIuxqAQHhS_7"
  }
}

variable "compatible_runtimes_2" {
  type     = "list"   
   default = ["nodejs10.x"]
}


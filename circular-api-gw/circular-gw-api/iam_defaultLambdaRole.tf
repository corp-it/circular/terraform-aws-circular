
################role policy for defaultLambdaRole

resource "aws_iam_role" "Circular-default-Lambda-Role" {
  name = "CircularDefault_Lambda_Rol"
  assume_role_policy = "${data.aws_iam_policy_document.Default-Lambda-assume-role-policy.json}"
}

data "aws_iam_policy_document" "Default-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

#######################################Lambda-Basic-Execution

resource "aws_iam_role_policy_attachment" "Circular-Lambda-Basic-Execution-role-policy" {
  role       = "${aws_iam_role.Circular-default-Lambda-Role.name}"
  policy_arn = "${aws_iam_policy.Circular-Lambda-Basic-Execution-policy.arn}"
}

resource "aws_iam_policy" "Circular-Lambda-Basic-Execution-policy" {
  name    = "Circular-Lambda-Basic_Execution-policy"
  policy  = "${data.aws_iam_policy_document.Lambda-basic_Execution-circular-policy.json}"
}

data "aws_iam_policy_document" "Lambda-basic_Execution-circular-policy" {
  statement {
    effect  = "Allow"
    actions = [
            "logs:CreateLogGroup",
    ]
  resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*"
  ]
 }
}

###################################Cognito-Admin-Create-User

resource "aws_iam_role_policy_attachment" "Cognito-Admin-Create-User-role-policy" {
  role       = "${aws_iam_role.Circular-default-Lambda-Role.name}"
  policy_arn = "${aws_iam_policy.Cognito-Admin-Create-User-policy.arn}"
}

resource "aws_iam_policy" "Cognito-Admin-Create-User-policy" {
  name    = "Cognito_AdminCreate_User_Circular-policy"
  policy  = "${data.aws_iam_policy_document.Cognito_AdminCreate_User_Circular-policy.json}"
}

data "aws_iam_policy_document" "Cognito_AdminCreate_User_Circular-policy" {
  statement {
    sid     = "VisualEditor0"
    effect  = "Allow"
    actions = [
            "cognito-idp:AdminCreateUser",
    ]
  resources = [
      "*"
  ]
 }
}

#####################################Lambda invoke function

resource "aws_iam_role_policy_attachment" "LambdaInvoke-policy" {
  role       = "${aws_iam_role.Circular-default-Lambda-Role.name}"
  policy_arn = "${aws_iam_policy.LambdaInvoke-policy.arn}"
}

####################################Cognito-Admin-Create-User-Attributes

resource "aws_iam_role_policy_attachment" "Cognito-Admin-Create-User-Attributes-role-policy" {
  role       = "${aws_iam_role.Circular-default-Lambda-Role.name}"
  policy_arn = "${aws_iam_policy.Cognito-Admin-Create-User-Attributes-policy.arn}"
}

resource "aws_iam_policy" "Cognito-Admin-Create-User-Attributes-policy" {
  name    = "CognitoAdmin_CreateUser_Attributes_Circular-policy"
  policy  = "${data.aws_iam_policy_document.Admin_UpdateUser_Attributes_Circular-policy.json}"
}

data "aws_iam_policy_document" "Admin_UpdateUser_Attributes_Circular-policy" {
  statement {
    sid     = "VisualEditor0"
    effect  = "Allow"
    actions = [
            "cognito-idp:AdminUpdateUserAttributes",
    ]
  resources = [
      "*"
  ]
 }
}

#####################################Policies for AWS

resource "aws_iam_role_policy_attachment" "AmazonRDSFullAccess-policy" {
  role       = "${aws_iam_role.Circular-default-Lambda-Role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonRDSFullAccess"
}

resource "aws_iam_role_policy_attachment" "CloudWatchLogsFullAccess-policy" {
  role       = "${aws_iam_role.Circular-default-Lambda-Role.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}


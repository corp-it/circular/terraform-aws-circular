#####################################Certificate

resource "aws_api_gateway_client_certificate" "circular" {
  description = "Test cartificate"
}

################################################VPC Link Api GW
resource "aws_api_gateway_vpc_link" "Circular-vpc-link" {
  name        = "Circular-ednpoit"
  target_arns = ["${data.terraform_remote_state.circular_ec2.nlb_arn}"]
}

##################################################

module "gw_circular_rest_api" {
  source             = "corpit-consulting-public/api-gateway/aws"
  version            = "v3.1.0"
  name               = "${var.gw_params["name"]}"
  description        = "${var.gw_params["description"]}"
  api_key_source     = "${var.gw_params["api_key_source"]}"
  body               = "${data.template_file.circular_api_gw_swagger.rendered}"
  types              = ["${var.types}"]
  
  ###Deploymnet
  stage_name         = "${var.stage_name}"
  version_id         = "v1.2.0-circular"

  #####Stage
  cache_cluster_enabled               = "false"
  #cache_cluster_size                  = "0.5"
  variables          = {
    takeWebsiteScreenshotlambda       = "${module.takeWebsiteScreenshot.id}"
    uploadImageLocationlambda         = "${module.uploadImageLocation.id}"
    s3temporaryPresignedURLlambda     = "${module.s3temporaryPresignedURL.id}"
    sendMessageToManyAPIAdapterlambda = "${module.sendMessageToManyAPIAdapter.id}"
    registerFirebaseTokenlambda       = "${module.registerFirebaseToken.id}"
    contentBucket                     = "${var.contentBucket}"
    contentEndpoint                   = "${var.contentEndpoint}"
    conversalabSiteUrl                = "${var.conversalabSiteUrl}"
    conversalabPublicSiteUrl          = "${var.conversalabPublicSiteUrl}"
    vpcLinkId                         = "${aws_api_gateway_vpc_link.Circular-vpc-link.id}"
  }

  ####Method Settings
  method_path                                = "${var.method_params["method_path"]}"
    metrics_enabled                            = "${var.method_params["metrics_enabled"]}" 
    logging_level                              = "${var.method_params["logging_level"]}"
    data_trace_enabled                         = "${var.method_params["data_trace_enabled"]}"
    throttling_burst_limit                     = "${var.method_params["throttling_burst_limit"]}"
    throttling_rate_limit                      = "${var.method_params["throttling_rate_limit"]}"
    caching_enabled                            = "${var.method_params["caching_enabled"]}"
    cache_ttl_in_seconds                       = "${var.method_params["cache_ttl_in_seconds"]}"
    cache_data_encrypted                       = "${var.method_params["cache_data_encrypted"]}"
    require_authorization_for_cache_control    = "${var.method_params["require_authorization_for_cache_control"]}"
    unauthorized_cache_control_header_strategy = "${var.method_params["unauthorized_cache_control_header_strategy"]}"
}

resource "aws_api_gateway_account" "demo" {
  cloudwatch_role_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ApiGatewayPushToCloudwatch"
}

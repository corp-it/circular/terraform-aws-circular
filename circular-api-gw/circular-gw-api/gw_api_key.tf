module "circular-gw-api-key" {
  source      = "corpit-consulting-public/api-gateway-api-key/aws"
  version     = "v0.1.0"
  name        = "${var.gw_api_key_params["name"]}"
  enabled     = "${var.gw_api_key_params["enabled"]}"
  description = "${var.gw_api_key_params["description"]}"
  value       = "${var.gw_api_key_params["value"]}"
}

module "circular-actors-gw-api-key" {
  source      = "corpit-consulting-public/api-gateway-api-key/aws"
  version     = "v0.1.0"
  name        = "${var.gw_api_key_params_2["name"]}"
  enabled     = "${var.gw_api_key_params_2["enabled"]}"
  description = "${var.gw_api_key_params_2["description"]}"
  value       = "${var.gw_api_key_params_2["value"]}"
}

module "circular-cargil-gw-api-key" {
  source      = "corpit-consulting-public/api-gateway-api-key/aws"
  version     = "v0.1.0"
  name        = "${var.gw_api_key_params_3["name"]}"
  enabled     = "${var.gw_api_key_params_3["enabled"]}"
  description = "${var.gw_api_key_params_3["description"]}"
  value       = "${var.gw_api_key_params_3["value"]}"
}

module "circular-createCognitoUserWithPhone-Key-gw-api-key" {
  source      = "corpit-consulting-public/api-gateway-api-key/aws"
  version     = "v0.1.0"
  name        = "${var.gw_api_key_params_4["name"]}"
  enabled     = "${var.gw_api_key_params_4["enabled"]}"
  description = "${var.gw_api_key_params_4["description"]}"
  value       = "${var.gw_api_key_params_4["value"]}"
}

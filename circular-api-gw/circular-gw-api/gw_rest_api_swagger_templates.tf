#################################Circular api

data template_file "circular_api_gw_swagger" {
  template = "${file("circular-swagger-template/api_gw_swagger.tpl")}"
  vars {
    region                      = "${local.region}"
    user_pool_id                = "${data.terraform_remote_state.circular_cognito.user_pool_id}"
    account_id                  = "${data.aws_caller_identity.current.account_id}"
    rest_api_name               = "${var.gw_params["name"]}"
    rest_api_description        = "${var.gw_params["description"]}"
  }
}


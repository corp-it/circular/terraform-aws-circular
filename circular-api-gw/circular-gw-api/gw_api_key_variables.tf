################ServerKey
variable "gw_api_key_params" {
  type    = "map"
  default = {
    name        = "ServerKey"
    enabled     = "true"
    description = "key to be used for calls from the server"
    value       = "5OgKLFEHho9nhMgiaVndL96JdcPuqcJsc2dAAUN8"
  }
}

################Circular Actors API

variable "gw_api_key_params_2" {
  type    = "map"
  default = {
    name        = "CircularActors"
    enabled     = "true"
    description = "ApiKey for Circular Actors API."
    value       = "KTf176vBoXcQwnX4Iyn13odccO2MkKE2IhCxxH69"
  }
}

################Cargil

variable "gw_api_key_params_3" {
  type    = "map"
  default = {
    name        = "Cargil"
    enabled     = "true"
    description = "Key to be used by cargil to integrate with turn API"
    value       = "pCExlrlmnm9MygqvMToCa5EThmuYe2Ij9gyGNCcv"
  }
}

################createCognitoUserWithPhone-Key

variable "gw_api_key_params_4" {
  type    = "map"
  default = {
    name        = "createCognitoUserWithPhone-Key"
    enabled     = "true"
    description = "Created by AWS Lambda"
    value       = "hwAZzn8ipDaWL5TpD2BPQ1wLCxjWgmE12sFWo0s6"
  }
}




variable "gw_params" {
  type    = "map"
  default = {
    name           = "Circular API"
    api_key_source = "HEADER"
    description    = "API para administrar ofertas y ordenes de viajes hacia y desde acopios"
  }
}

variable "types" {
  type = "list"
  default = ["REGIONAL"]
}


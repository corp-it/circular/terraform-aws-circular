variable global_tags {
    default = {
      "Owner" = "Cliente"
    }
}

variable asg_global_tags {
    default = [
      {
        "key" = "Owner"
        "value" = "Cliente"
        "propagate_at_launch" = "true"
      },
    ]
}

variable "workspace_name_prefix" {
}

variable "s3_obj_registerFirebaseToken" {
}

variable "s3_obj_uploadImageLocation" {
}

variable "s3_obj_takeWebsiteScreenshot" {
}

variable "s3_obj_sendMessageToManyAPIAdapter" {
}

variable "s3_obj_sendMessageToMany" {
}

variable "s3_obj_s3temporaryPresignedURL" {
}

########Variables for stage

variable "contentBucket" {
}

variable "contentEndpoint" {
}

variable "conversalabSiteUrl" {
}

variable "conversalabPublicSiteUrl" {
}

variable "stage_name" {
}

###########Lambda Layer

variable "s3_obj_Sharp" {
}

variable "s3_obj_PhantomJsBinnaries" {
}

###########Lambdas variables

variable "circularBucketPublicBaseUrl" {
}

variable "dstBucket" {
}

variable "CircularBot" {
}
 
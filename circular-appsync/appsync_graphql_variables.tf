variable "graphql_params" {
  type    = "map"
  default = {
    authentication_type = "AMAZON_COGNITO_USER_POOLS"
    name                = "CircularAppSync"
    default_action      = "ALLOW"
    field_log_level     = "ALL"
  }
}
##############################

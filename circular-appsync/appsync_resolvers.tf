module "appsync-resolver" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id            = "${module.graphql-api-CircularAppSync.id}"
  field             = "messages"
  type              = "Conversation"
  data_source       = "${module.datasource-messagesTableDataSource.name}"
  request_template  = "${file("templates/resolvers/Conversation/messages/request.json")}"
  response_template = "${file("templates/resolvers/Conversation/messages/response.json")}"
}


########################################

module "appsync-resolver-conversationsTableDataSource" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "createConversation"
  type             = "Mutation"
  data_source      = "${module.datasource-conversationsTableDataSource.name}"
  request_template = "${file("templates/resolvers/Mutation/createConversation/request.json")}"

 response_template = "${file("templates/resolvers/Mutation/createConversation/response.json")}"
}

########################################

module "appsync-resolver-3" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "createMessage"
  type             = "Mutation"
  data_source      = "${module.datasource-messagesTableDataSource.name}"
  request_template = "${file("templates/resolvers/Mutation/createMessage/request.json")}"

 response_template = "${file("templates/resolvers/Mutation/createMessage/response.json")}"
}

########################################

module "appsync-resolver-4" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "makeMessageRead"
  type             = "Mutation"
  data_source      = "${module.datasource-messagesTableDataSource.name}"
  request_template = "${file("templates/resolvers/Mutation/makeMessageRead/request.json")}"

 response_template = "${file("templates/resolvers/Mutation/makeMessageRead/response.json")}"
}

########################################

module "appsync-resolver-5" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "createUser"
  type             = "Mutation"
  data_source      = "${module.datasource-usersTableDataSource.name}"
  request_template = "${file("templates/resolvers/Mutation/createUser/request.json")}"

 response_template = "${file("templates/resolvers/Mutation/createUser/response.json")}"
}

########################################

module "appsync-resolver-6" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "createUserConversations"
  type             = "Mutation"
  data_source      = "${module.datasource-userConversationsTableDataSource.name}"
  request_template = "${file("templates/resolvers/Mutation/createUserConversations/request.json")}"

 response_template = "${file("templates/resolvers/Mutation/createUserConversations/response.json")}"
}

########################################

module "appsync-resolver-7" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "startConversationWith"
  type             = "Mutation"
  data_source      = "${module.StartConversationWithLambda.name}"
  request_template = "${file("templates/resolvers/Mutation/startConversationWith/request.json")}"
  response_template = "${file("templates/resolvers/Mutation/startConversationWith/response.json")}"

}

########################################

module "appsync-resolver-8" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "allMessage"
  type             = "Query"
  data_source      = "${module.datasource-messagesTableDataSource.name}"
  request_template = "${file("templates/resolvers/Query/allMessage/request.json")}"

 response_template = "${file("templates/resolvers/Query/allMessage/response.json")}"
}

########################################

module "appsync-resolver-9" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "allMessageConnection"
  type             = "Query"
  data_source      = "${module.datasource-messagesTableDataSource.name}"
  request_template = "${file("templates/resolvers/Query/allMessageConnection/request.json")}"

 response_template = "${file("templates/resolvers/Query/allMessageConnection/response.json")}"
}

########################################

module "appsync-resolver-10" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "allMessageFrom"
  type             = "Query"
  data_source      = "${module.datasource-messagesTableDataSource.name}"
  request_template = "${file("templates/resolvers/Query/allMessageFrom/request.json")}"

 response_template = "${file("templates/resolvers/Query/allMessageFrom/response.json")}"
}

########################################

module "appsync-resolver-11" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "allUser"
  type             = "Query"
  data_source      = "${module.datasource-usersTableDataSource.name}"
  request_template = "${file("templates/resolvers/Query/allUser/request.json")}"

 response_template = "${file("templates/resolvers/Query/allUser/response.json")}"
}

########################################

module "appsync-resolver-12" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "me"
  type             = "Query"
  data_source      = "${module.datasource-usersTableDataSource.name}"
  request_template = "${file("templates/resolvers/Query/me/request.json")}"

 response_template = "${file("templates/resolvers/Query/me/response.json")}"
}

########################################

module "appsync-resolver-13" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "users"
  type             = "Query"
  data_source      = "${module.datasource-usersTableDataSource.name}"
  request_template = "${file("templates/resolvers/Query/users/request.json")}"

 response_template = "${file("templates/resolvers/Query/users/response.json")}"
}

########################################

module "appsync-resolver-14" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "conversations"
  type             = "User"
  data_source      = "${module.datasource-userConversationsTableDataSource.name}"
  request_template = "${file("templates/resolvers/User/conversations/request.json")}"

 response_template = "${file("templates/resolvers/User/conversations/response.json")}"
}

########################################

module "appsync-resolver-15" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "messages"
  type             = "User"
  data_source      = "${module.datasource-messagesTableDataSource.name}"
  request_template = "${file("templates/resolvers/User/messages/request.json")}"

 response_template = "${file("templates/resolvers/User/messages/response.json")}"
}

########################################

module "appsync-resolver-16" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "contacts"
  type             = "User"
  data_source      = "${module.ConversaUserContacts.name}"
  request_template = "${file("templates/resolvers/User/contacts/request.json")}"

 response_template = "${file("templates/resolvers/User/contacts/response.json")}"
}

########################################

module "appsync-resolver-17" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "associated"
  type             = "UserConversations"
  data_source      = "${module.datasource-userConversationsTableDataSource.name}"
  request_template = "${file("templates/resolvers/UserConversations/associated/request.json")}"

 response_template = "${file("templates/resolvers/UserConversations/associated/response.json")}"
}

########################################

module "appsync-resolver-18" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "conversation"
  type             = "UserConversations"
  data_source      = "${module.datasource-conversationsTableDataSource.name}"
  request_template = "${file("templates/resolvers/UserConversations/conversation/request.json")}"

 response_template = "${file("templates/resolvers/UserConversations/conversation/response.json")}"
}

########################################

module "appsync-resolver-19" {
  source            = "corpit-consulting-public/appsync-resolver-mod/aws"
  version           = "v0.1.0"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  field            = "user"
  type             = "UserConversations"
  data_source      = "${module.datasource-usersTableDataSource.name}"
  request_template = "${file("templates/resolvers/UserConversations/user/request.json")}"

 response_template = "${file("templates/resolvers/UserConversations/user/response.json")}"
}


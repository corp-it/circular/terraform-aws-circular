variable global_tags {
    default = {
      "Owner" = "Cliente"
    }
}

variable asg_global_tags {
    default = [
      {
        "key" = "Owner"
        "value" = "Cliente"
        "propagate_at_launch" = "true"
      },
    ]
}

variable "workspace_name_prefix" {
}

variable "http_endpoint" {
}

variable "StartConversation_arn" {
}

########################Variables for conversationsTableDataSource

variable "datasource_params-01" {
  type    = "map"
  default = {
    type  = "AMAZON_DYNAMODB"
    name  = "conversationsTableDataSource"
  }
}
##########################Variables for messagesTableDataSource

variable "datasource_params-02" {
  type    = "map"
  default = {
    type  = "AMAZON_DYNAMODB"
    name  = "messagesTableDataSource"
  }
}
##########################Variables for userConversationsTableDataSource

variable "datasource_params-03" {
  type    = "map"
  default = {
    type  = "AMAZON_DYNAMODB"
    name  = "userConversationsTableDataSource"
  }
}
##########################Variables for usersTableDataSource

variable "datasource_params-04" {
  type    = "map"
  default = {
    type  = "AMAZON_DYNAMODB"
    name  = "usersTableDataSource"
  }
}
##########################Variables for ConversaUserContacts

variable "datasource_params-05" {
  type            = "map"
  default         = {
    type          = "HTTP"
    name          = "ConversaUserContacts"
    http_endpoint = "https://services.appcircular.com/"
  }
}
##########################Variables for StartConversationWithLambda

variable "datasource_params-06" {
  type            = "map"
  default         = {
    type          = "AWS_LAMBDA"
    name          = "StartConversationWithLambda"
  }
}


################role policy for defaultLambdaRole
resource "aws_iam_role" "Circular-AppSync-DataSource-Lambda-Role" {
  name = "Circular-AppSync-DataSource-Lambda-Role"
  assume_role_policy = "${data.aws_iam_policy_document.AppSync-DataSource-with-Lambda-assume-role-policy.json}"
}

data "aws_iam_policy_document" "AppSync-DataSource-with-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "appsync.amazonaws.com",
        "lambda.amazonaws.com"
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

##############################Policies of AWS

resource "aws_iam_role_policy_attachment" "AmazonLambdaFullAccess-policy" {
  role       = "${aws_iam_role.Circular-AppSync-DataSource-Lambda-Role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaFullAccess"
}

resource "aws_iam_role_policy_attachment" "DynamodbFullAccess-policy" {
  role       = "${aws_iam_role.Circular-AppSync-DataSource-Lambda-Role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "DynamoDBFullAccesswithDataPipeline" {
  role       = "${aws_iam_role.Circular-AppSync-DataSource-Lambda-Role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccesswithDataPipeline"
}

resource "aws_iam_role_policy_attachment" "AWSAppSyncAdministrator" {
  role       = "${aws_iam_role.Circular-AppSync-DataSource-Lambda-Role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSAppSyncAdministrator"
}


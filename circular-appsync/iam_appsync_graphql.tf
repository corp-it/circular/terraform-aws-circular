################role policy for defaultLambdaRole
resource "aws_iam_role" "Circular-AppSync-Graphql-Role" {
  name = "Circular-AppSync-DataSource-Role"
  assume_role_policy = "${data.aws_iam_policy_document.AppSync-Graphql-assume-role-policy.json}"
}

data "aws_iam_policy_document" "AppSync-Graphql-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "appsync.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

####################################dynamodb-access

resource "aws_iam_role_policy_attachment" "Appsync-graphql-dynamodb-role-policy-attach" {
  role       = "${aws_iam_role.Circular-AppSync-Graphql-Role.name}"
  policy_arn = "${aws_iam_policy.Circular-appsyn-dynamodb-policy.arn}"
}

resource "aws_iam_policy" "Circular-appsyn-dynamodb-policy" {
  name    = "dynamodb-access"
  policy  = "${data.aws_iam_policy_document.Circular-appsync-dynamodbAcces-policy.json}"
}

data "aws_iam_policy_document" "Circular-Appsync-logs-policy" {
  statement {
         sid      = "VisualEditor0",
         effect   = "Allow",
         actions  = [
                "dynamodb:*",
         ]
         resources = [
            "*"
         ]
 },
}

###########################################

resource "aws_iam_role_policy_attachment" "Appsync-Graphql-Logs-role-policy-attach" {
  role       = "${aws_iam_role.Circular-AppSync-Graphql-Role.name}"
  policy_arn = "${aws_iam_policy.Circular-Appsyn-Graphql-policy.arn}"
}

resource "aws_iam_policy" "Circular-Appsyn-Graphql-policy" {
  name    = "appsync-graphqlapi-logs-us-east-1"
  policy  = "${data.aws_iam_policy_document.Circular-Appsync-Graphql-logs-policy.json}"
}

data "aws_iam_policy_document" "Circular-Appsync-Graphql-logs-policy" {
  statement {
         sid      = "VisualEditor0",
         effect   = "Allow",
         actions  = [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
         ]
         resources = [
            "*"
         ]
  },
  statement {
         sid      = "VisualEditor1",
         effect   = "Allow",
         actions  = [
                "logs:CreateLogGroup"
         ]
         resources = [
            "*"
         ]
  },
}


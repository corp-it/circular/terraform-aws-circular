module "graphql-api-CircularAppSync" {
  source              = "corpit-consulting-public/appsync-graphql-api-mod/aws"
  version             = "v0.1.0"
  authentication_type = "${var.graphql_params["authentication_type"]}"
  name                = "${var.graphql_params["name"]}"
  default_action      = "${var.graphql_params["default_action"]}"
  user_pool_id        = "${data.terraform_remote_state.circular_cognito.user_pool_id}"
  aws_region          = "${local.region}"
  schema              = "${file("./templates/appsync_schema/schema.graphql")}"
  cloudwatch_logs_role_arn  = "${aws_iam_role.Circular-AppSync-Graphql-Role.arn}"
  field_log_level           = "${var.graphql_params["field_log_level"]}"
}


################role policy for defaultLambdaRole
resource "aws_iam_role" "Circular-AppSync-DataSource-Role" {
  name = "Circular_AppSync_DataSource_Role"
  assume_role_policy = "${data.aws_iam_policy_document.AppSync-DataSource-assume-role-policy.json}"
}

data "aws_iam_policy_document" "AppSync-DataSource-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "appsync.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}


#######################################

resource "aws_iam_role_policy_attachment" "Appsync-dynamodb-role-policy-attach" {
  role       = "${aws_iam_role.Circular-AppSync-DataSource-Role.name}"
  policy_arn = "${aws_iam_policy.Circular-appsyn-dinamodb-policy.arn}"
}

resource "aws_iam_policy" "Circular-appsyn-dinamodb-policy" {
  name    = "Circular-Appsync-dynamodb"
  policy  = "${data.aws_iam_policy_document.Circular-appsync-dynamodbAcces-policy.json}"
}

data "aws_iam_policy_document" "Circular-appsync-dynamodbAcces-policy" {
  statement {
         sid      = "VisualEditor0",
         effect   = "Allow",
         actions  = [
                "dynamodb:BatchGetItem",
                "dynamodb:BatchWriteItem",
                "dynamodb:PutItem",
                "dynamodb:GetItem",
                "dynamodb:Scan",
                "dynamodb:Query",
                "dynamodb:UpdateItem"
         ]
         resources = [
            "arn:aws:dynamodb:*:*:table/*"
         ]
 },
 statement {
   sid      = "VisualEditor1",
   effect   = "Allow",
   actions  = [
            "dynamodb:Scan",
            "dynamodb:Query"
   ]
   resources = [
      "arn:aws:dynamodb:*:*:table/*/index/*"
   ]
  }
}

###################################

resource "aws_iam_role_policy_attachment" "Appsync-log-role-policy-attach" {
  role       = "${aws_iam_role.Circular-AppSync-DataSource-Role.name}"
  policy_arn = "${aws_iam_policy.Circular-appsyn-log-policy.arn}"
}

resource "aws_iam_policy" "Circular-appsyn-log-policy" {
  name    = "Circular-Appsync-log"
  policy  = "${data.aws_iam_policy_document.Circular-appsync-log-policy.json}"
}

data "aws_iam_policy_document" "Circular-appsync-log-policy" {
  statement {
         sid      = "VisualEditor0",
         effect   = "Allow",
         actions  = [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
         ]
         resources = [
             "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
         ]
 }
}



######################################

module "datasource-conversationsTableDataSource" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params-01["name"]}"
  type             = "${var.datasource_params-01["type"]}"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Role.arn}"
  table_name       = "${data.terraform_remote_state.circular_dynamodb.conversationsTable}"
}
######################################

module "datasource-messagesTableDataSource" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params-02["name"]}"
  type             = "${var.datasource_params-02["type"]}"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Role.arn}"
  table_name       = "${data.terraform_remote_state.circular_dynamodb.messagesTable}"
}                                                   
#####################################

module "datasource-userConversationsTableDataSource" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params-03["name"]}"
  type             = "${var.datasource_params-03["type"]}"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Role.arn}"
  table_name       = "${data.terraform_remote_state.circular_dynamodb.userConversationsTable}"
}
#####################################

module "datasource-usersTableDataSource" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params-04["name"]}"
  type             = "${var.datasource_params-04["type"]}"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Role.arn}"
  table_name       = "${data.terraform_remote_state.circular_dynamodb.usersTable}"
}
#####################################

module "ConversaUserContacts" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params-05["name"]}"
  type             = "${var.datasource_params-05["type"]}"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  http_endpoint    = "${var.http_endpoint}"
}

#####################################

module "StartConversationWithLambda" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params-06["name"]}"
  type             = "${var.datasource_params-06["type"]}"
  api_id           = "${module.graphql-api-CircularAppSync.id}"
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Lambda-Role.arn}"
  function_arn     = "${var.StartConversation_arn}"
}



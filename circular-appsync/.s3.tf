resource "aws_s3_bucket" "lambda-s3" {
  bucket = "circular-lambda-artifacts-prod"
  acl    = "private"

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "circular-uploads" {
  bucket = "temporary-uploads-circular-prod"
  acl    = "private"

  versioning {
    enabled = true
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["POST"]
    allowed_origins = ["https://*.appcircular.com"]
  }
}


resource "aws_s3_bucket" "app-circular" {
  bucket = "app-circular-prod"
  acl    = "private"

  versioning {
    enabled = true
  }
}

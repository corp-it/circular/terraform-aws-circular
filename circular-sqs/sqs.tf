module "ErroresRtoYRuta" {
  source  = "corpit-consulting-public/sqs/aws"
  version = "v0.2.0"
  name                       = "${var.ErroresRtoYRuta_params["name"]}"
  visibility_timeout_seconds = "${var.ErroresRtoYRuta_params["visibility_timeout_seconds"]}"
  max_message_size           = "${var.ErroresRtoYRuta_params["max_message_size"]}"
  message_retention_seconds  = "${var.ErroresRtoYRuta_params["message_retention_seconds"]}"
  delay_seconds              = "${var.ErroresRtoYRuta_params["delay_seconds"]}"
}
module "RtoAndRutaUpdate" {
  source  = "corpit-consulting-public/sqs/aws"
  version = "v0.2.0"
  name                       = "${var.RtoAndRutaUpdate_params["name"]}"
  visibility_timeout_seconds = "${var.RtoAndRutaUpdate_params["visibility_timeout_seconds"]}"
  max_message_size           = "${var.RtoAndRutaUpdate_params["max_message_size"]}"
  message_retention_seconds  = "${var.RtoAndRutaUpdate_params["message_retention_seconds"]}"
  delay_seconds              = "${var.RtoAndRutaUpdate_params["delay_seconds"]}"
}

module "UpdateAvatar" {
  source  = "corpit-consulting-public/sqs/aws"
  version = "v0.2.0"
  name                       = "${var.UpdateAvatar_params["name"]}"
  visibility_timeout_seconds = "${var.UpdateAvatar_params["visibility_timeout_seconds"]}"
  max_message_size           = "${var.UpdateAvatar_params["max_message_size"]}"
  message_retention_seconds  = "${var.UpdateAvatar_params["message_retention_seconds"]}"
  delay_seconds              = "${var.UpdateAvatar_params["delay_seconds"]}"
}

module "UpdateAvatarErrors" {
  source  = "corpit-consulting-public/sqs/aws"
  version = "v0.2.0"
  name                       = "${var.UpdateAvatarErrors_params["name"]}"
  visibility_timeout_seconds = "${var.UpdateAvatarErrors_params["visibility_timeout_seconds"]}"
  max_message_size           = "${var.UpdateAvatarErrors_params["max_message_size"]}"
  message_retention_seconds  = "${var.UpdateAvatarErrors_params["message_retention_seconds"]}"
  delay_seconds              = "${var.UpdateAvatarErrors_params["delay_seconds"]}"
}

module "MassiveMessages" {
  source  = "corpit-consulting-public/sqs/aws"
  version = "v0.3.0"
  name                       = "${var.MassiveMessages_params["name"]}"
  visibility_timeout_seconds = "${var.MassiveMessages_params["visibility_timeout_seconds"]}"
  max_message_size           = "${var.MassiveMessages_params["max_message_size"]}"
  message_retention_seconds  = "${var.MassiveMessages_params["message_retention_seconds"]}"
  delay_seconds              = "${var.MassiveMessages_params["delay_seconds"]}"
  receive_wait_time_seconds  = "${var.MassiveMessages_params["receive_wait_time_seconds"]}"
}

module "DataExtraction" {
  source  = "corpit-consulting-public/sqs/aws"
  version = "v0.3.0"
  name                       = "${var.DataExtraction_params["name"]}"
  visibility_timeout_seconds = "${var.DataExtraction_params["visibility_timeout_seconds"]}"
  max_message_size           = "${var.DataExtraction_params["max_message_size"]}"
  message_retention_seconds  = "${var.DataExtraction_params["message_retention_seconds"]}"
  delay_seconds              = "${var.DataExtraction_params["delay_seconds"]}"
  receive_wait_time_seconds  = "${var.DataExtraction_params["receive_wait_time_seconds"]}"
}

locals {
	## Global variables definition based on workspace.
    env = "${var.workspace_name_prefix}"

	## Workspace Variables
	region = "us-east-1"

	global_tags = {
        	project = "Circular"
	}
}



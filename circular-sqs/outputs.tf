
output "ErroresRtoYRuta_arn" {
  value = "${module.ErroresRtoYRuta.arn}"
}

output "RtoAndRutaUpdate_arn" {
  value = "${module.RtoAndRutaUpdate.arn}"
}

output "UpdateAvatar_arn" {
  value = "${module.UpdateAvatar.arn}"
}

output "UpdateAvatarErrors_arn" {
  value = "${module.UpdateAvatarErrors.arn}"
}

output "MassiveMessages_arn" {
  value = "${module.MassiveMessages.arn}"
}

output "DataExtraction_arn" {
  value = "${module.DataExtraction.arn}"
}

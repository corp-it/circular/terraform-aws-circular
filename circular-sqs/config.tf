terraform {
    required_version = ">= 0.11.14"
    backend "remote" {
        organization = "Circular-qa"
        workspaces {
            prefix = "circular-sqs-"
        }
    }    
}

provider "aws" {
    version = "v2.70.0"
    region  = "${local.region}"
   # region  = "us-east-1"
   # profile = "tfcloud-dev"
}


data "terraform_remote_state" "circular_appsync" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-appsync-${var.workspace_name_prefix}"
       }
    }
}

#data "terraform_remote_state" "circular_gw_actors" {
#    backend = "remote"
#    config = {
#       organization = "Circular-qa"
#       workspaces   = {
#          name = "circular-actors-gw-${var.workspace_name_prefix}"
#       }
#    }
#}

data "terraform_remote_state" "circular_cognito" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-cognito-${var.workspace_name_prefix}"
       }
    }
}




#######################ErroresRtoYRuta

variable "ErroresRtoYRuta_params" {
  type    = "map"
  default = {
    name                       = "ErroresRtoYRuta"
    visibility_timeout_seconds = 30
    max_message_size           = 262144
    message_retention_seconds  = 345600
    delay_seconds              = 0
  }
}

#######################RtoAndRutaUpdate

variable "RtoAndRutaUpdate_params" {
  type    = "map"
  default = {
    name                       = "RtoAndRutaUpdate"
    visibility_timeout_seconds = 30
    max_message_size           = 262144
    message_retention_seconds  = 345600
    delay_seconds              = 0
  }
}

#######################UpdateAvatar

variable "UpdateAvatar_params" {
  type    = "map"
  default = {
    name                       = "UpdateAvatar"
    visibility_timeout_seconds = 30
    max_message_size           = 262144
    message_retention_seconds  = 345600
    delay_seconds              = 0
  }
}

#######################UpdateAvatarErrors

variable "UpdateAvatarErrors_params" {
  type    = "map"
  default = {
    name                       = "UpdateAvatarErrors"
    visibility_timeout_seconds = 30
    max_message_size           = 262144
    message_retention_seconds  = 345600
    delay_seconds              = 0
  }
}

#######################MassiveMessages

variable "MassiveMessages_params" {
  type    = "map"
  default = {
    name                       = "MassiveMessages"
    visibility_timeout_seconds = 280
    max_message_size           = 262144
    message_retention_seconds  = 86400
    delay_seconds              = 0
    receive_wait_time_seconds  = 10
  }
}

#######################DataExtraction

variable "DataExtraction_params" {
  type    = "map"
  default = {
    name                       = "DataExtraction"
    visibility_timeout_seconds = 93
    max_message_size           = 262144
    message_retention_seconds  = 86400
    delay_seconds              = 0
    receive_wait_time_seconds  = 0
  }
}
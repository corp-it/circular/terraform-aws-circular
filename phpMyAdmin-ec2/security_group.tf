
##########################################SG-ASG-phpMyAdmin

module "sg_asg_phpMyAdmin" {
  source      = "corpit-consulting-public/security-group/aws"
  version     = "v2.17.0"
  name                 = "${var.sg_asg_phpMyAdmin_params["name"]}"
  use_name_prefix      = "true"
  description          = "${var.sg_asg_phpMyAdmin_params["description"]}"
  vpc_id               = "${data.terraform_remote_state.circular_vpc.vpc_id}"
  egress_cidr_blocks   = "${var.sg_asg_phpMyAdmin_cidr["egress_cidr_blocks"]}"
  egress_rules         = "${var.sg_asg_phpMyAdmin_cidr["egress_rules"]}"
  computed_ingress_with_source_security_group_id = [
    {
      source_security_group_id = "${element(flatten(module.alb-phpMyAdmin.security_groups),0)}"
      rule = "http-80-tcp"
    },
    {
      source_security_group_id = "${element(flatten(module.alb-phpMyAdmin.security_groups),0)}"
      rule = "https-443-tcp"
    }

  ]
  number_of_computed_ingress_with_source_security_group_id = 2
}

#########################################SG-ALB-phpMyAdmin

module "sg_alb_phpMyAdmin_http" {
  source      = "corpit-consulting-public/security-group/aws"
  version     = "v2.17.0"
  name                  = "${var.sg_alb_phpMyAdmin_params["name"]}"
  description           = "${var.sg_alb_phpMyAdmin_params["description"]}"
  vpc_id                = "${data.terraform_remote_state.circular_vpc.vpc_id}"
  ingress_cidr_blocks   = "${var.sg_alb_phpMyAdmin_cidr["ingress_cidr_blocks"]}"
  ingress_rules         = "${var.sg_alb_phpMyAdmin_cidr["ingress_rules"]}"
  egress_cidr_blocks    = "${var.sg_alb_phpMyAdmin_cidr["egress_cidr_blocks"]}"
  egress_rules          = "${var.sg_alb_phpMyAdmin_cidr["egress_rules"]}"
}


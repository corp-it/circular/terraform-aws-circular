##########Alb-phpMyAdmin

variable http_listener_phpMyAdmin {
    type = "list"
    default = [{ 
        port = "80", 
        protocol = "HTTP"
    }]
        
 }

variable http_listeners_count_phpMyAdmin {
    default = "1"
}

## Depende de los Listeners definidos como Locals.
variable https_listeners_count_phpMyAdmin {
    default = "1"
}


variable target_groups_phpMyAdmin {
    type = "list"
    default = [{
        name = "PUBLIC-HTTP-TARGET-GROUP-PHP" , 
        backend_port = "80",
        backend_protocol = "HTTP"
      },
      {
        name = "PUBLIC-HTTPS-TARGET-GROUP-PHP",
        backend_port = "443"
        backend_protocol = "HTTPS"
      },
    ]
}

variable target_groups_count_phpMyAdmin {
    default = "1"
}

variable alb_tags_phpMyAdmin {
    default = {}
}

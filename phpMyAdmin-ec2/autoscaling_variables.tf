#######Asg-PHPmyAdmin

variable "asg_phpMyAdmin_params" {
    default = {
      name = "circular-phpMyAdmin"
      health_check_type = "EC2"
      health_check_grace_period = "120"
      min_size                  = 1
      max_size                  = 1
      desired_capacity          = 1
      wait_for_capacity_timeout = "30m" 
    }
}

variable "lc_phpMyAdmin_params" {
    default = {
      lc_name         = "circular-phpMyAdmin-lc"
      image_id        = "ami-0c6b2afb77a9fb7d0"
      instance_type   = "t2.micro"
      key_name        = "aws_circular_ec2"
    }
}

variable "asg_phpMyAdmin_tags" {
    default = [
      {
        "key"                   = "Name"
        "value"                 = "circular-phpMyAdmin"
        "propagate_at_launch"   = "true"
      },
    ]
}


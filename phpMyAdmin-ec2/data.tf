
data "terraform_remote_state" "circular_vpc" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-vpc-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "circular_bastion" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-bastion-${var.workspace_name_prefix}"
       }
    }
}


data "template_cloudinit_config" "userdata_config" {
    part {
        content_type = "text/x-shellscript"
        content = "yum install -y nginx && service nginx start"
    }
}

####################ASG-phpMyAdmin

data "template_file" "user_data_phpmyadmin" {
  template = "${file("${path.module}/templates/user_data/phpmyadmin_install.tpl")}"
}

locals {
  asg_phpMyAdmin_security_groups = "${list(
    data.terraform_remote_state.circular_bastion.bastion_sg,
    module.sg_asg_phpMyAdmin.this_security_group_id
  )}"
}

module "asg_phpMyAdmin" {
  source    = "corpit-consulting-public/autoscaling/aws"
  version   = "v2.9.2"

  name = "${var.asg_phpMyAdmin_params["name"]}"

  # Launch configuration
  lc_name         = "${var.lc_phpMyAdmin_params["lc_name"]}"
  image_id        = "${var.image_id}" 
  instance_type   = "${var.lc_phpMyAdmin_params["instance_type"]}"
  key_name        = "${var.key_name}"
  security_groups = "${local.asg_phpMyAdmin_security_groups}"
  user_data       = "${data.template_file.user_data_phpmyadmin.rendered}"

  # Auto scaling group
  asg_name                  = "${var.asg_phpMyAdmin_params["name"]}"
  vpc_zone_identifier       = "${data.terraform_remote_state.circular_vpc.vpc_private_subnets}"
  health_check_type         = "${var.asg_phpMyAdmin_params["health_check_type"]}"
  health_check_grace_period = "${var.asg_phpMyAdmin_params["health_check_grace_period"]}"
  min_size                  = "${var.asg_phpMyAdmin_params["min_size"]}"
  max_size                  = "${var.asg_phpMyAdmin_params["max_size"]}"
  desired_capacity          = "${var.asg_phpMyAdmin_params["desired_capacity"]}"
  wait_for_capacity_timeout = "${var.asg_phpMyAdmin_params["wait_for_capacity_timeout"]}"
  iam_instance_profile      = "${aws_iam_instance_profile.jenkins-instance-profile.arn}"

  target_group_arns = "${module.alb-phpMyAdmin.target_group_arns}"
  tags = "${concat(var.asg_phpMyAdmin_tags,var.asg_global_tags)}"

}

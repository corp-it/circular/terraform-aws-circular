#########SG_ASG_phpMyAdmin

variable sg_asg_phpMyAdmin_params {
    type = "map"
    default = {
      name = "asg-sg-php"
      description = "security group for autoscaling group"
    }
}

variable sg_asg_phpMyAdmin_cidr {
    type = "map"
    default = {
      egress_cidr_blocks = ["0.0.0.0/0"]
      egress_rules = ["all-all"]
    }
}

#######SG_ALB_phpMyAdmin

variable sg_alb_phpMyAdmin_params {
    type = "map"
    default = {
      name        = "alb-sg-php"
      description = "security group for aplication load balancer"
    }
}

variable sg_alb_phpMyAdmin_cidr {
    type = "map"
    default = {
      ingress_cidr_blocks = [        
        "152.168.72.155/32",
        "152.170.208.15/32",
        "152.170.220.236/32",
        "152.170.94.45/32",
        "181.114.149.224/32",
        "181.165.254.47/32",
        "181.167.90.13/32",
        "181.170.84.151/32",
        "181.228.181.125/32",
        "181.45.108.106/32",
        "181.45.172.243/32",
        "181.46.250.28/32",
        "181.81.79.214/32",
        "181.81.82.126/32",
        "181.84.183.0/32",
        "186.137.29.114/32",
        "190.16.236.224/32",
        "190.16.243.19/32",
        "190.16.42.37/32",
        "200.123.143.120/32",
        "24.232.63.187/32",
        "52.23.63.224/27",
        "186.189.238.15/32",
        "45.179.167.59/32",
        "152.207.243.123/32"
      ]
      ingress_rules = ["http-80-tcp","https-443-tcp"] 
      egress_cidr_blocks = ["0.0.0.0/0"]
      egress_rules = ["all-all"]
    }
}

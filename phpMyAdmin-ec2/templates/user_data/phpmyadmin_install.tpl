#!/bin/bash

yum install -y epel-release
yum –y update
yum install -y httpd phpmyadmin mysql
systemctl restart httpd.service

variable global_tags {
    default = {
      "Owner" = "Cliente"
    }
}

variable asg_global_tags {
    default = [
      {
        "key" = "Owner"
        "value" = "Cliente"
        "propagate_at_launch" = "true"
      },
    ]
}

variable "workspace_name_prefix" {
}

variable "circular_php_cert_body"{
}

variable "circular_php_cert_key"{
}

variable "circular_php_cert_chain"{
}

variable "image_id" {
}

variable "key_name" {
}

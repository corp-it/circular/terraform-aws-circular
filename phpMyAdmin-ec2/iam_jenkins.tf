
################role policy for Jenkins

resource "aws_iam_role" "jenkins-instance-role" {
  name = "Jenkins-php-Instance-Role"
  assume_role_policy = "${data.aws_iam_policy_document.jenkins-instance-role-policy.json}"
}

data "aws_iam_policy_document" "jenkins-instance-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "ec2.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  } 
}

##########################

resource "aws_iam_role_policy_attachment" "jenkins-instance-role-policy" {
  role       = "${aws_iam_role.jenkins-instance-role.name}"
  policy_arn = "${aws_iam_policy.Jenkins-Instance-policy.arn}"
}

resource "aws_iam_policy" "Jenkins-Instance-policy" {
  name    = "Jenkins-php-Instance-policy"
  policy  = "${data.aws_iam_policy_document.Jenkins-Instance-policy.json}"
}

data "aws_iam_policy_document" "Jenkins-Instance-policy" {
  statement {
    effect  = "Allow"
    actions = [
            "autoscaling:Describe*",
            "cloudformation:Describe*",
            "cloudformation:GetTemplate",
            "s3:Get*"
    ]
  resources = [
      "*"
  ]
 }
}
############################################

resource "aws_iam_instance_profile" "jenkins-instance-profile" {
  name = "JenkinsCodeDeploy-php-InstanceRoleInstanceProfile"
  role = "${aws_iam_role.jenkins-instance-role.name}"
}

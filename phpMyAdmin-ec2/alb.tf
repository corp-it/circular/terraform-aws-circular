resource "aws_iam_server_certificate" "alb_php_cert" {
  name_prefix       = "CLIENTE-ALB-SELF-SIGNED"
  certificate_body  = "${var.circular_php_cert_body}"
  private_key       = "${var.circular_php_cert_key}"
  certificate_chain = "${var.circular_php_cert_chain}"

  lifecycle {
    create_before_destroy = true
  }
}



locals {
  https_listeners_phpMyAdmin = [{
    certificate_arn = "${aws_iam_server_certificate.alb_php_cert.arn}",
    port            = 443
  }]
  
  extra_ssl_certs   = [{
    certificate_arn = "${aws_iam_server_certificate.alb_php_cert.arn}",
    index                 = 0
    https_listener_index  = 0 
  }]  
}

########################ALB-phpMyAdmin

module "alb-phpMyAdmin" {
  source                        = "corpit-consulting-public/elb/aws"
  version                       = "v3.12.2"
  source                        = "./modules/terraform-aws-alb"
  load_balancer_type            = "application"
  logging_enabled               = "false"
  load_balancer_name            = "circular-php-MyAdmin"
  security_groups               = ["${module.sg_alb_phpMyAdmin_http.this_security_group_id}"]
  subnets                       = "${data.terraform_remote_state.circular_vpc.vpc_public_subnets}"
  tags                          = "${merge(var.alb_tags_phpMyAdmin,var.global_tags)}"
  vpc_id                        = "${data.terraform_remote_state.circular_vpc.vpc_id}"
  https_listeners               = "${local.https_listeners_phpMyAdmin}"
  https_listeners_count         = "${var.https_listeners_count_phpMyAdmin}"
  http_tcp_listeners            = "${var.http_listener_phpMyAdmin}"
  http_tcp_listeners_count      = "${var.http_listeners_count_phpMyAdmin}"
  target_groups                 = "${var.target_groups_phpMyAdmin}"
  target_groups_count           = "${var.target_groups_count_phpMyAdmin}" 
  enable_cross_zone_load_balancing = "true"
}


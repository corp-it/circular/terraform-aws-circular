module "cloudfront-s3-cdn" {
  source              = "juanConversalab/cloudfront-s3-cdn/aws"
  version             = "0.36.1"
  origin_bucket       = aws_s3_bucket.cdn_bucket.bucket
  name                = var.cdn_name
  acm_certificate_arn = aws_acm_certificate.circular_cdn_certificate.id
  aliases             = [var.aliases]
  stage               = var.stage
  parent_zone_id      = aws_route53_zone.appcircular_com.zone_id
  #parent_zone_id      = ""

}

resource "aws_route53_zone" "appcircular_com" {
  name = var.rout_53_name
}

resource "aws_s3_bucket" "cdn_bucket" {
    bucket = var.bucket_name
    acl    = "private"

    cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = var.allowed_origins
  }
}

resource "aws_acm_certificate" "circular_cdn_certificate" {
  certificate_body  = var.circular_cert_body
  certificate_chain = var.circular_cert_chain
  private_key       = var.circular_cert_key

  lifecycle {
    create_before_destroy = true
  }
}
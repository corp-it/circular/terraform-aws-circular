variable "workspace_name_prefix" {
}

variable "circular_cert_body" {
}

variable "circular_cert_key" {
}

variable "circular_cert_chain" {
}

variable "rout_53_name" {
}

variable "bucket_name" {
}

variable "cdn_name" {
}

variable "stage" {
}

variable "aliases" {
}

variable "allowed_origins" {
    
}

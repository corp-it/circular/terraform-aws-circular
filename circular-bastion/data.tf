
data "terraform_remote_state" "circular_vpc" {
     backend = "remote"
     config  = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-vpc-${var.workspace_name_prefix}"
       }
     }
}

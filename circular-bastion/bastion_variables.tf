variable bastion_params {
	default = {
    bucket_name               = "circular-dev-ssh-public-keys"
    is_lb_private             = "false"
    host_key_pair             = "aws_circular_ssh"
    create_dns_record         = 0
    launch_configuration_name = "circular-bastion-lc"
    image_id                  = "ami-060c92c70bea517bd"
  }
}

variable bastion_tags {
    default = {
      "Name"        = "circular-bastion"
      "Description" = "Main Bastion Instance"
    }
}

variable "bastion_instance_count" {
    default = 1
}

variable "cidrs" {
  type = "list"
  default = [
      "181.45.75.229/32",
      "181.230.132.242/32",
      "190.16.55.228/32",
      "24.232.63.187/32",
      "181.45.172.243/32",
      "190.16.85.5/32",
  ]
}

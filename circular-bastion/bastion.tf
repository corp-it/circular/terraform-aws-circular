module "bastion" {
  "source"                            = "juanConversalab/bastion/aws"
  "version"                           = "v1.2.2"
  "bucket_name"                       = "${var.bucket_name}"
  "region"                            = "${local.region}"
  "vpc_id"                            = "${data.terraform_remote_state.circular_vpc.vpc_id}"
  "is_lb_private"                     = "${var.bastion_params["is_lb_private"]}"
  "bastion_host_key_pair"             = "${var.bastion_host_key_pair}"
  "bastion_launch_configuration_name" = "${var.bastion_params["launch_configuration_name"]}"
  "create_dns_record"                 = "${var.bastion_params["create_dns_record"]}"
  "elb_subnets"                       = "${data.terraform_remote_state.circular_vpc.vpc_public_subnets}"
  "auto_scaling_group_subnets"        = "${data.terraform_remote_state.circular_vpc.vpc_public_subnets}"
  "tags"                              = "${merge(var.bastion_tags,var.global_tags)}"
  "bastion_instance_count"            = "${var.bastion_instance_count}"
  "image_id"                          = "${var.image_id}"
  "cidrs"                             = ["${var.cidrs}"]
}

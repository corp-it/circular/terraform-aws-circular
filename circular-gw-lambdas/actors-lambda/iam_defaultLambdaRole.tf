################role policy for defaultLambdaRole

resource "aws_iam_role" "Circular-default-Lambda-Role" {
  name               = "Circular_Default_Lambda_Rol_policy"
  assume_role_policy = data.aws_iam_policy_document.Default-Lambda-assume-role-policy.json
}

data "aws_iam_policy_document" "Default-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

#######################################Lambda-Basic-Execution

resource "aws_iam_role_policy_attachment" "Circular-Lambda-Basic-Execution-role-policy" {
  role       = aws_iam_role.Circular-default-Lambda-Role.name
  policy_arn = aws_iam_policy.Circular-Lambda-Basic-Execution-policy.arn
}

resource "aws_iam_policy" "Circular-Lambda-Basic-Execution-policy" {
  name   = "Circular_Lambda_Basic_Execution-policy"
  policy = data.aws_iam_policy_document.Lambda-Basic-Execution-Circular-policy.json
}

data "aws_iam_policy_document" "Lambda-Basic-Execution-Circular-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "*",
    ]
  }
}

###################################Cognito-Admin-Create-User

resource "aws_iam_role_policy_attachment" "Cognito-Admin-Create-User-role-policy" {
  role       = aws_iam_role.Circular-default-Lambda-Role.name
  policy_arn = aws_iam_policy.Cognito-Admin-Create-User-policy.arn
}

resource "aws_iam_policy" "Cognito-Admin-Create-User-policy" {
  name   = "Cognito_Admin_Create_User_Circular_policy"
  policy = data.aws_iam_policy_document.Cognito-Admin-Create-User-Circular-policy.json
}

data "aws_iam_policy_document" "Cognito-Admin-Create-User-Circular-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "cognito-idp:AdminCreateUser",
    ]
    resources = [
      "*",
    ]
  }
}

################################################### lambdaInvoke

resource "aws_iam_role_policy_attachment" "Circular-lambda-Invoke-role-policy" {
  role       = aws_iam_role.Circular-default-Lambda-Role.name
  policy_arn = aws_iam_policy.LambdaInvoke-policy.arn
}

resource "aws_iam_policy" "LambdaInvoke-policy" {
  name   = "Circular_Lambda_Invoke_policy"
  policy = data.aws_iam_policy_document.circular-LambdaInvoke_policy.json
}

data "aws_iam_policy_document" "circular-LambdaInvoke_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "lambda:InvokeFunction",
    ]
    resources = [
      "*",
    ]
  }
}

####################################Cognito-Admin-Create-User-Attributes

resource "aws_iam_role_policy_attachment" "Cognito-Admin-Create-User-Attributes-role-policy" {
  role       = aws_iam_role.Circular-default-Lambda-Role.name
  policy_arn = aws_iam_policy.Cognito-Admin-Create-User-Attributes-policy.arn
}

resource "aws_iam_policy" "Cognito-Admin-Create-User-Attributes-policy" {
  name   = "Cognito_Admin_Create_User_Attributes_Circular_policy"
  policy = data.aws_iam_policy_document.Admin-Update-User-Attributes-Circular-policy.json
}

data "aws_iam_policy_document" "Admin-Update-User-Attributes-Circular-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "cognito-idp:AdminUpdateUserAttributes",
    ]
    resources = [
      "*",
    ]
  }
}

#####################################Policies for AWS

resource "aws_iam_role_policy_attachment" "AmazonRDSFullAccess-policy" {
  role       = aws_iam_role.Circular-default-Lambda-Role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonRDSFullAccess"
}

resource "aws_iam_role_policy_attachment" "CloudWatchLogsFullAccess-policy" {
  role       = aws_iam_role.Circular-default-Lambda-Role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}


###########################FirebaseAdmin

variable "layer_params_1" {
  type = map(string)
  default = {
    layer_name  = "FirebaseAdmin"
    description = "Firebase Admin for Notifications v7.2.0"
    s3_bucket   = "circular-lambda-artifacts-dev"
    s3_key      = "layers/FirebaseAdmin.zip"
  }
}

variable "compatible_runtimes_1" {
  type    = list(string)
  default = ["nodejs14.x"]
}

###########################FirebaseAdmin

variable "layer_params_2" {
  type = map(string)
  default = {
    layer_name  = "AwsSdkJs"
    description = "Firebase Admin for Notifications v7.2.0"
    s3_bucket   = "circular-lambda-artifacts-dev"
    s3_key      = "layers/AwsSdkJs.zip"
  }
}

variable "compatible_runtimes_2" {
  type    = list(string)
  default = ["nodejs14.x"]
}

########################### Uuid

variable "layer_params_3" {
  type = map(string)
  default = {
    layer_name  = "Uuid"
    description = "Layer para user uuid en node 10"
    s3_bucket   = "circular-lambda-artifacts-prod"
    s3_key      = "layers/Uuid.zip"
  }
}

variable "compatible_runtimes_3" {
  type    = list(string)
  default = ["nodejs14.x"]
}

########################## Parse5

variable "layer_params_4" {
  type = map(string)
  default = {
    layer_name  = "Parse5"
    description = "Libreria para Parsear html"
    s3_bucket   = "circular-lambda-artifacts-prod"
    s3_key      = "layers/Parse5.zip"
  }
}

variable "compatible_runtimes_4" {
  type    = list(string)
  default = ["nodejs14.x"]
}

############################# itemFactory
##
###variable "layer_params_7" {
###  type    = "map"
###  default = {
###    layer_name  = "itemFactory"
###    description = "Dynamo Item factory"
###    s3_bucket   = "circular-lambda-artifacts-dev"
###    s3_key      = "layers/Parse5.zip"
###  }
###}
##
###variable "compatible_runtimes_7" {
###  type     = "list"   
###   default = ["nodejs14.x",
###              "nodejs8.10", 
###             ]
###}

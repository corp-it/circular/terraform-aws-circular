variable "global_tags" {
  default = {
    "Owner" = "Cliente"
  }
}

variable "asg_global_tags" {
  default = [
    {
      "key"                 = "Owner"
      "value"               = "Cliente"
      "propagate_at_launch" = "true"
    },
  ]
}

variable "workspace_name_prefix" {
}

###Lambdas

variable "s3_obj_sendEventsAPIAdapters" {
}

variable "s3_obj_processGeofenceEvent" {
}

variable "s3_obj_lnhDriver" {
}

variable "s3_obj_actorsApiLog" {
}

variable "s3_obj_getQuicksightDashboardUrl" {
}

variable "s3_obj_sendNotifications" {
}

variable "s3_obj_deleteNotificationTokens" {
}

variable "s3_obj_StartConversation" {
}

###Layers

variable "s3_obj_FirebaseAdmin" {
}

variable "s3_obj_wsSdkJs" {
}

variable "s3_obj_Uuid" {
}

variable "s3_obj_Parse5" {
}

######Stage
##
##variable "stage_name" {
##}

variable "conversalabSiteUrl" {
}

variable "FirebaseFile" {
}

variable "FirebaseUrl" {
}

variable "NotificationWebLink" {
}

##### Variables for iam names

variable "facilitiesadminauth_role_name" {
}

variable "circular_cognitoUserPool_lambda_role_name" {
}

variable "lambda_basic_exe_role_name" {
}

variable "policy_cognito_name" {
}

variable "Policy_oneclick_name" {
}

variable "lambda_basic_execution_name" {
}

variable "s3_obj_getLnhCnrt" {
}


#######################Layer FirebaseAdmin

module "lambda_layer_circular_FirebaseAdmin" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v2.0.2"
  layer_name          = var.layer_params_1["layer_name"]
  description         = var.layer_params_1["description"]
  s3_object_version   = var.s3_obj_FirebaseAdmin
  s3_bucket           = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key              = var.layer_params_1["s3_key"]
  compatible_runtimes = var.compatible_runtimes_1
}

#######################AwsSdkJs

module "lambda_layer_circular_AwsSdkJs" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v2.0.2"
  layer_name          = var.layer_params_2["layer_name"]
  description         = var.layer_params_2["description"]
  s3_object_version   = var.s3_obj_wsSdkJs
  s3_bucket           = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key              = var.layer_params_2["s3_key"]
  compatible_runtimes = var.compatible_runtimes_2
}

#######################Uuid

module "lambda_layer_circular_Uuid" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v2.0.2"
  layer_name          = var.layer_params_3["layer_name"]
  description         = var.layer_params_3["description"]
  s3_object_version   = var.s3_obj_Uuid
  s3_bucket           = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key              = var.layer_params_3["s3_key"]
  compatible_runtimes = var.compatible_runtimes_3
}

#######################Parse5

module "lambda_layer_circular_Parse5" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v2.0.2"
  layer_name          = var.layer_params_4["layer_name"]
  description         = var.layer_params_4["description"]
  s3_object_version   = var.s3_obj_Parse5
  s3_bucket           = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key              = var.layer_params_4["s3_key"]
  compatible_runtimes = var.compatible_runtimes_4
}

#########################itemFactory
##
##module "lambda_layer_circular_itemFactory" {
##  source              = "./modules/terraform-aws-lambda-layer-version"
##  layer_name          = "${var.layer_params_7["layer_name"]}"
##  description         = "${var.layer_params_7["description"]}"
##  s3_bucket           = "${var.layer_params_7["s3_bucket"]}"
##  s3_key              = "${var.layer_params_7["s3_key"]}"
##  compatible_runtimes = ["${var.compatible_runtimes_7}"]
##}

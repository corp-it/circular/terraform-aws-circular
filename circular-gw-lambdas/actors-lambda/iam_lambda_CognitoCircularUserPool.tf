################role policy for CognitoUserPool

resource "aws_iam_role" "Circular-CognitoUserPool-Lambda-Role" {
  name               = var.circular_cognitoUserPool_lambda_role_name
  assume_role_policy = data.aws_iam_policy_document.CognitoUserPool-Lambda-assume-role-policy.json
}

data "aws_iam_policy_document" "CognitoUserPool-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

##############################circular-lambda-basic-execution-policy

resource "aws_iam_role_policy_attachment" "lambda-circular-basic-execution-role-policy" {
  role       = aws_iam_role.Circular-CognitoUserPool-Lambda-Role.name
  policy_arn = aws_iam_policy.lambda-circular-basic-execution-policy.arn
}

resource "aws_iam_policy" "lambda-circular-basic-execution-policy" {
  name   = var.lambda_basic_execution_name
  policy = data.aws_iam_policy_document.lambda-circular-basic-execution-policy.json
}

data "aws_iam_policy_document" "lambda-circular-basic-execution-policy" {
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/ProcessAppSyncNewMessage:*",
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/ProcessAppSyncNewMessageTest:*",
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/*:*",
    ]
  }
}

##############################

resource "aws_iam_role_policy_attachment" "Cognito-User-Pool-Circular-role-policy" {
  role       = aws_iam_role.Circular-CognitoUserPool-Lambda-Role.name
  policy_arn = aws_iam_policy.Circular-Cognito-User-Pool-policy.arn
}

resource "aws_iam_policy" "Circular-Cognito-User-Pool-policy" {
  name   = var.policy_cognito_name
  policy = data.aws_iam_policy_document.Cognito-Circular-User-Pool-policy.json
}

data "aws_iam_policy_document" "Cognito-Circular-User-Pool-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "cognito-idp:AdminGetDevice",
      "cognito-idp:AdminCreateUser",
      "cognito-idp:ListIdentityProviders",
      "cognito-idp:AdminSetUserSettings",
      "cognito-idp:GetIdentityProviderByIdentifier",
      "cognito-idp:GetUICustomization",
      "cognito-idp:AdminGetUser",
      "cognito-idp:ListUserPoolClients",
      "cognito-idp:ListUsersInGroup",
      "cognito-idp:DescribeUserPool",
      "cognito-idp:AdminListUserAuthEvents",
      "cognito-idp:ListGroups",
      "cognito-idp:ListResourceServers",
      "cognito-idp:AdminListDevices",
      "cognito-idp:DescribeIdentityProvider",
      "cognito-idp:DescribeResourceServer",
      "cognito-idp:GetUserAttributeVerificationCode",
      "cognito-idp:DescribeUserImportJob",
      "cognito-idp:DescribeUserPoolClient",
      "cognito-idp:GetSigningCertificate",
      "cognito-idp:GetUser",
      "cognito-idp:SetUserSettings",
      "cognito-idp:GetCSVHeader",
      "cognito-idp:ListDevices",
      "cognito-idp:GetUserPoolMfaConfig",
      "cognito-idp:GetGroup",
      "cognito-idp:DescribeRiskConfiguration",
      "cognito-idp:GetDevice",
      "cognito-idp:UpdateUserAttributes",
      "cognito-idp:DescribeUserPoolDomain",
      "cognito-idp:ChangePassword",
      "cognito-idp:AdminListGroupsForUser",
      "cognito-idp:ListUserImportJobs",
      "cognito-idp:ListUsers",
    ]
    resources = [
      "arn:aws:cognito-idp:us-east-1:${data.aws_caller_identity.current.account_id}:userpool/us-east-1_EDII3tg7D",
    ]
  }
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "cognito-idp:ListUserPools",
    ]
    resources = [
      "*",
    ]
  }
}

################################AWSLambdaRole

resource "aws_iam_role_policy_attachment" "AWSLambda-role-policy-attach" {
  role       = aws_iam_role.Circular-CognitoUserPool-Lambda-Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}


#################################registerFirebaseToken

module "registerFirebaseToken" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_1["function_name"]
  role              = aws_iam_role.AppSyncDynamoReader.arn
  description       = var.lambda_params_1["description"]
  memory_size       = var.lambda_params_1["memory_size"]
  handler           = var.lambda_params_1["handler"]
  runtime           = var.lambda_params_1["runtime"]
  timeout           = var.lambda_params_1["timeout"]
  mode              = var.lambda_params_1["mode"]
  s3_object_version = var.s3_obj_registerFirebaseToken
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_1["s3_key"]
  environment = {
    variables = {
      UsersTable = data.terraform_remote_state.circular_dynamodb.outputs.usersTable
    }
  }
}

###################################Lambda Function uploadImageLocation

module "uploadImageLocation" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_2["function_name"]
  role              = aws_iam_role.LambdaS3FullAccess.arn
  memory_size       = var.lambda_params_2["memory_size"]
  handler           = var.lambda_params_2["handler"]
  runtime           = var.lambda_params_2["runtime"]
  timeout           = var.lambda_params_2["timeout"]
  mode              = var.lambda_params_2["mode"]
  s3_object_version = var.s3_obj_uploadImageLocation
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_2["s3_key"]
  environment = {
    variables = {
      circularBucketPublicBaseUrl = var.circularBucketPublicBaseUrl
      srcBucket                   = data.terraform_remote_state.circular_s3.outputs.s3_uploads_name
      dstBucket                   = var.dstBucket
    }
  }
  layers = ["${module.lambda_layer_circular_Sharp.layer_arn}:${module.lambda_layer_circular_Sharp.version}"]
}

####################################takeWebsiteScreenshot

module "takeWebsiteScreenshot" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_3["function_name"]
  role              = aws_iam_role.LambdaS3FullAccess.arn
  memory_size       = var.lambda_params_3["memory_size"]
  handler           = var.lambda_params_3["handler"]
  runtime           = var.lambda_params_3["runtime"]
  timeout           = var.lambda_params_3["timeout"]
  mode              = var.lambda_params_3["mode"]
  s3_object_version = var.s3_obj_takeWebsiteScreenshot
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_3["s3_key"]
  environment = {
    variables = {
      screenshotTimeout = "15"
      HOME              = "/var/task"
    }
  }
  layers = ["${module.lambda_layer_circular_PhantomJsBinnaries.layer_arn}:${module.lambda_layer_circular_PhantomJsBinnaries.version}"]
}

#################################sendMessageToManyAPIAdapter

module "sendMessageToManyAPIAdapter" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_4["function_name"]
  role              = aws_iam_role.sendMessageToManyAPIAdapter.arn
  memory_size       = var.lambda_params_4["memory_size"]
  handler           = var.lambda_params_4["handler"]
  runtime           = var.lambda_params_4["runtime"]
  timeout           = var.lambda_params_4["timeout"]
  mode              = var.lambda_params_4["mode"]
  s3_object_version = var.s3_obj_sendMessageToManyAPIAdapter
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_4["s3_key"]
  environment = {
    variables = {
      sendMessageToManyLambda = module.sendMessageToMany.id
    }
  }
}

#################################sendMessageToMany

module "sendMessageToMany" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_5["function_name"]
  role              = aws_iam_role.Lambda-With-Dynamodb-Access.arn
  memory_size       = var.lambda_params_5["memory_size"]
  handler           = var.lambda_params_5["handler"]
  runtime           = var.lambda_params_5["runtime"]
  timeout           = var.lambda_params_5["timeout"]
  mode              = var.lambda_params_5["mode"]
  s3_object_version = var.s3_obj_sendMessageToMany
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_5["s3_key"]
  environment = {
    variables = {
      CircularBot            = var.CircularBot
      UserConversationsTable = data.terraform_remote_state.circular_dynamodb.outputs.userConversationsTable
      MessagesTable          = data.terraform_remote_state.circular_dynamodb.outputs.messagesTable
    }
  }
  layers        = ["${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_arn}:${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_version}"]
}

#################################s3temporaryPresignedURL

module "s3temporaryPresignedURL" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_6["function_name"]
  role              = aws_iam_role.LambdaS3FullAccess.arn
  memory_size       = var.lambda_params_6["memory_size"]
  handler           = var.lambda_params_6["handler"]
  runtime           = var.lambda_params_6["runtime"]
  timeout           = var.lambda_params_6["timeout"]
  mode              = var.lambda_params_6["mode"]
  s3_object_version = var.s3_obj_s3temporaryPresignedURL
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_6["s3_key"]
  environment = {
    variables = {
      S3_BUCKET = data.terraform_remote_state.circular_s3.outputs.s3_uploads_name
    }
  }
  layers        = ["${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_arn}:${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_version}"]
}


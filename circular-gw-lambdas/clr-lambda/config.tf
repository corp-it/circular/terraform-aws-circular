terraform {
  required_version = ">= 0.12.24"
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "Circular-qa"
    workspaces {
      prefix = "circular-gw-lambda-"
    }
  }
}

provider "aws" {
    version = "~> 3.0"
  region  = local.region
  # region  = "us-east-1"
  # profile = "tfcloud-dev"
  # profile = "tfcloud-prod"
}


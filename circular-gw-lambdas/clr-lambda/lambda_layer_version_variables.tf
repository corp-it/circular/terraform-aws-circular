variable "layer_params" {
  type = map(string)
  default = {
    layer_name  = "Sharp"
    description = "Image processing library (linux node 8.10)"
    s3_key      = "layers/Sharp.zip"
  }
}

variable "compatible_runtimes" {
  type    = list
  default = [
            "nodejs14.x",
          ]
}

###########################PhantomJsBinnaries

variable "layer_params_2" {
  type = map(string)
  default = {
    layer_name        = "PhantomJsBinnaries"
    description       = "nodejs/node_module"
    s3_key            = "layers/PhantomJsBinnaries.zip"
  }
}

variable "compatible_runtimes_2" {
  type    = list
  default = [
            "nodejs14.x",
          ]
}


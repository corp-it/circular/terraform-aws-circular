################role policy for LambdaWithDynamoAccess

resource "aws_iam_role" "Lambda-With-Dynamodb-Access" {
  name               = "Lambda_With_DynamoDB_Access_Circular-policy"
  assume_role_policy = data.aws_iam_policy_document.Lambda-With-Dynamodb-Access-assume-role-policy.json
}

data "aws_iam_policy_document" "Lambda-With-Dynamodb-Access-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

########################################Circular-DynamoDB

resource "aws_iam_role_policy_attachment" "Dynamo-DB-role-policy-attach" {
  role       = aws_iam_role.Lambda-With-Dynamodb-Access.name
  policy_arn = aws_iam_policy.Circular-dynamodb-policy.arn
}

resource "aws_iam_policy" "Circular-dynamodb-policy" {
  name   = "LambdaCircular_DynamoDB-policy"
  policy = data.aws_iam_policy_document.LambdaCircular_DynamoDB-policy.json
}

data "aws_iam_policy_document" "LambdaCircular_DynamoDB-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "dynamodb:UpdateItem",
      "dynamodb:Scan",
      "dynamodb:Query",
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
      "dynamodb:BatchWriteItem",
      "dynamodb:BatchGetItem",
    ]
    resources = [
      "*",
    ]
  }
}

#########################################AWSLambdaTracerAccessExecutionRole

resource "aws_iam_role_policy_attachment" "Lambda-Trace-Acces-Xray-Circular-role-policy-attach" {
  role       = aws_iam_role.Lambda-With-Dynamodb-Access.name
  policy_arn = aws_iam_policy.Circular-Lambda-Trace-Acces-Xray-Circular-policy.arn
}

resource "aws_iam_policy" "Circular-Lambda-Trace-Acces-Xray-Circular-policy" {
  name   = "LambdaTraceAcces_XrayCircular-policy"
  policy = data.aws_iam_policy_document.LambdaTracer_Access_Execution_xray_Circular-policy.json
}

data "aws_iam_policy_document" "LambdaTracer_Access_Execution_xray_Circular-policy" {
  statement {
    effect = "Allow"
    actions = [
      "xray:PutTraceSegments",
      "xray:PutTelemetryRecords",
    ]
    resources = [
      "*",
    ]
  }
}

#########################################################Policies of AWS

resource "aws_iam_role_policy_attachment" "AWSXrayWriteOnlyAccess-role-policy-attach" {
  role       = aws_iam_role.Lambda-With-Dynamodb-Access.name
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "AWSOpsWorksCloudWatchLogs-role-policy-attach" {
  role       = aws_iam_role.Lambda-With-Dynamodb-Access.name
  policy_arn = "arn:aws:iam::aws:policy/AWSOpsWorksCloudWatchLogs"
}


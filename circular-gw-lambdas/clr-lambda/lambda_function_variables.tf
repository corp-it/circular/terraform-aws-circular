############################################registerFirebaseToken

variable "lambda_params_1" {
  type = map(string)
  default = {
    function_name     = "registerFirebaseToken"
    memory_size       = "128"
    handler           = "index.handler"
    description       = "Register Token on Dynamo Table"
    runtime           = "nodejs14.x"
    timeout           = "6"
    mode              = "PassThrough"
    s3_key            = "registerFirebaseToken.zip"
    s3_object_version = "4W_QFqfpAnbjjEEeartI5TTzpoK8S5le"
  }
}

########################################uploadImageLocation

variable "lambda_params_2" {
  type = map(string)
  default = {
    function_name     = "uploadImageLocation"
    memory_size       = "256"
    handler           = "index.handler"
    runtime           = "nodejs12.x"
    timeout           = "3"
    mode              = "PassThrough"
    s3_key            = "uploadImageLocation.zip"
    s3_object_version = "AkPF38DnsaipS334YdZG0OZ8Wm3o0ccN"
  }
}

########################################takeWebsiteScreenshot

variable "lambda_params_3" {
  type = map(string)
  default = {
    function_name     = "takeWebsiteScreenshot"
    memory_size       = "1536"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "30"
    mode              = "PassThrough"
    s3_key            = "takeWebsiteScreenshot.zip"
    s3_object_version = "NwOHRJyNfA9ymqIHqwSfk4fWWtMmpsFS"
  }
}

############################################sendMessageToManyAPIAdapter

variable "lambda_params_4" {
  type = map(string)
  default = {
    function_name     = "sendMessageToManyAPIAdapter"
    memory_size       = "1024"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "3"
    mode              = "PassThrough"
    s3_key            = "sendMessageToManyAPIAdapter.zip"
    s3_object_version = "zb3aMEwSq3qxXlbVSxTEBvqk75Rjcfa5"
  }
}

############################################sendMessageToMany

variable "lambda_params_5" {
  type = map(string)
  default = {
    function_name     = "sendMessageToMany"
    memory_size       = "1024"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "30"
    mode              = "PassThrough"
    s3_key            = "sendMessageToMany.zip"
    s3_object_version = "Ls9kZ18u5JgClLIhJazfdZ.4g2rxbKmx"
  }
}

############################################s3temporaryPresignedURL

variable "lambda_params_6" {
  type = map(string)
  default = {
    function_name     = "s3temporaryPresignedURL"
    memory_size       = "128"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "3"
    mode              = "PassThrough"
    s3_key            = "s3temporaryPresignedURL.zip"
    s3_object_version = "Ls9kZ18u5JgClLIhJazfdZ.4g2rxbKmx"
  }
}


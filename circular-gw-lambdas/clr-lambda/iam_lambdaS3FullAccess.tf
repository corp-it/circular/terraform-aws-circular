################role policy for LambdaS3FullAccess

resource "aws_iam_role" "LambdaS3FullAccess" {
  name               = "LambdaS3_Full_Access_policy"
  assume_role_policy = data.aws_iam_policy_document.circular-lambda-assume-role-policy.json
}

data "aws_iam_policy_document" "circular-lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

##############################LambdaBasicExecution

resource "aws_iam_role_policy_attachment" "LambdaBasicExecution-role-policy-attach" {
  role       = aws_iam_role.LambdaS3FullAccess.name
  policy_arn = aws_iam_policy.Circular-LambdaBasicExecutionRole-policy.arn
}

resource "aws_iam_policy" "Circular-LambdaBasicExecutionRole-policy" {
  name   = "lambdaBasic_ExecutionCircular"
  policy = data.aws_iam_policy_document.lambdaBasic_Execution_Circular_policy.json
}

data "aws_iam_policy_document" "lambdaBasic_Execution_Circular_policy" {
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "*",
    ]
  }
}

#########################Policy of AWS

resource "aws_iam_role_policy_attachment" "LambdaRole-role-policy-attach" {
  role       = aws_iam_role.LambdaS3FullAccess.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}

resource "aws_iam_role_policy_attachment" "AmazonS3FullAcces-role-policy-attach" {
  role       = aws_iam_role.LambdaS3FullAccess.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}


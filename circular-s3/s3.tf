resource "aws_s3_bucket" "lambda-s3" {
  bucket = "${var.bucket_name_lambda}"
  acl    = "private"

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "circular-uploads" {
  bucket = "${var.bucket_name_uploads}"
  acl    = "private"

  versioning {
    enabled = true
  }
    cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["POST"]
    allowed_origins = ["https://*.appcircular.com"]
  }
}


resource "aws_s3_bucket" "app-circular" {
  bucket = "${var.bucket_name_app}"
  acl    = "private"

  versioning {
    enabled = true
  }
}


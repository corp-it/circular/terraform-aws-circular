##############S3 Output

output "s3_name" {
    value = "${aws_s3_bucket.lambda-s3.id}"
}

output "s3_uploads_name" {
    value = "${aws_s3_bucket.circular-uploads.id}"
}

output "s3_app_circular_name" {
    value = "${aws_s3_bucket.app-circular.id}"
}


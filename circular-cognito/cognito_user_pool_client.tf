module "cognito-client-WebAppClient" {
  source                               = "corpit-consulting-public/cognito-user-pool-client-mod/aws"
  version                              = "v0.1.2"
  name                                 = "${var.cognito_client_name_1}"
  user_pool_id                         = "${module.Circular-User-Pool.id}"
  read_attributes                      = "${var.read_attributes}"
  write_attributes                     = "${var.write_attributes}"
  supported_identity_providers         = "${var.supported_identity_providers}"
  callback_urls                        = "${var.callback_urls_WebAppClient}"
  logout_urls                          = "${var.logout_urls_WebAppClient}"
  allowed_oauth_flows                  = ["${var.allowed_oauth_flows}"]
  refresh_token_validity               = "${var.cognito_client_params["refresh_token_validity"]}"
  allowed_oauth_flows_user_pool_client = "${var.cognito_client_params["allowed_oauth_flows_user_pool_client"]}"
  allowed_oauth_scopes                 = "${var.allowed_oauth_scopes}"
  explicit_auth_flows                  = "${var.explicit_auth_flows_1}"
}

########################################

module "cognito-client-AndroidAppClient" {
  source                               = "corpit-consulting-public/cognito-user-pool-client-mod/aws"
  version                              = "v0.1.2"
  name                                 = "${var.cognito_client_name_2}"
  user_pool_id                         = "${module.Circular-User-Pool.id}"
  read_attributes                      = "${var.read_attributes_2}"
  write_attributes                     = "${var.write_attributes_2}"
  supported_identity_providers         = "${var.supported_identity_providers_2}"
  callback_urls                        = "${var.callback_urls_AndroidAppClient}"
  logout_urls                          = "${var.logout_urls_AndroidAppClient}"
  allowed_oauth_flows                  = ["${var.allowed_oauth_flows_2}"]
  refresh_token_validity               = "${var.cognito_client_params_2["refresh_token_validity"]}"
  allowed_oauth_flows_user_pool_client = "${var.cognito_client_params_2["allowed_oauth_flows_user_pool_client"]}"
  allowed_oauth_scopes                 = "${var.allowed_oauth_scopes_2}"
  explicit_auth_flows                  = "${var.explicit_auth_flows_2}"
}

########################################

module "cognito-client-AdminPlanta" {
  source                               = "corpit-consulting-public/cognito-user-pool-client-mod/aws"
  version                              = "v0.1.2"
  name                                 = "${var.cognito_client_name_3}"
  user_pool_id                         = "${module.Circular-User-Pool.id}"
  read_attributes                      = "${var.read_attributes_3}"
  write_attributes                     = "${var.write_attributes_3}"
  supported_identity_providers         = "${var.supported_identity_providers_3}"
  callback_urls                        = "${var.callback_urls_AdminPlanta}"
  logout_urls                          = "${var.logout_urls_AdminPlanta}"
  allowed_oauth_flows                  = ["${var.allowed_oauth_flows_3}"]
  refresh_token_validity               = "${var.cognito_client_params_3["refresh_token_validity"]}"
  allowed_oauth_flows_user_pool_client = "${var.cognito_client_params_3["allowed_oauth_flows_user_pool_client"]}"
  allowed_oauth_scopes                 = "${var.allowed_oauth_scopes_3}"
  explicit_auth_flows                  = "${var.explicit_auth_flows_3}"
}

########################################

module "cognito-client-WebAdmin" {
  source                               = "corpit-consulting-public/cognito-user-pool-client-mod/aws"
  version                              = "v0.1.2"
  name                                 = "${var.cognito_client_name_4}"
  user_pool_id                         = "${module.Circular-User-Pool.id}"
  read_attributes                      = "${var.read_attributes_4}"
  write_attributes                     = "${var.write_attributes_4}"
  supported_identity_providers         = "${var.supported_identity_providers_4}"
  callback_urls                        = "${var.callback_urls_WebAdmin}"
  logout_urls                          = "${var.logout_urls_WebAdmin}"
  allowed_oauth_flows                  = ["${var.allowed_oauth_flows_4}"]
  refresh_token_validity               = "${var.cognito_client_params_4["refresh_token_validity"]}"
  allowed_oauth_flows_user_pool_client = "${var.cognito_client_params_4["allowed_oauth_flows_user_pool_client"]}"
  allowed_oauth_scopes                 = "${var.allowed_oauth_scopes_4}"
  explicit_auth_flows                  = "${var.explicit_auth_flows_4}"
}

########################################

module "cognito-client-WebAcceso" {
  source                               = "corpit-consulting-public/cognito-user-pool-client-mod/aws"
  version                              = "v0.1.2"
  name                                 = "${var.cognito_client_name_5}"
  user_pool_id                         = "${module.Circular-User-Pool.id}"
  read_attributes                      = "${var.read_attributes_5}"
  write_attributes                     = "${var.write_attributes_5}"
  supported_identity_providers         = "${var.supported_identity_providers_5}"
  callback_urls                        = "${var.callback_urls_WebAcceso}"
  logout_urls                          = "${var.logout_urls_WebAcceso}"
  allowed_oauth_flows                  = ["${var.allowed_oauth_flows_5}"]
  refresh_token_validity               = "${var.cognito_client_params_5["refresh_token_validity"]}"
  allowed_oauth_flows_user_pool_client = "${var.cognito_client_params_5["allowed_oauth_flows_user_pool_client"]}"
  allowed_oauth_scopes                 = "${var.allowed_oauth_scopes_5}"
  explicit_auth_flows                  = "${var.explicit_auth_flows_5}"
}

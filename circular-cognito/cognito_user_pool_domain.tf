module "circular-cognito-user-pool-domain" {
  source       = "corpit-consulting-public/cognito-user-pool-domain-mod/aws"
  version      = "v0.1.0"
  domain       = "${var.domain}"
  user_pool_id = "${module.Circular-User-Pool.id}"
}



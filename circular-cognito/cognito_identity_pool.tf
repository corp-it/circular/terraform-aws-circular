module "FacilitiesAdmin" {
  source                           = "corpit-consulting-public/cognito-identity-pool-mod/aws"
  version                          = "v0.2.0"
  identity_pool_name               = "${var.FacilitiesAdmin_params["identity_pool_name"]}"
  allow_unauthenticated_identities = "${var.FacilitiesAdmin_params["allow_unauthenticated_identities"]}"
  client_id                        = "${module.cognito-client-AdminPlanta.id}"
  provider_name                    = "${module.Circular-User-Pool.endpoint}"
  server_side_token_check          = "${var.FacilitiesAdmin_params["server_side_token_check"]}"
}

module "CircularIdentityPool" {
  source                           = "corpit-consulting-public/cognito-identity-pool-mod/aws"
  version                          = "v0.2.0"
  identity_pool_name               = "${var.CircularIdentityPool_params["identity_pool_name"]}"
  allow_unauthenticated_identities = "${var.CircularIdentityPool_params["allow_unauthenticated_identities"]}"
  client_id                        = "${module.cognito-client-WebAppClient.id}"
  provider_name                    = "${module.Circular-User-Pool.endpoint}"
  server_side_token_check          = "${var.CircularIdentityPool_params["server_side_token_check"]}"
}

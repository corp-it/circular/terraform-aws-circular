terraform {
    required_version = ">= 0.11.14"
    backend "remote" {
        organization = "Circular-qa"
        workspaces {
            prefix = "circular-cognito-"
        }
    }    
}

provider "aws" {
    version = "v2.70.0"
    region  = "${local.region}"
   # region  = "us-east-1"
   # profile = "tfcloud-dev"
   # profile = "tfcloud-prod"
   # profile = "tfcloud-py"
}

##################################ProdWebAppClient

variable "cognito_client_params" {
  type    = "map"
  default = {
    allowed_oauth_flows_user_pool_client = "true"
    refresh_token_validity               = 3650
  }
}

variable "cognito_client_name_1" {
}

variable "read_attributes" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "email_verified",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "phone_number_verified",
            "picture",
            "preferred_username",
            "custom:actorIntegrationId",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "write_attributes" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "picture",
            "preferred_username",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "supported_identity_providers" {
  type = "list"
  default = [
             "COGNITO"
  ]
}

variable "callback_urls_WebAppClient" {
  type = "list"
}

variable "logout_urls_WebAppClient" {
  type    = "list"
}

variable "allowed_oauth_flows" {
  type    = "list"
  default = ["code"]
}

variable "allowed_oauth_scopes" {
  type    = "list"
  default = [
            "aws.cognito.signin.user.admin",
            "openid",
            "profile",
  ]
}

variable "explicit_auth_flows_1" {
  type    = "list"
  default = [ 
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_CUSTOM_AUTH",
  ]
}

#################################Cognito_NewAndroidAppClient

variable "cognito_client_params_2" {
  type    = "map"
  default = {
    allowed_oauth_flows_user_pool_client = "true"
    refresh_token_validity               = 3650
  }
}

variable "cognito_client_name_2" {
}

variable "read_attributes_2" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "email_verified",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "phone_number_verified",
            "picture",
            "preferred_username",
            "custom:actorIntegrationId",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "write_attributes_2" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "picture",
            "preferred_username",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "supported_identity_providers_2" {
  type = "list"
  default = [
             "COGNITO"
  ]
}

variable "callback_urls_AndroidAppClient" {
  type = "list"
}

variable "logout_urls_AndroidAppClient" {
  type    = "list"
}

variable "allowed_oauth_flows_2" {
  type    = "list"
  default = ["code"]
}

variable "allowed_oauth_scopes_2" {
  type    = "list"
  default = [
            "aws.cognito.signin.user.admin",
            "openid"
  ]
}

variable "explicit_auth_flows_2" {
  type    = "list"
  default = [
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_CUSTOM_AUTH",
  ]
}


#################################Cognito_AdminPlanta

variable "cognito_client_params_3" {
  type    = "map"
  default = {
    allowed_oauth_flows_user_pool_client = "true"
    refresh_token_validity               = 3650
  }
}

variable "cognito_client_name_3" {
}

variable "read_attributes_3" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "email_verified",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "phone_number_verified",
            "picture",
            "preferred_username",
            "custom:actorIntegrationId",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "write_attributes_3" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "picture",
            "preferred_username",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "supported_identity_providers_3" {
  type = "list"
  default = [
             "COGNITO"
  ]
}

variable "callback_urls_AdminPlanta" {
  type = "list"
}

variable "logout_urls_AdminPlanta" {
  type    = "list"
}

variable "allowed_oauth_flows_3" {
  type    = "list"
  default = ["code"]
}

variable "allowed_oauth_scopes_3" {
  type    = "list"
  default = [
            "aws.cognito.signin.user.admin",
            "openid",
            "profile"
  ]
}

variable "explicit_auth_flows_3" {
  type    = "list"
  default = [
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_CUSTOM_AUTH",
  ]
}



#################################Cognito_WebAdmin

variable "cognito_client_params_4" {
  type    = "map"
  default = {
    allowed_oauth_flows_user_pool_client = "true"
    refresh_token_validity               = 30
  }
}

variable "cognito_client_name_4" {
}

variable "read_attributes_4" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "email_verified",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "phone_number_verified",
            "picture",
            "preferred_username",
            "custom:actorIntegrationId",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "write_attributes_4" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "picture",
            "preferred_username",
            "custom:actorIntegrationId",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "supported_identity_providers_4" {
  type = "list"
  default = [
             "COGNITO"
  ]
}

variable "callback_urls_WebAdmin" {
  type = "list"
}

variable "logout_urls_WebAdmin" {
  type    = "list"
}

variable "allowed_oauth_flows_4" {
  type    = "list"
  default = ["code"]
}

variable "allowed_oauth_scopes_4" {
  type    = "list"
  default = [
            "aws.cognito.signin.user.admin",
            "openid",
            "profile"
  ]
}

variable "explicit_auth_flows_4" {
  type    = "list"
  default = [
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_CUSTOM_AUTH",
  ]
}



#################################Cognito_WebAcceso

variable "cognito_client_params_5" {
  type    = "map"
  default = {
    allowed_oauth_flows_user_pool_client = "true"
    refresh_token_validity               = 30
  }
}

variable "cognito_client_name_5" {
}

variable "read_attributes_5" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "email_verified",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "phone_number_verified",
            "picture",
            "preferred_username",
            "custom:actorIntegrationId",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "write_attributes_5" {
  type    = "list"
  default = [
            "address",
            "birthdate",
            "email",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "phone_number",
            "picture",
            "preferred_username",
            "custom:actorIntegrationId",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
  ]
}

variable "supported_identity_providers_5" {
  type = "list"
  default = [
             "COGNITO"
  ]
}

variable "callback_urls_WebAcceso" {
  type = "list"
}

variable "logout_urls_WebAcceso" {
  type    = "list"
}

variable "allowed_oauth_flows_5" {
  type    = "list"
  default = ["code"]
}

variable "allowed_oauth_scopes_5" {
  type    = "list"
  default = [
            "aws.cognito.signin.user.admin",
            "openid",
            "profile"
  ]
}

variable "explicit_auth_flows_5" {
  type    = "list"
  default = [
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_CUSTOM_AUTH",
  ]
}





################role policy for cognito
resource "aws_iam_role" "cognitoSmSRole" {
  name = "CircularUserPool-Role-SMS"
  assume_role_policy = "${data.aws_iam_policy_document.cognito-assume-role-policy.json}"
}

data "aws_iam_policy_document" "cognito-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "cognito-idp.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]

    condition {
      test = "StringEquals"
      variable = "sts:ExternalId"
      values   = ["34b3248c-b5b8-4b35-9610-5612c0bcaa56"]
    }
  }
}
  
################################################# SNS Publish Policy

data "aws_iam_policy_document" "cognito-sns-publish" {
  statement {
    effect = "Allow"
    actions = [
      "sns:publish",
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "cognito-sns-publish-policy" {
  name    = "cognitoSNSPublish"
  policy  = "${data.aws_iam_policy_document.cognito-sns-publish.json}"
}

resource "aws_iam_role_policy_attachment" "cognito_sns_publish_attachment" {
  role       = "${aws_iam_role.cognitoSmSRole.name}"
  policy_arn = "${aws_iam_policy.cognito-sns-publish-policy.arn}"
}

#################################################

variable "cognito_params" {
  type    = "map"
  default = {
    sms_verification_message     = "Your verification code is {####}"
    sms_authentication_message   = "Hola, ingresa escribiendo:{####}"
    email_verification_message   = "Your verification code is {####}"
    email_verification_subject   = "Your verification code"
    default_email_option         = "CONFIRM_WITH_CODE" 
    mfa_configuration            = "OPTIONAL"
    minimum_length               =  "8"
    require_lowercase            = "false"
    require_numbers              = "false"
    require_symbols              = "false"
    require_uppercase            = "false"
    allow_admin_create_user_only = "true"
    temporary_password_validity_days = "7"
  }
}

variable "user_pool_name" {
}

variable "allow_admin_create_user_only" {
   default = "true"
}

variable "invite_message_template" {
  type = "list"
  default = [{
            sms_message   = "Tu nombre de usuario es: {username} y tu contraseña temporaria es: {####}." ,
            email_message = "Tu nombre de usuario es: {username} y tu contraseña temporaria es: {####}." ,
            email_subject = "Contraseña temporaria" ,
  }]
}

variable "schema" {
  type    = "list"
  default = [
            {
                "name"= "actorIntegrationId",
                "attribute_data_type"= "String",
                "developer_only_attribute"= false,
                "mutable"= true,
                "required"= false,
                "string_attribute_constraints"= [{
                    "min_length"= "36",
                    "max_length"= "36"
                }]
            }
    ]
}

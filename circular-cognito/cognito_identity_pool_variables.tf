variable "FacilitiesAdmin_params" {
  type    = "map"
  default = {
    identity_pool_name               = "FacilitiesAdmin"
    allow_unauthenticated_identities = "false"
    server_side_token_check          = "false"
  }
}

variable "CircularIdentityPool_params" {
  type    = "map"
  default = {
    identity_pool_name               = "CircularIdentityPool"
    allow_unauthenticated_identities = "false"
    server_side_token_check          = "false"
  }
}


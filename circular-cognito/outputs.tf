output "user_pool_id" {
  description = "The id of the user pool"
  value       = "${module.Circular-User-Pool.id}"
}

output "user_pool_arn" {
  description = "The ARN of the user pool."
  value       = "${module.Circular-User-Pool.arn}"
}

output "user_pool_endpoint" {
  description = "The endpoint name of the user pool. Example format: cognito-idp.REGION.amazonaws.com/xxxx_yyyyy"
  value       = "${module.Circular-User-Pool.endpoint}"
}

output "identity-pool_id" {
  value       = "${module.FacilitiesAdmin.id}"
}


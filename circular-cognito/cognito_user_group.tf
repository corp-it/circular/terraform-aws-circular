module "cognito-user-group" {
  source       = "corpit-consulting-public/cognito-user-group/aws"
  version      = "v0.1.0"
  name         = "${var.cognito_group_params["name"]}"
  user_pool_id = "${module.Circular-User-Pool.id}"
  precedence   = "${var.cognito_group_params["precedence"]}"
}

module "cognito-user-group-admins" {
  source       = "corpit-consulting-public/cognito-user-group/aws"
  version      = "v0.1.0"
  name         = "${var.cognito_group_params_1["name"]}"
  user_pool_id = "${module.Circular-User-Pool.id}"
  description  = "administradores"
  precedence   = "${var.cognito_group_params_1["precedence"]}"
}
  
variable "cognito_group_params" {
  type    = "map"
  default = {
    name       = "Admins"
    precedence = 1
  }
}

variable "cognito_group_params_1" {
  type    = "map"
  default = {
    name       = "admins"
    precedence = 0
  }
}

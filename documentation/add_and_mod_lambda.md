
## Lambda

* En este documento vamos a aprender a crear una nueva Funcion Lambda y a modificar una Funcion Lambda existente utilizando Terraform y Terraform Cloud.

#### Agregar nueva Funcion Lambda

* Para crear una nueva funcion lambda, Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar ejemplo `~/workspace/circular/circular-lambda/lambda_function.tf` o en caso de que esten divididas por micro-servicios ej:
`~/workspace/circular/circular-api-gateway/lambda_function.tf`

* Para agregar una lambda nueva, vamos a editar los siguientes archivos: `lambda_function.tf ` y `lambda_function_variables.tf`

* Inserte el siguiente bloque de codigo en los archivos para agregar una lambda nueva:

`lambda_function.tf`:
```hcl
module "Circular-lambda" {
  source             = "corpit-consulting-public/lambda-function-mod/aws"
  version            = "v0.2.2"
  function_name      = "${var.lambda_params_*[function_name]}"
  role               = "${var.lambda_params_*[role]}"
  memory_size        = "${var.lambda_params_*[memory_size]}"
  handler            = "${var.lambda_params_*[handler]}"
  runtime            = "${var.lambda_params_*[runtime]}"
  timeout            = "${var.lambda_params_*[timeout]}"
  mode               = "${var.lambda_params_*[mode]}"
  s3_object_version  = "${var.lambda_params_*[s3_object_version]}"
  s3_bucket          = "${var.lambda_params_*[s3_bucket]}"
  s3_key             = "${var.lambda_params_*[s3_key]}"
  has_variables      = "true"
  variables          = {
      key1 = "value1"
      key2 = "value2"
  }
  layers             = ["${var.layers}"] 
}
###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: lambda_params_1
```
_copiar el bloque y llenar las variables con los valores que necesite usar_
* _si se utiliza lambdas layer version ingresar en [`lambda_layer_version`](https://registry.terraform.io/modules/corpit-consulting-public/lambda-layer-version-mod/aws/0.1.3)_ de no utilizarce, no pasar la variable `layers`
* Escribir el artifact de la lambda (funcion lambda) con extension java

#### <span style="color:blue">Nota:</span>
 
##### <span style="color:blue"> Es importante subir el archivo zip con el mismo nombre para que el bucket s3 pueda versionar el artifact y tener un backup de versiones anteriores</span>

* Cargue el artifact de la lambda a un buckey s3 y completar las variables `s3_object_version`, `s3_bucket`, `s3_key` del artifact cargado en s3 ej:

```hcl

  s3_object_version  = "VUEc.MifxSuEKIB4ObcYByTTtyA_REDc"
  s3_bucket          = "name-bucket"
  s3_key             = "name-artifact-lambda.zip"

```
_se pueden usar interpolaciones entre modulos_

* Para obtener las variables `s3_object_version`, `s3_bucket` y `s3_key`

* s3_object_version
![s3_object_version](./imagen/object_version.png)

* s3_key
![s3_key](./imagen/s3.png)

`lambda_function_variables.tf`
```hcl
variable "lambda_params_*" {
    type    = "map"
    default = {
      function_name     = "circular-lambda"
      memory_size       = "2048"
      handler           = "index.handler"
      runtime           = "nodejs8.10"
      timeout           = "15"
      mode              = "PassThrough"
      s3_bucket         = "name-bucket-s"
      s3_key            = "name-artifact-lambda.zip"
      s3_object_version = "GT4iESUXEDcKTu.X.Im15GQUhPlN9ZkZ"
    }
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: lambda_params_1

```
_Cambiar los valores por los valores que necesite utilizar_

* Guardar los cambios hecho en los archivos, comitear los cambios y subirlos al versionador de codigo elegido, se ejecutara un pipeline automaticamente en Terraform Cloud
    se puede ver el proceso de deployado en su cuenta de [Terraform cloud](https://app.terraform.io/app).

### Nueva Version Lambda existente

* Para editar o cambiar parametros de una lambda existente vamos a editar el archivo `lambda_function_variables.tf` , muévase al directorio donde tiene el repositorio con el codigo ej: `~/workspace/circular/circular-lambda/lambda_function_variables.tf` o en caso de dividirse por mircro-servicio ej: `~/workspace/circular/circular-api-gateway/lambda_function_variables.tf`, si se desea editar el artifact:
    * descargue el archivo zip del bucket s3 y descomprimalo para editar el artifact
    * luego de editar el o los archivos, volver azipear y cargarlo nuevamente en el bucket s3
    * copiar el Version ID (s3_object_version, como se muestra en la imagen anterior) y edite la variable `s3_object_version` en el archivo `lambda_function_variables.tf`
    
* Para cambiar los valores de la lambda que se desea editar, vamos a editar el archivo  `lambda_function_variables.tf` y agregar los valores que necesitamos ej:

```hcl
variable "lambda_params_*" {
    type    = "map"
    default = {
      function_name     = "circular-lambda" >> "circular-lambda-2"
      memory_size       = "2048" >> "128"
      handler           = "index.handler" >> "ambda_function.lambda_handler"
      runtime           = "nodejs8.10" >> "python3.6"
      timeout           = "15" >> "30"
      mode              = "PassThrough"
      s3_bucket         = "name-bucket" >> "circular-bucket" 
      s3_key            = "name-artifact-lambda.zip" >> "circular.zip"
      s3_object_version = "GT4iESUXEDcKTu.X.Im15GQUhPlN9ZkZ" >> "X.Im15GQUhPlN9ZkZGT4iESUXEDcKTu.s"
    }
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: lambda_params_1

```

### Agregar una Layer Version a la Lambda

* Para crear un nuevo layer version , Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar ejemplo `~/workspace/circular/circular-lambda/lambda_layer_version.tf` o en caso de que esten divididas por micro-servicios ej:
`~/workspace/circular/circular-api-gateway/lambda_layer_version.tf`

* Para agregar una lambda nueva, vamos a editar los siguientes archivos: `lambda_layer_version.tf ` y `lambda_layer_version_variables.tf`

* Inserte el siguiente bloque de codigo en los archivos para agregar una lambda nueva:

`lambda_layer_version.tf`

```hcl

module "lambda_layer_circular" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v0.1.3"
  layer_name          = "${var.layer_params*["layer_name"]}"
  description         = "${var.layer_params*["description"]}"
  s3_bucket           = "${data.terraform_remote_state.circular_appsync.s3_name}"
  s3_key              = "${var.layer_params*["s3_key"]}"
  compatible_runtimes = ["${var.compatible_runtimes}"]
}

### Nota: Remplazar * por numero correspondiente o valor corespondiente ej: layer_params_1

```
* El artifact de las layer es similar a las lambdas, se debe escribir el codigo en un archivo con una extension java, luego zipear los archivos y subirlos a s3.
* El `s3_key` se obtine de la misma manera que con las lambdas en el ejemplo que se muestra mas arriba.
* En este ejemplo, el bucket s3 se encuentra en otro workspace por eso se interpola de esa manera, de lo contrario si el s3 se encuentra en el mismo workspace se debe interpolar de la manera tradicional:
`"${aws_s3_bucket.lambda-s3.id}"` >> si se interpola un modulo, es : `"${modulo.nombre-del-modulo.output}"` ej: `"${modulo.lambda-circular.arn}` 

`lambda_layer_version_variables.tf`

```hcl

variable "layer_params*" {
  type    = "map"
  default = {
    layer_name  = "Sharp"
    description = "Image processing library (linux node 8.10)"
    s3_key      = "layers/Sharp.zip" ## si el artifact se encuentra en un sub-directorion en el s3 se debe pasar el nombre del directorio tambien
  }
}

variable "compatible_runtimes*" {
  type     = "list"   
   default = ["nodejs8.10"]
}

### Nota: Remplazar * por numero correspondiente o valor corespondiente ej: layer_params_1

```

## Modificar un Layer existente

* Para modificar los parametros de una layer, vamos a modificar el siguiente archivo: `lambda_layer_version_variables.tf`, para modificar la funcion de una layer, vamos a descargar el zip almacenado en s3 y luego descomprimirlo para editar los archivos con la configuracion que querramos utilizar, luego volver a ziperar con el mismo nombre y cargarlo a s3.


#### <span style="color:blue">Nota:</span>
 
##### <span style="color:blue"> Es importante subir el archivo zip con el mismo nombre para que el bucket s3 pueda versionar el artifact y tener un backup de versiones anteriores</span>

 ```hcl


variable "layer_params*" {
  type    = "map"
  default = {
    layer_name  = "Sharp" >> "DB"
    description = "Image processing library (linux node 8.10)" >> "Image processing library"
    s3_key      = "layers/Sharp.zip" ## si el artifact se encuentra en un sub-directorion en el s3 se debe pasar el nombre del directorio tambien
  }
}

variable "compatible_runtimes*" {
  type     = "list"   
   default = ["nodejs8.10"] >> ["nodejs14.x",]
}

### Nota: Remplazar * por numero correspondiente o valor corespondiente ej: layer_params_1

 ```

## AppSync

* En este documento vamos a aprender como agregar un nuevo servicio de AppSync (DataSource, Resolver, Graphql) y a modificar un Appsync existente utilizando Terraform y Terraform Cloud.

### Nuevo AppSync GraphQL

* Para crear un nuevo AppSync GraphQl, Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar, ejemplo: `~/workspace/circular/circular-appsync/`
* Para agregar un nuevo appsync vamos a editar los archivos `appsync_graphql.tf` y `appsync_graphql_variables.tf`

* Inserte el siguiente codigo para agregar un nuevo appsync:

`appsync_graphql.tf`

```hcl

 module "graphql-api-CircularAppSync" { 
   source              = "corpit-consulting-public/appsync-graphql-api-mod/aws"
   version             = "v0.1.0"
   authentication_type = "${var.graphql_params*["authentication_type"]}"
   name                = "${var.graphql_params*["name"]}"
   default_action      = "${var.graphql_params*["default_action"]}"
   user_pool_id        = "${data.terraform_remote_state.circular_cognito.user_pool_id}"
   aws_region          = "${local.region}"
   schema              = "${file("./templates/appsync_schema/schema.graphql")}"
   cloudwatch_logs_role_arn  = "${aws_iam_role.Circular-AppSync-DataSource-Role.arn}"
   field_log_level           = "${var.graphql_params*["field_log_level"]}"
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: graphql_params_1
```
*  _Si se utiliza el sistema de directorio por default como viene en el repositorio, degar la linea "user_pool_id" como ya esta interpolada para poder vincular con el servicio de cognito-user-pool_
* _En todos los directorios donde se encuentran los servicios hay un archivo `local.tf` donde se interpola la region de los servicios_

* para agregar variables a appsync graphql inserte el siguiente codigo en el archivo:

`appsync_graphql_variables.tf`

```hcl

variable "graphql_params_*" {
   type    = "map"
   default = {
      authentication_type = "AMAZON_COGNITO_USER_POOLS" >> tipo de appsync
      name                = "CircularAppSync" 
      default_action      = "ALLOW"
      field_log_level     = "ERROR"
    }
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: graphql_params_1

```
### Modificar un AppSync Graphql existente

* Para modificar un appsync graphql existentes, debemos modificar los archivos `appsync_graphql_variables.tf` y  `appsync_graphql.tf` para agregar los valores que deseamos modificar.

 * Para los modulos que dependan de otro, se puede interpolar el output que se desea consumir para el modulo, el metodo de enterpolacion es:
`modulo.<nombreDelModulo>.id (o output que se necesite utilizar)`

`appsync_graphql_variables.tf`

```hcl

variable "graphql_params_*" {
   type    = "map"
   default = {
      authentication_type = "AMAZON_COGNITO_USER_POOLS" >> "API_KEY"
      name                = "CircularAppSync" >> "Circualr-graphql-test"
      default_action      = "ALLOW" >> "DENY"
      field_log_level     = "ERROR" >> "NONE"
    }
}

```
* Los tipos de appsync graphql pueden ser "AMAZON_COGNITO_USER_POOLS, API_KEY, AWS_IAM, OPENID_CONNECT" dependiendo de lo que se necesite utilizar

### Agregar un nuevo AppSync DataSource

* Para crear un nuevo AppSync DataSource, Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar, ejemplo: `~/workspace/circular/circular-appsync/`
* Para agregar un nuevo appsync vamos a editar los archivos `appsync_datasource.tf` y `appsync_datasource_variables.tf`

* Inserte el siguiente codigo para agregar un nuevo datasource:

`appsync_datasource.tf`

```hcl
### Si es de tipo "AMAZON_DYNAMODB" inserte este coddigo

module "datasource-appsync" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params_*["name"]}"
  type             = "${var.datasource_params_*["type"]}"
  api_id           = "${module.CircularAppSync.id}" ## Interpolar appsync Graphql
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Role.arn}"
  table_name       = "${module.circular-dynamodb-Table.id}"
}

### Si es de tipo "HTTP" inserte este codigo

module "datasource-appsync-http" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params_*["name"]}"
  type             = "${var.datasource_params_*["type"]}"
  api_id           = "${module.CircularAppSync.id}" ## Interpolar appsync Graphql
  http_endpoint    = "${var.datasource_params_*["http_endpoint"]}"
}

### Si es de tipo "AWS_LAMBDA" inserte este codigo 

module "datasource-appsync-lambda" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params_*["name"]}"
  type             = "${var.datasource_params_*["type"]}"
  api_id           = "${module.CircularAppSync.id}" ## Interpolar appsync Graphql
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Lambda-Role.arn}"
  function_arn     = "${module.circular-lambda.arn}" ## Interpolar lambda que se desea utilizar
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: datasource_params_1

```

`appsync_datasource_variables.tf`

```hcl

### Para tipo "AMAZON_DYNAMODB" inserte el siguiente codigo

variable "datasource_params_*" {0
  type    = "map"
  default = {
    type  = "AMAZON_DYNAMODB"
    name  = "appsync-dynamodb-test"
  }
}

### Para tipo "HTTP" inserte el siguiente codigo

 variable "datasource_params_*" {
  type            = "map"
  default         = {
    type          = "HTTP"
    name          = "appsync-http-test"
    http_endpoint = "https://circular-host.com/"
  }
}

### Para tipo "AWS_LAMBDA" inserte el siguiente codigo

variable "datasource_params_*" {
  type            = "map"
  default         = {
    type          = "AWS_LAMBDA"
    name          = "appsync-lambda-test"
  }
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: datasource_params_1


```
### Modificar un AppSync DataSource

* Para modificar un appsync data source, tenemos que editar los archivos `appsync_datasource.tf` y `appsync_datasource_variables.tf` y agregar los valores que deseamos modificar.

* Generalmente se dividen las tablas dynamodb y las lambdas por micro-servicio, por lo que en el directorio del servicio appsync se encuentren las lambdas y tablas dynamodb que se utilizan en los servicios.

* Para los modulos que dependan de otro, se puede interpolar el output que se desea consumir para el modulo, el metodo de enterpolacion es:
`modulo.<nombreDelModulo>.id (o output que se necesite utilizar)`

`appsync_datasource.tf`

```hcl

module "datasource-appsync" {
  source           = "corpit-consulting-public/appsync-datasource-mod/aws"
  version          = "v0.1.0"
  name             = "${var.datasource_params_*["name"]}"
  type             = "${var.datasource_params_*["type"]}"
  api_id           = "${module.CircularAppSync.id}" ## Interpolar appsync Graphql
  service_role_arn = "${aws_iam_role.Circular-AppSync-DataSource-Role.arn}" >> "${aws_iam_role.AppSync-DataSource-Role.arn}"
  table_name       = "${module.circular-dynamodb-Table.id}" >> "${module.circular-dynamodb-Table_2.id}"
}

```
`appsync_datasource_variables.tf`

```hcl
variable "datasource_params_*" {
  type    = "map"
  default = {
    type  = "AMAZON_DYNAMODB" >> "AWS_LAMBDA"
    name  = "appsync-dynamodb-test" >> "appsync-lambda-test"
  }
}

```

### Agregar un AppSync Resolver
 
* Para crear un nuevo AppSync Resolver, Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar, ejemplo: `~/workspace/circular/circular-appsync/`
* Para agregar un nuevo appsync vamos a editar el `appsync_resolver.tf`

* Inserte el siguiente codigo para agregar un nuevo appsync resolver:

`appsync_resolver.tf`

```hcl
module "appsync-resolver" { 
  source            = "corpit-consulting-public/appsync-resolver-mod/aws" 
  version           = "v0.1.0" 
  api_id            = "${module.CircularAppSync.id}" 
  field             = "messages" 
  type              = "Conversation" 
  data_source       = "${module.datasource-messagesTableDataSource.name}" 
  request_template  = "${file("templates/resolvers/Conversation/messages/request.json")}" 
  response_template = "${file("templates/resolvers/Conversation/messages/response.json")}" 
} 

```
* Para cargar los `request_templte` y `response_templates` primero se deve ir al directorio donde se encuentra el servicio ejemplo: `~/workspace/circular/circular-appsync` y clonar este repositorio de git [apppsyn-resolver](https://gitlab.com/corp-it/circular-appsync-demo.git)

* En ese repositorio hay una estructura de directorios que representa el orden de llamada de cada `request_template` y `response_template` que se ejecutaran en el servicio de appsync

* Se debe respetar la estructura de directorios porque de la manera en que esta hecho la estructura de representa en el servicio de appsync con nombres ya predefinidos por el schema cargado en el servicio de appsync, si faltara algun directorio se debe agregar con el nombre de algun resolver compatible con el servicio de appsync

![resolver](./imagen/1_KZoMCNAR202gb_4YpLgGeA.jpeg)

### Modificar un AppSync Resolver Existente

* Para modificar un appsync resolver, tenemos que editar el archivo `appsync_resolver.tf` y  agregar los valores que deseamos modificar.

`appsync_resolver.tf`

```hcl

module "appsync-resolver" { 
  source            = "corpit-consulting-public/appsync-resolver-mod/aws" 
  version           = "v0.1.0" 
  api_id            = "${module.CircularAppSync.id}" 
  field             = "messages" >> mutation
  type              = "Conversation" >> createConversation
  data_source       = "${module.appsync_datasource.name}" >> ${module.appsync_datasource_2.name}
  request_template  = "${file("templates/resolvers/Conversation/messages/request.json")}" 
  response_template = "${file("templates/resolvers/Conversation/messages/response.json")}" 

```

* Para modificar un resolver, se debe ir al directorio clonado de git y editar los archivos `request.json` y `response.json` con lo valores que quisiéramos modificar ejemplo de directorio: `~/workspace/circular/circular-appsync/templates/resolver` y luego navegar el directorio para modificar el archivo segun el `type` y `fiel` que necesitemos modificar

`response.json`

```hcl
{
  "version" : "2017-02-28",
  "operation" : "PutItem",
  "key": {
      "id": { "S" : "${context.arguments.id}"}
  },
  "attributeValues" : {
     "id": {  "S": "${context.arguments.id}" },
     "name": {  "S": "${context.arguments.name}" }
     #if(${context.arguments.createdAt}) ,"createdAt": { "S": "${context.arguments.createdAt}"} #end
  }
}

## Modificar los valores que necesitemos cambiar
## De la misma forma se modifican los request.json

```

* Despues de modificar los resolver, hay que commitear los cambios y subirlos a git para mas informacion visite: [git](https://rogerdudler.github.io/git-guide/index.es.html)

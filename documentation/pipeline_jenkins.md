# Pipeline Jenkis

CI/CD Gitlab - Jenkins Pipeline - CodeDeploy

## Description

Mostramos una implementación de CI/CD Utilizando Gitlab VCS, Jenkins Pipeline y Amazon AWS Code Deploy para Webapps.

* Gitlab: Clonar repositorio template: [circular_app_demo](https://gitlab.com/circular_test/circular_app_demo.git)
* Jenkins Pipeline :  Configurar Acceso Jenkins a AWS Account. [jobs-Jenkins](https://jenkins.io/doc/)
** Security : DEV / PROD / AWS Cross Account Role Based Access Control (C.A.R.B.A.C.)

* workflow:
* * source:
* * * clone feature branch
* * * commit
* * * push
* * * Merge Request (Test Driven Approval)
* * pipeline:
* * * job 'dev_deployment':
* * * * artifact_upload: Upload artifact to repository
* * * * cloud_api_deployment: Automatic Deployment Push
* * * * script: Documentation Update <Pending>
* * * Merge Request (Master: Test Driven Approval)
* * * Jenkins:
* * * job 'prod_delivery':
* * * * artifact_upload: Upload Artifact to repository
* * * * cloud_api_delivery: Manual Deployment Push
* * * * script: Documentation Update <Pending>
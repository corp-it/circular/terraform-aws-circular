
### Estructura de Repositorio

* En este documenta vamos entender la estructura de repositorios y como se utiliza para facilitar el trabajo de los desarrolladores.

* Utilizamos una estructura de 2 Branches para desarrollar el codigo: `develop` para desarrollar el codigo y testear `master` donde se hacen los `merge request` de los branches donde se hicieron los cambios

* Branches `master` y `develop`:

![branches](./imagen/master-develop-branch.svg)

* Se recomienda crear un feature branch para cada nueva caracteristica o cambio que se quiera agregar al codigo a partir del branch `devops` para despues crear un `pull request` y hacer un merge a `develop` para testear y evaluar el codigo por los review antes de hacer un `merge request` al branch `master`

* Features Branches:

![feature-branches](./imagen/feature-branches.svg)


### Git Flow

* Trabajamos de manera que para desarrollar un codigo o implementar un cambio creamos un feature branch con el id de un ticket o el nombre del cambio que se quiera hacer, a partir del branch `devops`

* Para mergear el cambio, se hacer un merge request al branch `dev` donde se ejecutara un pipeline que deployara automaticamente los cambios en las instancias de DEV 

* Despues de haber testeado el codigo y todos los cambios se hace un `Merge Request` a `master` donde se ejecutara un un pipeline que buildee los cambios y los zipea para cargarlos en un bucket S3, pero el deploymet se hara manualmente en CodeDeploy

* El pipeline solo se ejecuta cunado se hacen `Merge Request` o se commiteen `Hotfix` en los branches `dev` y `prod`, desde los feature branches que se requieran para implementar los cambios fixes

* Para mas informacion ingrese a [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 


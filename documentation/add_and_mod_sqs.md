## SQS

* En este documento vamos a aprender a crear un trigger de SQS y a modificar un trigger existente utilizando Terraform y Terraform Cloud

#### Agregar un Trigger de SQS

* Para agregar un nuevo Trigger de SQS, lo primero es moverse al directorio de trabajo donde se almacena el codigo que implementa el servicio que se desea cambier.
Ejemplo: `~/workspace/circular/circular-sqs/sqs.tf`, en caso de que esten divididos por micro-servicios ej: `~/workspace/circular/circular-lambda/sqs.tf`

* Para agregar un trigger de sqs vamos a editar los siguientes archivos: `sqs.tf` y  `sqs_variables.tf`

* inserte el siguiente bloque de codigo para agregar un nuevo trigger de sqs:

`sqs.tf` 

```hcl

module "ErroresLiNTI" {
  source  = "corpit-consulting-public/sqs/aws"
  version = "v0.2.0"
  name                       = "${var.ErroresLiNTI_params["name"]}"
  visibility_timeout_seconds = "${var.ErroresLiNTI_params["visibility_timeout_seconds"]}"
  max_message_size           = "${var.ErroresLiNTI_params["max_message_size"]}"
  message_retention_seconds  = "${var.ErroresLiNTI_params["message_retention_seconds"]}"
  delay_seconds              = "${var.ErroresLiNTI_params["delay_seconds"]}"
}

```

`sqs_variables.tf`

```hcl

variable "ErroresLiNTI_params" {
  type    = "map"
  default = { 
    name                       = "ErroresLiNTI"
    visibility_timeout_seconds = 30
    max_message_size           = 262144
    message_retention_seconds  = 345600
    delay_seconds              = 0
  }
}

```

#### Modificar un SQS existente

* Para editar o cambiar parametros de un sqs existente, vamos a editar el siguiente archivo: `sqs_variables.tf`,
muevase al directorio donde se encuentra el codigo ej: `~/workspace/circular/circular-sqs/sqs_variables.tf` o en caso de dividirce por mocro-servicio ej: `~/workspace/circular/circular-lambda/sqs_variables.tf`

`sqs_variables.tf`

```hcl

variable "ErroresLiNTI_params" {
  type    = "map"
  default = { 
    name                       = "ErroresLiNTI"
    visibility_timeout_seconds = 30 >> 15
    max_message_size           = 262144 >> 261056
    message_retention_seconds  = 345600 >> 300000
    delay_seconds              = 0 >> 3
  }
}

```
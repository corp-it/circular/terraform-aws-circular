# Cambio de AMI_ID Autoscaling

* En este documento vamos a aprender como cambiar una `image_id` utilizando Terraform y terraform Cloud.

#### Cambio de AMI ID

* AWS recomienda como "Best practices" son:
    * Ingresar a la instancia por SHH a la cual se quiere cambiar la imagen y modificar hasta que este lista como uno quiera
    * Despues vamos a la consola de aws y tomamos una imagen de la instancia que modificamos:

    ![image_id](./imagen/create_image_id.png)

    * Una vez creada la imagen copiamos el AMI ID que acabamos de crear
    ![AMi](./imagen/image_id.png)

    * Despues vamos al directorio donde tenemos almacenado el codigo que implementa el servicio de Autoscaling, ejemplo: `~/workspace/circular/circular-ec2/autoscaling_variables.tf` y editamos el archivo `autoscaling_variables.tf` y agregamos el Ami ID
        * Lo primero que hay que hacer es cambiar el `health_check_type` de `ELB` a `EC2` para poder hacer los cambios en la instancia y que el autoscaling no mate las instancias.
        * Una vez terminado de hacer los cambios, volver el `health_check_type`  de `EC2` a `ELB` para que el autoscaling funcione y testear que las instancias estan correctas.
        * Tambien se recomienda aumentar el numero de instancias para que se generen nuevas instancias con la nueva AMI, ej: si se tienen 2 instancias se recomienda ponerlas en 4 para que genere dos intancias nuevas 2 intancias mas que van a tener el Ami nuevo, una vez creadas volvemos a poner las instancias en 2 para que elimine las instancias viejas y solo queden las nuevas, de este modo no se pierden el trafico en las instancias y se hace con un flujo mas tranquilo sin que los usuarios lo noten
    `autoscaling_variables`

    ```hcl

    variable asg_params {
        default = { 
          name = "circular-app-asg"

          #### Cambiamos el healtcheck de "ELB" a "EC2" para hacer los cambios, una vez terminados los cambios volvemos a colocarlo en "ELB" para que el ASG trabaje
          health_check_type = "ELB" >>>> "EC2" >>>> "ELB"

          health_check_grace_period = "120"
          min_size                  = 1
          max_size                  = 4

          ### cambiamos el valor por para generar mas instancias
          desired_capacity          = 2 >>>> 4 >>>> 2

          ### una vez generadas las instancias volvemos al valor original para que solo queden las instancias nuevas

          wait_for_capacity_timeout = "30m"
        }   
    }

    variable lc_params {
        default = { 
          lc_name         = "circular-asg-lc"
          ### cambiamos el ami id
          image_id        = "ami-123456789" >>>> "ami-987654321"

          instance_type   = "t2.small"
          key_name        = "aws_circular_ssh_internal"
        }   
    }

    ```
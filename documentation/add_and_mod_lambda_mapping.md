# Lambda Mapping

* En este documento vamos a aprender a crear y modificar un trigger de lambda Source Mapping utilizando Terraform y Terraform Cloud

#### Agregar un nuvo Source Mapping 

Para agregar un nuevo source mapping, lo primero es movese al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar, ejemplo: `~/workspace/circular/circular-lambda/lambda_mapping.tf` o en caso de que esten divididos por micro-servicios ej: `~/workspace/circular/circular-api-gateway/lambda_mapping.tf`\

* Para agregar un nuevo triiger mapping vamos a editar los siguientes archivos: `lambda_mapping.tf` y `lambda_mapping_variables.tf`.
    * Para agregar un nuevo mapping copie el siguiente bloque de codigo:

`lambda_mapping.tf`

```hcl

### Para Dynamodb

module "forwardMessagesToBot" {
  source            = "corpit-consulting-public/lambda-event-source-mapping/aws"
  version           = "v0.1.1"
  batch_size        = "${var.mapping_params_#["batch_size"]}"

  ##### Se interpola una tabla dynamodb, en este caso se utiliza una interpolacion de tipo "data" por la divicion de workspace
  event_source_arn  = "${data.terraform_remote_state.circular_appsync.messagesTable_stream}"

  function_name     = "${module.forwardMessagesToBot.arn}"
  starting_position = "${var.mapping_params_#["starting_position"]}"
  enabled           = "${var.mapping_params_#["enabled"]}"
  has_dynamodb      = "true"
}

### Para SQS

module "updateLiNTI_sqs" {
  source            = "corpit-consulting-public/lambda-event-source-mapping/aws"
  version           = "v0.1.1"
  batch_size       = "${var.mapping_params_5["batch_size"]}"
  ##### Se interpola un un trigger SQS, en este caso se utiliza una interpolacion de tipo "data" por la divicion entre workspace
  event_source_arn = "${data.terraform_remote_state.circular_sqs.LiNTIUpdates_arn}"
  function_name    = "${module.updateLiNTI.id}"
  enabled          = "${var.mapping_params_5["enabled"]}"
  has_sqs          = "true"
}

```

`lambda_mapping_variables.tf`

```hcl

### Para Dynamodb

variable "mapping_params_#" {
  type    = "map"
  default = { 
    batch_size        = "100"
    starting_position = "LATEST"
    enabled           = "true"
  }
}

### Para SQS
variable "mapping_params_#" {
  type    = "map"
  default = {
    batch_size        = "10"
    enabled           = "true"
  }
}

```
#### Modificar un Source Mapping existente

* Para editar o cambiar parametros de un Source Mapping existente vamos a editar los archivos `lambda_mapping.tf` y `lambda_mapping_variables.tf`, ejemplo:

`lambda_mapping.tf`

```hcl

### Para Dynamodb

module "forwardMessagesToBot" {
  source            = "corpit-consulting-public/lambda-event-source-mapping/aws"
  version           = "v0.1.1"
  batch_size        = "${var.mapping_params_#["batch_size"]}"

  ##### Se interpola una tabla dynamodb, en este caso se utiliza una interpolacion de tipo "data" por la divicion de workspace
  event_source_arn  = "${data.terraform_remote_state.circular_appsync.messagesTable_stream}" >>> "${data.terraform_remote_state.circular_appsync.dynamodb2_stream}"

  function_name     = "${module.forwardMessagesToBot.arn}" >>> "${module.lambda_2.arn}
  starting_position = "${var.mapping_params_#["starting_position"]}"
  enabled           = "${var.mapping_params_#["enabled"]}"
  has_dynamodb      = "true"
}

### Para SQS

module "updateLiNTI_sqs" {
  source            = "corpit-consulting-public/lambda-event-source-mapping/aws"
  version           = "v0.1.1"
  batch_size       = "${var.mapping_params_5["batch_size"]}"
  ##### Se interpola un un trigger SQS, en este caso se utiliza una interpolacion de tipo "data" por la divicion entre workspace
  event_source_arn = "${data.terraform_remote_state.circular_sqs.LiNTIUpdates_arn}" >>> "${data.terraform_remote_state.circular_sqs.SQS_2_arn}

  function_name    = "${module.updateLiNTI.id}" >>> "${module.lambda_3.arn}
  enabled          = "${var.mapping_params_5["enabled"]}"
  has_sqs          = "true"
}

```

`lambda_mapping_variables.tf`

```hcl

### Para Dynamodb

variable "mapping_params_#" {
  type    = "map"
  default = { 
    batch_size        = "100" >> "50"
    starting_position = "LATEST"

    ### Esta variable habilita el Trigger  
    enabled           = "true" >>> "false"
  }
}

### Para SQS
variable "mapping_params_#" {
  type    = "map"
  default = {
    batch_size        = "10" >>> "20"

    ### Esta variable habilita el Trigger
    enabled           = "true"  >>> "false"
  }
}

```
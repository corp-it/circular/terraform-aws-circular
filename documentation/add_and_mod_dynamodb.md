
## Dynamodb

* En este documento vamos a aprender como agregar una nueva Tabla Dynamodb y a modificar una Tabla existente utilizando Terraform y Terraform Cloud.

### Agregar una nueva Tabla Dynamodb

* Para agregar una nuva tabla dynamodb, lo primero es moverse al directorio de tabajo donde se almacena el codigo del servicio que se desea modificar o agregar, ejemplo `~/workspace/circular/dynamodb/dynamodb.tf` o en caso de que esten divididos por micro-servicios ejemplo `~/workspace/circular/circular-appsync/dynamodb.tf`

* Para agregar una tabla dynamodb nueva, vamos a editar los siguientes archivos: `dynamodb.tf` y `dynamodb_variables.tf`
* Inserte el siguiente ejemplo de codigo para agregar una nueva tabla dynamodb:

`dynamodb.tf`

```hcl

## Si se utilizan global-index utilicen este bloque de codigo

module "dynamodb-table" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params_*["name"]}"
  billing_mode     = "${var.dynamodb_params_*["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params_*["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params_*["write_capacity"]}"
  hash_key         = "${var.dynamodb_params_*["hash_key"]}"
  range_key        = "${var.dynamodb_params_*["range_key"]}"
  stream_enabled   = "${var.dynamodb_params_*["stream_enabled"]}"
  stream_view_type = "${var.dynamodb_params_*["stream_view_type"]}"
  attribute        = "${var.attribute-*}"
  global_index_name               = "${var.global_index_params_*["global_index_name"]}"
  global_index_hash_key           = "${var.global_index_params_*["global_index_hash_key"]}"
  global_index_read_capacity      = "${var.global_index_params_*["global_index_read_capacity"]}"
  global_index_write_capacity     = "${var.global_index_params_*["global_index_write_capacity"]}"
  global_index_projection_type    = "${var.global_index_params_*["global_index_projection_type"]}"
  has_global_secondary_index      = "true"
}

## Si se utilizan local-index utilice este bloque de codigo

module "dynamodbtable" {
  source           = "corpit-consulting-public/dynamodb-table-mod/aws"
  version          = "v0.2.0" 
  name             = "${var.dynamodb_params_*["name"]}"
  billing_mode     = "${var.dynamodb_params_*["billing_mode"]}"
  read_capacity    = "${var.dynamodb_params_*["read_capacity"]}"
  write_capacity   = "${var.dynamodb_params_*["write_capacity"]}"
  hash_key         = "${var.dynamodb_params_*["hash_key"]}"
  range_key        = "${var.dynamodb_params_*["range_key"]}"
  stream_enabled   = "${var.dynamodb_params_*["stream_enabled"]}"
  stream_view_type = "${var.dynamodb_params_*["stream_view_type"]}"
  attribute        = "${var.attribute-*"
  index_name                  = "${var.local_index_params_*["index_name"]}"
  index_hash_key              = "${var.local_index_params_*["index_hash_key"]}"
  index_range_key             = "${var.local_index_params_*["index_range_key"]}"
  projection_type             = "${var.local_index_params_*["projection_type"]}"
  has_local_secondary_index   = "true"
  non_key_attributes          = "${var.non_key_attributes-*}"
}

## Si se utiliza una tabla sin global-index o local-index utilice el siguiente codigo

module "dynamodb-table" {
  source            = "corpit-consulting-public/dynamodb-table-mod/aws"
  version           = "v0.2.0" 
  name              = "${var.dynamodb_params_*["name"]}"
  billing_mode      = "${var.dynamodb_params_*["billing_mode"]}"
  read_capacity     = "${var.dynamodb_params_*["read_capacity"]}"
  write_capacity    = "${var.dynamodb_params_*["write_capacity"]}"
  hash_key          = "${var.dynamodb_params_*["hash_key"]}"
  stream_enabled    = "${var.dynamodb_params_*["stream_enabled"]}"
  stream_view_type  = "${var.dynamodb_params_*["stream_view_type"]}"
  attribute         = "${var.attribute-*}"
}

### Nota: Remplazar * por numero correspondiente o valor corespondiente ej: dynamodb_params_1

```

`dynamodb_variables.tf`

```hcl

## Si se utilizan global-index utilicen este bloque de codigo

variable "dynamodb_params_*" {
  type    = "map"
  default = {
    name           = "nombre-dynamodb-table"
    billing_mode   = "PAY_PER_REQUEST"
    read_capacity  = 50
    write_capacity = 200
    hash_key       = "conversationId"
    range_key      = "createdAt"
    stream_enabled   = "true"
    stream_view_type = "NEW_AND_OLD_IMAGES"
  }
}

variable "attribute_*" {
  type    = "list"
  default = [
    {
      name = "conversationId"
      type = "S"
    }, {
      name = "createdAt"
      type = "S"
    },
    {
      name = "dateHour"
      type = "S"
    }
  ]
}

variable "global_index_params_*" {
  type    = "map"
  default = {
    global_index_name               = "global-dynamodb-index"
    global_index_hash_key           = "dateHour"
    global_index_range_key          = "createdAt"
    global_index_read_capacity      = 50
    global_index_write_capacity     = 200
    global_index_projection_type    = "INCLUDE"
  } 
}

variable "global_index_non_key_attributes_*" {
  type    = "list"
  default = [
    "sender",
    "attachments", 
    "text",
  ]
}

## Si se utiliza local-index utilice el seguiente bloque de codigo

variable "dynamodb_params_*" {
  type    = "map"
  default = {
    name           = "nombre-dynamodb-table"
    billing_mode   = "PAY_PER_REQUEST"
    read_capacity  = 5
    write_capacity = 5
    hash_key       = "eventId"
    range_key      = "commentId"
  }
}

variable "attribute-*" {
  type    = "list"
  default = [
    {
      name = "commentId"
      type = "S"
    }, {
      name = "eventId"
      type = "S"
    },
      {
      name = "createdAt"
      type = "S"
    }
  ]
}

variable "local_index_params_*" {
  type    = "map"
  default = {
    index_name           = "nombre-index"
    index_hash_key       = "eventId"
    index_range_key      = "createdAt"
    projection_type      = "ALL"
  } 
}

## Si no se utiliza global-index o local-index utilice el seguiente bloque de codigo

variable "dynamodb_params_*" {
  type    = "map"
  default = {
    name             = "nombre-dynamodb-table"
    billing_mode     = "PAY_PER_REQUEST"
    read_capacity    = 5
    write_capacity   = 5
    hash_key         = "id"
  }
}

variable "attribute-*" {
  type    = "list"
  default = [
    {
      name = "id"
      type = "S"
    }
  ]
}

### Nota: Remplazar * por numero correspondiente o valor corespondiente ej: dynamodb_params_1

```

### Modificaion de una Tabla Dynamodb existente

* Para modificar una tabla dynamodb existente, vamos a editar el siguiente archivo `dynamodb_variables.tf`

`dynamodb_variables.tf`

```hcl

## Si se utilizan global-index utilicen este bloque de codigo

variable "dynamodb_params_*" {
  type    = "map"
  default = {
    name           = "nombre-dynamodb-table" >> "dynamo-table"
    billing_mode   = "PAY_PER_REQUEST" >> "PROVISIONED"
    read_capacity  = 50  >> 25
    write_capacity = 200 >> 100
    hash_key       = "conversationId" >> "Conversation"
    range_key      = "createdAt"
    stream_enabled   = "true" >> "false"
    stream_view_type = "NEW_AND_OLD_IMAGES" >> "NEW_IMAGE"
  }
}

variable "attribute_*" {
  type    = "list"
  default = [
    {
      name = "conversationId" >> "Conversation"
      type = "S"
    }, {
      name = "createdAt" >> "create"
      type = "S"
    },
    {
      name = "dateHour" >> "hourDate"
      type = "S"
    }
  ]
}

variable "global_index_params_*" {
  type    = "map"
  default = {
    global_index_name               = "global-dynamodb-index" >> "dynamodb-global-index"
    global_index_hash_key           = "dateHour" >> "hourDate"
    global_index_range_key          = "createdAt" >> "create"
    global_index_read_capacity      = 50 >> 25
    global_index_write_capacity     = 200 >> 50
    global_index_projection_type    = "INCLUDE" >> "ALL"
  } 
}

variable "global_index_non_key_attributes_*" {
  type    = "list"
  default = [
    "sender",
    "attachments", 
    "text",
  ]
}

## Si se utiliza local-index utilice el seguiente bloque de codigo

variable "dynamodb_params_*" {
  type    = "map"
  default = {
    name           = "nombre-dynamodb-table" >> "dynamo-table"
    billing_mode   = "PAY_PER_REQUEST" >> "PROVISIONED
    read_capacity  = 5 >> 20
    write_capacity = 5 >> 15
    hash_key       = "eventId" >> "idevent"
    range_key      = "commentId" >> "comment"
  }
}

variable "attribute_*" {
  type    = "list"
  default = [
    {
      name = "commentId" >> "comment"
      type = "S"
    }, {
      name = "eventId" >> "idevent"
      type = "S"
    },
      {
      name = "createdAt" >> "id"
      type = "S"
    }
  ]
}

variable "local_index_params_*" {
  type    = "map"
  default = {
    index_name           = "nombre-index"
    index_hash_key       = "eventId"
    index_range_key      = "createdAt" 
    projection_type      = "ALL" >> "INCLUDE"
  } 
}

## Si no se utiliza global-index o local-index utilice el seguiente bloque de codigo

variable "dynamodb_params_*" {
  type    = "map"
  default = {
    name             = "nombre-dynamodb-table" >> "dynamodb-tabla"
    billing_mode     = "PAY_PER_REQUEST" >> "PROVISIONER"
    read_capacity    = 5 >> 10
    write_capacity   = 5 >> 20
    hash_key         = "id" >> "5"
  }
}

variable "attribute-*" {
  type    = "list"
  default = [
    {
      name = "id" >> "5"
      type = "S" >> "N"
    }
  ]
}

### Nota: Remplazar * por numero correspondiente o valor corespondiente ej: dynamodb_params_1

```
* Despues de haber hecho todos los cambios, commitear los cambios y subirlos a git


# Plantilla para Terraform Cloud AWS Project Manager

Este documentro muestra un ejemplo de plantillas que cualquiera puede utilizar para administrar espacios de trabajo de AWS utilizando Terraform Cloud.
 * Lea mas sobre Terraform Cloud aquí : [Tf-CLoud](https://www.terraform.io/docs/cloud/index.html)


## Terraform Cloud Backend

 Estamos utilizando la app de Terraform Cloud como backend remoto. Para leer mas sobre Terraform backend pulse en : [Tf-Banckend](https://www.terraform.io/docs/backends/index.html).
Para poder utilizar Terraform cloud, seguir los siguientes pasos.


### Pre-requisitos

* Lo primero que necesitamos es tener una cuenta Free en Terraform Cloud, aqui : [Terraform-cloud-account](https://app.terraform.io/signup/account)

* Luego de crear una cuenta en Terraform Cloud vamos a configurar un archivo `.terraformrc` con el siguiente bloque, 
* En Windows, el archivo debe nombrarse `terraform.rc` y colocarse en el directorio %APPDATA% del usuario correspondiente . La ubicación física de este directorio depende de la versión de Windows y la configuración del sistema; use `$env:APPDATAen PowerShell` para encontrar su ubicación en su sistema.

* En Windows, tenga cuidado con el comportamiento predeterminado del Explorador de Windows de ocultar extenciones de nombres de archivos. Terraform no reconocera un archivo llamado `terraform.rc.txt` como archivo de configuracion de CLI, aunque el Explorador de Windows muestre su nombre como `terraform.rc`. Utilice PowerSell o Simbolo de Sistema para confirmar el nombre del archivo.

`.terraformrc` o `terraform.rc` si se utiliza Windows

```hcl
    credentials "app.terraform.io" {
      token = "Se obtiene de terraform cloud"
    }      
 ```

* Para generar un token en Terraform Cloud, ingrese a su cuenta de Tf-cloud y vaya a `Settings >> API Tokens` y seleccione `Create an authentication token` para generar el token.Como se observa en la siguiente imagen:

![tftoken](./imagen/tfcloudtoken.png)

* Copiar el Token y pegarlo en el archivo `.terrafirmrc` o `terraform.rc` comose ve en el ejemplo mas arriba

### Setup

Para configurar los workspace en terraform cloud vamos a utilizar el siguiente bloque de codigo por cada workspace que se quiera utilizar, se recomienda devidir los workspace por servicos.

* Vamos a utilizar los siguientes archivos en el directorio donde se va a ejecutar el workspace:

  * `config.tf` donde vamos a tener la configuracion del backend remoto.
  * `variables.tf` donde vamos a colocar las variables locales o las que ingresemos en terraform cloud.
  * `output.tf` donde vamos a colocar los output de los servicios para poder interpolarlos a otros servicios a medida que los necesitemos.
  * `locals.tf` donde vamos a colocar la variable environment y region.

`config.tf`

```hcl

terraform {
    required_version = ">= 0.11.14"
    backend "remote" {
        hostname = "app.terraform.io"
        organization = "Circular-qa"
        workspaces {
            prefix = "circular-vpc-"
        }
    }    
}

provider "aws" {
    version = ">= 2.3.0"
    region  = "${local.region}"
}

```

`locals.tf`

```hcl

locals {
	## Global variables definition based on workspace.
    env = "${var.workspace_name_prefix}"

	## Workspace Variables
	region = "us-east-1"

	global_tags = {
        project = "Circular"
	}
}

```
`variables.tf`

```hcl

variable "workspace_name_prefix" {
}

## En el archivo variables vamos a ingresar las variables que vamos a cargar en terraform cloud, los bloques se pasan vacios

```
* Una vez que tengamos los archivos, corremos el comando `terraform init` para cargar el backend remoto y despues corremos el comando `terraform workspace new <nombre del worspace>` para crear el workspace en Tf-cloud

### Configuracion de Workspace en Terraform Cloud

Una vez que tengamos listos los workspaces vamos a configurar los parametros de workspaces:

* EL nombre del workspace es la union de el valor prefix en el archivo `config.tf` y el valor dado en `terraform workspace new` 
* Seleccionamos el workspace y vamos a su configuracion

![workspace](./imagen/workspace.png)


![settings](./imagen/settings.png)

* Despues vamos a la seccion de `Terraform Working Directory` que esta mas abajo y agregamos el path del directorio de trabajo, son las carpetas que estan el el repositorio de git.

![working-directory](./imagen/working-directori.png)

* Despues de terminar la configuracion general, vamos a la seccion de `Version Control` como se ve en la imagen de abajo, ahí vamos a agregar el branch de git de donde se va a consumir el codigo
    * Por lo general se utilizan 1 branch por cuenta, ejemplo para una cuenta `prod` se utiliza un brach `master`, en este caso utilizamos una cuenta `dev` y y utilizamos un branch `develop` como se observa en la imagen de abajo

![version-settings](./imagen/version-settings.png)

### Configurando variables de entorno

Una vez que tengamos configurado los directorios vamos a la seccion de variables, y ahi vamos a agregar las variables que necesitemos y tambien cargar las credenciales de AWS

![envVars](./imagen/envVars.png)

* Ahi vamos a agregar las variable `workspace_name_prefix` que colocamos vacia en el archivo variables, pero aca le vamos a colocar el valor de la cuenta que estemos configurando este workspace, en este caso es `dev`

* Despues vamos a cargar las credenciales de AWS, es importante marcar la casilla `sensitive` para que las credenciales esten encriptadas y no sean visibles 
    * La credenciales se pasan a traves de las siguientes environment `AWS_ACCES_KEY_ID` y `AWS_SECRET_ACCES_KEY`
    * Tambien podemos agregar la variable `CONFIRM_DESTROY` para apagar los servicios del workspace seleccionado
* La seccion `Environment Variables` se cargan desde Terraform Cloud, y las `Terraform Variables` se deben pasar en el archivo variables vacias como el ejemplo mas arriba y despues en Terraform Cloud con el valor que necesitemos.

###### Una vez configurado el workspace y haber colocado las variables necesarias ya estamos listo para aplicar un `Queue plan` para aplicar los servicios en AWS


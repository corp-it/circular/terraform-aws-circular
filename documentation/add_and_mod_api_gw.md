
## Api Gateway

* En este documento vamos a aprender como agregar un nuevo servicio de Api GateWay y a modificar un Api GW existente utilizando Terraform y Terraform Cloud.

* En el modulo de api gateway vamos a tenes varioa archivos para modificar: `gw_rest_api*`, `gw_method_setting*`, `gw_stage*` y `gw_api_key*`

### Agregar un Api Gateway Nuevo

* Para crear un nuevo Api GW, Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar, ejemplo: `~/workspace/circular/circular-api-gateway/`
* Despues vamos a editar los siguentes archivos con sus variables `gw_rest_api*`, `gw_method_setting*`, `gw_stage*` y `gw_api_key*`.

* Para crear un nuevo api, vamos a editar los siguientes archivos: `gw_rest_api.tf`, `gw_rest_api_variables.tf`, `gw_rest_api_swagger_templates.tf`  y un archivo swaggerfile con extencion .tpl

`gw_rest_api.tf`

```hcl

module "circular_api_gw" {
  source         = "corpit-consulting-public/api-gateway/aws"
  version        = "v0.1.1"
  name           = "${var.gw_params*["name"]}"
  api_key_source = "${var.gw_params*["api_key_source"]}"
  body           = "${data.template_file.api_gw_actors_swagger.rendered}"
  types          = ["${var.types}"]
}

resource "aws_api_gateway_deployment" "circular_actors" {
  rest_api_id = "${module.gw_circular_rest_api_Circular_Actors_API.id}"
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: gw_params_1

```
* La variable `body` es una especificación OpenAPI que define el conjunto de rutas e integraciones para crear como parte de la API REST, se recomienda pasar un body para mantener el codigo mas limpio y simplificado

`gw_rest_api_variables.tf`

```hcl
variable "gw_params*" {
  type    = "map"
  default = {
    name           = "Circular GW"
    api_key_source = "HEADER"
  }
}

variable "types" {
  type = "list"
  default = ["REGIONAL"]
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: gw_params_1

```

`gw_rest_api_swagger_templates.tf` 

```hcl

data template_file "api_gw_swagger" {
  template = "${file("templates/api_gw_actors_swagger.tpl")}"
  vars {
    region                      = "${local.region}"
    user_pool_id                = "${data.terraform_remote_state.circular_cognito.user_pool_id}"
    account_id                  = "${data.aws_caller_identity.current.account_id}"
  }
}

```
* El archivo `gw_rest_api_swagger_templates.tf` es una data que renderiza el swaggerfile con extencion .tpl para poder hacer intepolaciones de modulos al swaggerfile.

* Estructura basica de un swaggerfile
`swaggerfile.tpl`

```hcl
swagger: "2.0"
info:
  title: Sample API
  description: API description in Markdown.
  version: 1.0.0
host: api.example.com
basePath: /v1
schemes:
  - https
paths:
  /users:
    get:
      summary: Returns a list of users.
      description: Optional extended description in Markdown.
      produces:
        - application/json
      responses:
        200:
          description: OK

```

* Editar el swaggerfile con la configuracion que se desea en el api, se recomienda utilizar una cuenta de aws dev para extraer el swaggerfile del apigateway ya funcionando, para tener como modelo el archivo y poder editarlo con las configuraciones que uno desea.
* En caso de no tener conocimiento de escritura de swagger file, se recomienda levantar a mano un api gw en una cuenta de desarrollo y exportar el swaggerfile con la configuracion dada, en cualquiera de los de casos habra que editar el swagger file.
* Descargar Swagger file de un api gateway existente, en el apartado de stage:

![stage](./imagen/export-swagger.png)

* Copiar el contenido del swaggerfile descargado y pegarlo en un archivo nuevo con extencion .tpl para despues hacer las interpolaciones requeridas

* Luego de descargar el swagger file, tendra que recorrer el suaggew para poder interpolar las variables del archivo `gw_rest_api_swagger_template.tf`

```hcl
## Remplazar los siguientes valores por las interpolacions

us-east-* >> ${region} # Esta variable permite interpolar cualquier region que se este utilizando

### Remplazar account id de las cuentas de aws ej:

123456789123 >> ${account_id}

## Remplazar los id de cognito si es que se utiliza
us-east-1_148aEWsf >> ${user_pool_id}

## Ejemplo de una linea de swaggerfile editada con interpolaciones:

"arn:aws:cognito-idp:${region}:${account_id}:userpool/${user_pool_id}"

## Tambien se deben editar las sintaxis de interpolaciones de stage variables, agregar un "$" mas ej:

${stageVariable.lambda} >> $${stageVariable.lambda}

```

### Argregar una configuracion para Api Gateway

* Para agregar una configuracion de api gw, Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar, ejemplo: `~/workspace/circular/circular-api-gateway/`

* Despues vamos a editar los siguientes archivos: `gw_method_settings.tf`, `gw_method_settings_variables.tf` y `gw_stage.tf` agregue el siguiente ejemplo de codigo con los valores que se desea

`gw_method_settings.tf`

```hcl

module "circular_method_settings" {
  source                                     = "corpit-consulting-public/api-gateway-method-settings/aws"
  version                                    = "v0.1.2"
  rest_api_id                                = "${module.gw_circular_rest_api.id}"
  stage_name                                 = "${module.circular_stage.name}"
  method_path                                = "${var.method_params*["method_path"]}"
    metrics_enabled                            = "${var.method_params*["metrics_enabled"]}" 
    logging_level                              = "${var.method_params*["logging_level"]}"
    data_trace_enabled                         = "${var.method_params*["data_trace_enabled"]}"
    throttling_burst_limit                     = "${var.method_params*["throttling_burst_limit"]}"
    throttling_rate_limit                      = "${var.method_params*["throttling_rate_limit"]}"
    caching_enabled                            = "${var.method_params*["caching_enabled"]}"
    cache_ttl_in_seconds                       = "${var.method_params*["cache_ttl_in_seconds"]}"
    cache_data_encrypted                       = "${var.method_params*["cache_data_encrypted"]}"
    require_authorization_for_cache_control    = "${var.method_params*["require_authorization_for_cache_control"]}"
    unauthorized_cache_control_header_strategy = "${var.method_params*["unauthorized_cache_control_header_strategy"]}"
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: gw_params_1

```
`gw_method_settings_variables.tf`

```hcl

variable "method_params*" {
  type    = "map"
  default = {
    method_path                                = "*/*"
    metrics_enabled                            = "true" 
    logging_level                              = "INFO"
    data_trace_enabled                         = "true"
    throttling_burst_limit                     = "5000"
    throttling_rate_limit                      = "10000.0"
    caching_enabled                            = "false"
    cache_ttl_in_seconds                       = "300"
    cache_data_encrypted                       = "false"
    require_authorization_for_cache_control    = "true"
    unauthorized_cache_control_header_strategy = "SUCCEED_WITH_RESPONSE_HEADER"
  }
}

## Cambiar los valores por los valores que se desean utilizar

```
`gw_stage.tf`

```hcl

module "circular_stage" {
  source        = "corpit-consulting-public/api-gateway-stage/aws"
  version       = "v0.1.2"
  stage_name    = "dev"
  rest_api_id   = "${module.gw_circular_rest_api.id}"
  deployment_id = "${aws_api_gateway_deployment.circular.id}"
  variables      {
    key1 = "value1"
    key2 = "value2"
    ## Ejemplo de stage variable con vpc_link
    vpcLinkId = "${aws_api_gateway_vpc_link.Circular-vpc-link.id}"
  }
}

### En la variables se pueden pasar cualquier valor que se requiera utilizar, tambien se pueden interpolar modulos o resources de otros servicios

```
### Para cambiar la configuracion de Api Gateway 

* Para cambiar la configuracion de un api gateway vamos a editar los siguientes archivos: `gw_stage.tf`, `gw_method_settings_variables.tf`, ejemplo:

`gw_stage.tf`

```hcl

module "circular_stage" {
  source        = "corpit-consulting-public/api-gateway-stage/aws"
  version       = "v0.1.2"
  stage_name    = "dev"
  rest_api_id   = "${module.gw_circular_rest_api.id}" ## Interpolar el rest api que se desea utilizar
  deployment_id = "${aws_api_gateway_deployment.circular.id}" ## Interpolar el recurso Deployment Id qque se desea utilizar, el resouce deploymet se implementa en "gw_rest_api.tf"
  variables      {
    key1 = "value1" ## Agregar las variables que se necesiten
    #ejemplo:
    lambda  = "${module.lambda.id}" 
    ## Ejemplo de stage variable con vpc_link
    vpcLinkId = "${aws_api_gateway_vpc_link.Circular-vpc-link.id}"
  }
}

```

`gw_method_settings_variables.tf`

```hcl

variable "method_params*" {
  type    = "map"
  default = {
    method_path                                = "*/*"
    metrics_enabled                            = "true" >> "false"
    logging_level                              = "INFO" >> "ERROR"
    data_trace_enabled                         = "true" >> "false"
    throttling_burst_limit                     = "5000" >> "10000"
    throttling_rate_limit                      = "10000.0" >> "5000.0"
    caching_enabled                            = "false" >> "true"
    cache_ttl_in_seconds                       = "300" >> "120"
    cache_data_encrypted                       = "false" >> "true"
    require_authorization_for_cache_control    = "true" >> "false"
    unauthorized_cache_control_header_strategy = "SUCCEED_WITH_RESPONSE_HEADER" >> "SUCCEED_WITHOUT_RESPONSE_HEADER"
  }
}

## Agregar valores que se necesiten modificar

```


### Para agregar un Api Gateway Api key

* Para crear un nuevo Api GW,Api Key Lo primero es moverse al directorio de trabajo donde se almacena el codigo del servicio que se desea cambiar, ejemplo: `~/workspace/circular/circular-api-gateway/`
* Despues vamos a editar los siguentes archivos con sus variables `gw_api_key.tf` y `gw_api_key_variables.tf`.

* Agregue el siguientes ejemplos de codigo con los valores que uno desea

`gw_api_key.tf`

```hcl

module "circular-gw-api-key" {
  source      = "corpit-consulting-public/api-gateway-api-key/aws"
  version     = "v0.1.0"
  name        = "${var.gw_api_key_params*["name"]}"
  enabled     = "${var.gw_api_key_params*["enabled"]}"
  description = "${var.gw_api_key_params*["description"]}"
  value       = "${var.gw_api_key_params*["value"]}"
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: gw_api_key_params_1
```
`gw_api_key_variable.tf`

```hcl

variable "gw_api_key_params*" {
  type    = "map"
  default = {
    name        = "Server-Key"
    enabled     = "true"
    description = "key to be used for calls from the server"
  }
}

###Nota: Remplazar * por numero correspondiente o valor corespondiente ej: gw_api_key_params_1
```

### Modificar un GW Api Key

* Para modificar un gw api key existente vamos a modificar el archivo `gw_api_key_variables.tf`

`gw_api_key_variables.tf`

```hcl

variable "gw_api_key_params*" {
  type    = "map"
  default = {
    name        = "Server-Key" >> "key-server"
    enabled     = "true" >> "false"
    description = "key to be used for calls from the server" >> "agregar descripcion opcional"
  }
}

```

* Despues de editar todos los archivos se debe commitear y subirlos a git, se utiliza el mismo proceso con todos los workspace creados.

## Nuevo sistema de Deployment Api GW

* Desarrollamos una solucion para el deployment en api gateway mediante varios cambios en el modulo del servicio, esto genero un nuevo sistema de deployments que en esta seccion vamos a explicar de una forma facil de hacer:

* Como primer cambio podemos ver que ahora el cogigo de los servicios de api gateway `gw_stage`, `gw_method_settings` y `gw_deployment` se conbinaron con  `gw_rest_api` para hacer un solo modulo en siguiente archivo:

`gw_rest_api.tf`

```hcl
module "gw_circular_rest_api" {
  source             = "corpit-consulting-public/api-gateway/aws"
  version            = "v3.1.0"
  name               = "${var.gw_params["name"]}"
  description        = "${var.gw_params["description"]}"
  api_key_source     = "${var.gw_params["api_key_source"]}"
  body               = "${data.template_file.circular_api_gw_swagger.rendered}"
  types              = ["${var.types}"]
  
  ###Deploymnet
  stage_name         = "${var.stage_name}"
  version_id         = "v1.1.0-circular"

  #####Stage
  cache_cluster_enabled               = "false"
  #cache_cluster_size                  = "0.5"
  variables          = {
    takeWebsiteScreenshotlambda       = "${module.takeWebsiteScreenshot.id}"
    uploadImageLocationlambda         = "${module.uploadImageLocation.id}"
    s3temporaryPresignedURLlambda     = "${module.s3temporaryPresignedURL.id}"
    sendMessageToManyAPIAdapterlambda = "${module.sendMessageToManyAPIAdapter.id}"
    registerFirebaseTokenlambda       = "${module.registerFirebaseToken.id}"
    contentBucket                     = "${var.contentBucket}"
    contentEndpoint                   = "${var.contentEndpoint}"
    conversalabSiteUrl                = "${var.conversalabSiteUrl}"
    conversalabPublicSiteUrl          = "${var.conversalabPublicSiteUrl}"
    vpcLinkId                         = "${aws_api_gateway_vpc_link.Circular-vpc-link.id}"
  }

  ####Method Settings
  method_path                                = "${var.method_params["method_path"]}"
    metrics_enabled                            = "${var.method_params["metrics_enabled"]}" 
    logging_level                              = "${var.method_params["logging_level"]}"
    data_trace_enabled                         = "${var.method_params["data_trace_enabled"]}"
    throttling_burst_limit                     = "${var.method_params["throttling_burst_limit"]}"
    throttling_rate_limit                      = "${var.method_params["throttling_rate_limit"]}"
    caching_enabled                            = "${var.method_params["caching_enabled"]}"
    cache_ttl_in_seconds                       = "${var.method_params["cache_ttl_in_seconds"]}"
    cache_data_encrypted                       = "${var.method_params["cache_data_encrypted"]}"
    require_authorization_for_cache_control    = "${var.method_params["require_authorization_for_cache_control"]}"
    unauthorized_cache_control_header_strategy = "${var.method_params["unauthorized_cache_control_header_strategy"]}"
}


```

* Para hacer cualquier cambio vamos a seguir estos pasos:
  * Lo primero es hacer las modificaciones como se mencionan en anteriormente en este documento
  * Ahora tanto method settings como Stage se encuentran en el mismo archivo por lo que para poder aplicar los cambios necesitamos cambiar la siguiente variable:

  ```hcl

  version_id         = "v1.1.0-circular" >>> "v1.2.0-circular"
  ## Este valor puede ser aleatorio, es una forma de poder forzar un nuevo deployment y tener una guia de en que version se encuentra la api.
  ## Si no se cambia el valor en esta variable, no se va a ejecutar un nuevo deployment, para hacer un cambio en el swagger o en las variables de api gateway para que se apliquen se devera cambiar el valor en esta variable
  
  ```
  * Despues commiteamos los cambios y eso va a ejecutar el pipeline en terraform cloud para aplicar los cambios hechos
  * En este proceso de descubrio un bug en el codigo que ya se esta trabajando en la solucion, pero por el momento luego de hacer el primer apply de los cambios, se debera ejecutar otro apply en terraform cloud manualmente para poder aplicar los valores de `gw_method_setting` que el primer apply piso y los colo como vacios, en el segundo apply se aplican los valores correctos
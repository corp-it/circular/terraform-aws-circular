################role policy for LambdaS3FullAccess

resource "aws_iam_role" "downloadFileToS3" {
  name               = "Lambda-downloadFileToS3-role"
  assume_role_policy = data.aws_iam_policy_document.circular-downloadFileToS3-assume-role-policy.json
}

data "aws_iam_policy_document" "circular-downloadFileToS3-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

############################################

resource "aws_iam_role_policy_attachment" "Lambda-Basic-Execution-role-policy-attach" {
  role       = aws_iam_role.downloadFileToS3.name
  policy_arn = aws_iam_policy.LambdaBasic_Execution_Circular_policy.arn
}

resource "aws_iam_policy" "LambdaBasic_Execution_Circular_policy" {
  name   = "Lambda-Basic-Execution-Circular"
  policy = data.aws_iam_policy_document.LambdaBasic_Execution_Circular_policy.json
}

data "aws_iam_policy_document" "LambdaBasic_Execution_Circular_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "Logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/ProcessAppSyncNewMessage:*",
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/ProcessAppSyncNewMessageTest:*",
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/*:*",
    ]
  }
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "Logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}

#################################### Policies of AWS

resource "aws_iam_role_policy_attachment" "AWS-Lambda-role-policy-attach" {
  role       = aws_iam_role.downloadFileToS3.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}

resource "aws_iam_role_policy_attachment" "AWS-s3-full-access-policy-attach" {
  role       = aws_iam_role.downloadFileToS3.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}


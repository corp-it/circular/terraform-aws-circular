##################################Lambda Function cognitoResetUserPassword

module "cognitoResetUserPassword" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_4["function_name"]
  role              = aws_iam_role.CognitoCircularUserPool.arn
  memory_size       = var.lambda_params_4["memory_size"]
  handler           = var.lambda_params_4["handler"]
  runtime           = var.lambda_params_4["runtime"]
  timeout           = var.lambda_params_4["timeout"]
  mode              = var.lambda_params_4["mode"]
  s3_object_version = var.s3_obj_cognitoResetUserPassword
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_4["s3_key"]
  environment = {
    variables = {
      AwsRegion  = local.region
      UserPoolId = data.terraform_remote_state.circular_cognito.outputs.user_pool_id
    }
  }
}

##################################updateAvatarFromEC2

module "updateAvatarFromEC2" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_5["function_name"]
  role              = aws_iam_role.LambdaUpdateAvatarFromEC2.arn
  memory_size       = var.lambda_params_5["memory_size"]
  handler           = var.lambda_params_5["handler"]
  runtime           = var.lambda_params_5["runtime"]
  timeout           = var.lambda_params_5["timeout"]
  mode              = var.lambda_params_5["mode"]
  s3_object_version = var.s3_obj_updateAvatarFromEC2
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_5["s3_key"]
  environment = {
    variables = {
      destinationFolder           = "content/avatar/"
      usersTable                  = data.terraform_remote_state.circular_dynamodb.outputs.usersTable
      circularBucketPublicBaseUrl = var.lmbd_s3PublicRoot
      srcBucket                   = var.lmbd_srcBucket
      dstBucket                   = var.lmbd_dstBucket
      tmpFolder                   = "content/avatar-temporary-uploads/"
    }
  }
  layers = ["${data.terraform_remote_state.circular-gw-lambda.outputs.layer_Sharp_arn}:${data.terraform_remote_state.circular-gw-lambda.outputs.layer_Sharp_version}"]
}

#################################readOffer

module "readOffer" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_6["function_name"]
  role              = aws_iam_role.Circular-readOffer-Lambda-Role.arn
  memory_size       = var.lambda_params_6["memory_size"]
  handler           = var.lambda_params_6["handler"]
  runtime           = var.lambda_params_6["runtime"]
  timeout           = var.lambda_params_6["timeout"]
  mode              = var.lambda_params_6["mode"]
  s3_object_version = var.s3_obj_readOffer
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_6["s3_key"]
  environment = {
    variables = {
      AuthKey    = "aws:TmNKz3B79nWZNVhW"
      apiPathUrl = "/processes/readOffer"
      apiRootUrl = var.lmbd_apiRootUrl
    }
  }
}

#####################################notifyNewMessage

module "notifyNewMessage" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_7["function_name"]
  role              = aws_iam_role.Circular-notifyNewMessage-Lambda-Role.arn
  memory_size       = var.lambda_params_7["memory_size"]
  handler           = var.lambda_params_7["handler"]
  runtime           = var.lambda_params_7["runtime"]
  timeout           = var.lambda_params_7["timeout"]
  mode              = var.lambda_params_7["mode"]
  s3_object_version = var.s3_obj_notifyNewMessage
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_7["s3_key"]
  environment = {
    variables = {
      sendNotificationsLambda = data.terraform_remote_state.circular-actors-lambda.outputs.sendNotifications_id
      UsersConversationsTable = data.terraform_remote_state.circular_dynamodb.outputs.userConversationsTable
      UsersTable              = data.terraform_remote_state.circular_dynamodb.outputs.usersTable
    }
  }
}

####################################forwardMessagesToBot

module "forwardMessagesToBot" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_8["function_name"]
  role              = aws_iam_role.forwardMessagesToBot-Lambda-Role.arn
  memory_size       = var.lambda_params_8["memory_size"]
  handler           = var.lambda_params_8["handler"]
  runtime           = var.lambda_params_8["runtime"]
  timeout           = var.lambda_params_8["timeout"]
  mode              = var.lambda_params_8["mode"]
  s3_object_version = var.s3_obj_forwardMessagesToBot
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_8["s3_key"]
  environment = {
    variables = {
      ApiRootUrl                = var.lmbd_apiRootUrl
      ApiPathUrl                = "/processes/botResponder"
      AuthPass                  =	var.processAuthPass
      AuthUser                  =	var.processAurhUser
      CircularBot               = var.lmbd_circularBot
      UsersConversationsTable   = data.terraform_remote_state.circular_dynamodb.outputs.userConversationsTable
    }
  }
  layers = ["${module.qsLayer.layer_arn}:${module.qsLayer.version}","${module.axiosLayer.layer_arn}:${module.axiosLayer.version}"]
}

####################################updateRTOAndRUTA

module "updateRTOAndRUTA" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_9["function_name"]
  role              = aws_iam_role.Circular-LambdaRtoAndRutaUpdate-Role.arn
  memory_size       = var.lambda_params_9["memory_size"]
  handler           = var.lambda_params_9["handler"]
  runtime           = var.lambda_params_9["runtime"]
  timeout           = var.lambda_params_9["timeout"]
  mode              = var.lambda_params_9["mode"]
  s3_object_version = var.s3_obj_updateRTOAndRUTA
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_9["s3_key"]
  environment = {
    variables = {
      AWSRegion                = local.region
      CnrtApiMethod            = "https://api.cnrt.gob.ar/dut/v1/public/equipos?dominios="
      CnrtPdfMethod            = "https://api.cnrt.gob.ar/dut/v1/public/equiposPdf?dominios="
      DynamoUpdateRutaRtoTable = data.terraform_remote_state.circular_dynamodb.outputs.Updates-Doc-Rto-Ruta_id
      downloadToS3Lambda       = module.downloadFileToS3.id
      s3Bucket                 = var.lmbd_s3Bucket
      s3PublicRoot             = var.lmbd_s3PublicRoot
    }
  }
  layers        = ["${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_arn}:${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_version}"]
}


####################################downloadFileToS3

module "downloadFileToS3" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_11["function_name"]
  role              = aws_iam_role.downloadFileToS3.arn
  memory_size       = var.lambda_params_11["memory_size"]
  handler           = var.lambda_params_11["handler"]
  runtime           = var.lambda_params_11["runtime"]
  timeout           = var.lambda_params_11["timeout"]
  mode              = var.lambda_params_11["mode"]
  s3_object_version = var.s3_obj_downloadFileToS3
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_11["s3_key"]
}

####################################pullSTOPModifications

module "lambda-function-pullSTOPModifications" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_12["function_name"]
  role              = aws_iam_role.Lambda-pullSTOPModifications-role-policy.arn
  memory_size       = var.lambda_params_12["memory_size"]
  handler           = var.lambda_params_12["handler"]
  runtime           = var.lambda_params_12["runtime"]
  timeout           = var.lambda_params_12["timeout"]
  mode              = var.lambda_params_12["mode"]
  s3_object_version = var.s3_obj_pullSTOPModifications
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_12["s3_key"]
  environment = {
    variables = {
      ApiRootSTOP      = "https://www.ssta.gob.ar/turnos/v1.1.0/"
      DynamoDBRegion   = local.region
      StopUpdatesTable = data.terraform_remote_state.circular_dynamodb.outputs.STOP-updates_id
      ServicesHost = "services.appcircular.com"
      AuthPass =	var.processAuthPass
      AuthUser =	var.processAurhUser
    }
  }
  layers = ["${module.axiosLayer.layer_arn}:${module.axiosLayer.version}"]
}

####################################Lambda Function processMassMessagesQueue

module "processMassMessagesQueue" {
  source                         = "corpit-consulting-public/lambda-function-mod/aws"
  version                        = "v2.1.1"
  function_name                  = var.lambda_params_13["function_name"]
  role                           = aws_iam_role.Circular-LambdaProcessMassMessagesQueue-Role.arn
  memory_size                    = var.lambda_params_13["memory_size"]
  handler                        = var.lambda_params_13["handler"]
  runtime                        = var.lambda_params_13["runtime"]
  timeout                        = var.lambda_params_13["timeout"]
  mode                           = var.lambda_params_13["mode"]
  reserved_concurrent_executions = var.lambda_params_13["reserved_concurrent_executions"]
  s3_object_version              = var.s3_obj_processMassMessagesQueue
  s3_bucket                      = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key                         = var.lambda_params_13["s3_key"]
  environment = {
    variables = {
      AWSRegion     = local.region
      MessagesTable = data.terraform_remote_state.circular_dynamodb.outputs.messagesTable
    }
  }
  layers        = ["${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_arn}:${data.terraform_remote_state.circular-actors-lambda.outputs.layer_Uuid_version}"]
}

####################################Lambda Function processDataExtractionQueue

module "processDataExtractionQueue" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = var.lambda_params_14["function_name"]
  role              = aws_iam_role.Circular-LambdaProcessDataExtractionQueue-Role.arn
  memory_size       = var.lambda_params_14["memory_size"]
  handler           = var.lambda_params_14["handler"]
  runtime           = var.lambda_params_14["runtime"]
  timeout           = var.lambda_params_14["timeout"]
  mode              = var.lambda_params_14["mode"]
  s3_object_version = var.s3_obj_processDataExtractionQueue
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = var.lambda_params_14["s3_key"]
  environment = {
    variables = {
      ApiAuth                   = "aws:TmNKz3B79nWZNVhW"
      ApiRootUrl                = var.lmbd_apiRootUrl
      ApiPathUrl                = "/processes/dataExtraction/cartadeporte"
      DynamoExtractResultsTable = data.terraform_remote_state.circular_dynamodb.outputs.Data-Extraction-Results_id
      S3Bucket                  = var.private_permanent_s3Bucket
      S3InitKey                 = "cartas-de-porte/"
    }
  }
  layers        = ["${module.lambda_layer_circular_Pillow.layer_arn}:${module.lambda_layer_circular_Pillow.version}"]
}


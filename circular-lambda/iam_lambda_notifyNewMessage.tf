################role policy for Circular-notifyNewMessage-Lambda-Role

resource "aws_iam_role" "Circular-notifyNewMessage-Lambda-Role" {
  name               = "Circular-notifyNewMessage-Lambda-Rol-policy"
  assume_role_policy = data.aws_iam_policy_document.notifyNewMessage-Lambda-assume-role-policy.json
}

data "aws_iam_policy_document" "notifyNewMessage-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

#################################DynamoRead

resource "aws_iam_role_policy_attachment" "Circular-DynamoRead-role-policy" {
  role       = aws_iam_role.Circular-notifyNewMessage-Lambda-Role.name
  policy_arn = aws_iam_policy.Circular-DynamoRead-policy.arn
}

resource "aws_iam_policy" "Circular-DynamoRead-policy" {
  name   = "Circular-DynamoRead-policy"
  policy = data.aws_iam_policy_document.Circular-DynamoRead-policy.json
}

data "aws_iam_policy_document" "Circular-DynamoRead-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "dynamodb:BatchGetItem",
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:Query",
    ]
    resources = [
      "*",
    ]
  }
}

############################AWS-Lambda-Basic-ExecutionRole

resource "aws_iam_role_policy_attachment" "Lambda-Circular-Basic-ExecutionRole-role-policy" {
  role       = aws_iam_role.Circular-notifyNewMessage-Lambda-Role.name
  policy_arn = aws_iam_policy.Lambda-Circular-Basic-ExecutionRole-policy.arn
}

resource "aws_iam_policy" "Lambda-Circular-Basic-ExecutionRole-policy" {
  name   = "Lambda-Circular-Basic-ExecutionRole-policy"
  policy = data.aws_iam_policy_document.Lambda-Circular-Basic-Execution-policy.json
}

data "aws_iam_policy_document" "Lambda-Circular-Basic-Execution-policy" {
  statement {
    sid    = ""
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    sid    = ""
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}

###################################Policy od AWS 

resource "aws_iam_role_policy_attachment" "AWSLambdaInvocation-DynamoDB" {
  role       = aws_iam_role.Circular-notifyNewMessage-Lambda-Role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaInvocation-DynamoDB"
}


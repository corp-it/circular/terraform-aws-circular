####################################### 

variable "lambda_params_4" {
  type = map(string)
  default = {
    function_name     = "cognitoResetUserPassword"
    memory_size       = "128"
    handler           = "index.handler"
    runtime           = "nodejs12.x"
    timeout           = "3"
    mode              = "PassThrough"
    s3_key            = "cognitoResetUserPassword.zip"
    s3_object_version = "fmIiGBnXu.48f.HJTue0FOPfkPVO13A2"
  }
}

####################################### 

variable "lambda_params_5" {
  type = map(string)
  default = {
    function_name     = "updateAvatarFromEC2"
    memory_size       = "2048"
    handler           = "index.handler"
    runtime           = "nodejs12.x"
    timeout           = "3"
    mode              = "PassThrough"
    s3_key            = "updateAvatarFromEC2.zip"
    s3_object_version = "Cb_dZtDdUQDjBZejXvCHXOtqkTla0kMR"
  }
}

###########################################readOffer

variable "lambda_params_6" {
  type = map(string)
  default = {  
    function_name     = "readOffer"
    memory_size       = "128"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "3"
    mode              = "PassThrough"
    s3_bucket         = "circular-lambda-artifacts"
    s3_key            = "readOffer.zip"
    s3_object_version = "ZpkPefTXT1fyCC3rjiy.H_XPvYPtoEV0"
  }
}

###################################notifyNewMessage 

variable "lambda_params_7" {
  type = map(string)
  default = {
    function_name     = "notifyNewMessage"
    memory_size       = "1024"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "20"
    mode              = "PassThrough"
    s3_key            = "notifyNewMessage.zip"
    s3_object_version = "NZoYBJ4Eae2R1HPEx.XIHVAx3ucSdlI7"
  }
}

#################################################forwardMessagesToBot

variable "lambda_params_8" {
  type = map(string)
  default = {
    function_name     = "forwardMessagesToBot"
    memory_size       = "1024"
    handler           = "index.handler"
    source_code_hash  = "B59DLMEAupohw9mV+FSMAdamV8OyPcAXULGDy9zFjB8="
    runtime           = "nodejs14.x"
    timeout           = "59"
    mode              = "PassThrough"
    s3_key            = "forwardMessagesToBot.zip"
    s3_object_version = "qFbIy.AZM29jZ9teyhetknOIeQKoO6Kn"
  }
}

#################################################updateRTOAndRUTA 

variable "lambda_params_9" {
  type = map(string)
  default = {
    function_name     = "updateRTOAndRUTA"
    memory_size       = "128"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "20"
    mode              = "PassThrough"
    s3_key            = "updateRTOAndRUTA.zip"
    s3_object_version = "2r0RhoD2CkQWV.IlCF4ohHEscCojzFpq"
  }
}

#################################################downloadFileToS3

variable "lambda_params_11" {
  type = map(string)
  default = {
    function_name     = "downloadFileToS3"
    memory_size       = "128"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "30"
    mode              = "PassThrough"
    s3_key            = "downloadFileToS3.zip"
    s3_object_version = "mPdOSJDlKgIbn.qwWBx7tOSIn1PO.Jkl"
  }
}

#################################################pullSTOPModifications

variable "lambda_params_12" {
  type = map(string)
  default = {
    function_name     = "pullSTOPModifications"
    memory_size       = "1024"
    handler           = "index.handler"
    runtime           = "nodejs14.x"
    timeout           = "210"
    mode              = "PassThrough"
    s3_key            = "pullSTOPModifications.zip"
    s3_object_version = "IAuC45aOTBvdhkOtRi.zhTM2G9Z.8OjT"
  }
}

#################################################processMassMessagesQueue 

variable "lambda_params_13" {
  type = map(string)
  default = {
    function_name                  = "processMassMessagesQueue"
    memory_size                    = "1024"
    handler                        = "index.handler"
    runtime                        = "nodejs12.x"
    timeout                        = "90"
    mode                           = "PassThrough"
    reserved_concurrent_executions = "1"
    s3_key                         = "processMassMessagesQueue.zip"
    s3_object_version              = "83XPnbjwdY.RvweCgcQSGSITIsp14X7y"
  }
}

#################################################processDataExtractionQueue 

variable "lambda_params_14" {
  type = map(string)
  default = {
    function_name     = "processDataExtractionQueue"
    memory_size       = "256"
    handler           = "lambda_function.lambda_handler"
    runtime           = "python3.8"
    timeout           = "30"
    mode              = "PassThrough"
    s3_key            = "processDataExtractionQueue.zip"
    s3_object_version = "sEY3TmY19XDOIqFc1css5spYAcocdHIu"
  }
}


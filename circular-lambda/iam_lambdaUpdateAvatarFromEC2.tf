################role policy for Circular Basic execution

resource "aws_iam_role" "LambdaUpdateAvatarFromEC2" {
  name               = "LambdaUpdateAvatarFromEC2-role"
  assume_role_policy = data.aws_iam_policy_document.LambdaUpdateAvatarFromEC2_assume_role_policy.json
}

data "aws_iam_policy_document" "LambdaUpdateAvatarFromEC2_assume_role_policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

#########################CircularDynamoDBCRUD

resource "aws_iam_role_policy_attachment" "CircularDynamoDBCRUD-policy_attach" {
  role       = aws_iam_role.LambdaUpdateAvatarFromEC2.name
  policy_arn = aws_iam_policy.CircularDynamoDBCRUD-policy.arn
}

resource "aws_iam_policy" "CircularDynamoDBCRUD-policy" {
  name   = "CircularDynamoDBCRUD-policy"
  policy = data.aws_iam_policy_document.CircularDynamoDBCRUD_policy.json
}

data "aws_iam_policy_document" "CircularDynamoDBCRUD_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "dynamodb:BatchGetItem",
      "dynamodb:BatchWriteItem",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:Query",
      "dynamodb:UpdateItem",
    ]
    resources = [
      "*",
    ]
  }
}

#####################################################

resource "aws_iam_role_policy_attachment" "s3Fullaccess-policy_attach" {
  role       = aws_iam_role.LambdaUpdateAvatarFromEC2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "LambdaSQSQueueExecutionRole-policy_attach" {
  role       = aws_iam_role.LambdaUpdateAvatarFromEC2.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole"
}

resource "aws_iam_role_policy_attachment" "LambdaBasic-policy_attach" {
  role       = aws_iam_role.LambdaUpdateAvatarFromEC2.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}


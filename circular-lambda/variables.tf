variable "global_tags" {
  default = {
    "Owner" = "Cliente"
  }
}

variable "asg_global_tags" {
  default = [
    {
      "key"                 = "Owner"
      "value"               = "Cliente"
      "propagate_at_launch" = "true"
    },
  ]
}

variable "workspace_name_prefix" {
}

variable "s3_obj_cognitoResetUserPassword" {
}

variable "s3_obj_updateAvatarFromEC2" {
}

variable "s3_obj_readOffer" {
}

variable "s3_obj_notifyNewMessage" {
}

variable "s3_obj_forwardMessagesToBot" {
}

variable "s3_obj_updateRTOAndRUTA" {
}

variable "s3_obj_downloadFileToS3" {
}

variable "s3_obj_pullSTOPModifications" {
}

variable "s3_obj_processMassMessagesQueue" {
}

variable "s3_obj_processDataExtractionQueue" {
}

################Lambda variables

variable "lmbd_srcBucket" {
}

variable "lmbd_dstBucket" {
}

variable "lmbd_apiRootUrl" {
}

variable "lmbd_BackEndDomain" {
}

variable "lmbd_s3Bucket" {
}

variable "lmbd_S3Endpoint" {
}

variable "lmbd_s3PublicRoot" {
}

variable "lmbd_S3BucketLocation" {
}

variable "private_permanent_s3Bucket" {
}

variable "lmbd_circularBot" {
}

####Layers

variable "s3_obj_pillow" {
}
variable "processAurhUser" {
}
variable "processAuthPass" {
}


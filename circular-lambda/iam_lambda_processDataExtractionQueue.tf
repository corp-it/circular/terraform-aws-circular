################role policy for Lambda processDataExtractionQueue

resource "aws_iam_role" "Circular-LambdaProcessDataExtractionQueue-Role" {
  name               = "Circular-LambdaProcessDataExtractionQueue-Rol"
  assume_role_policy = data.aws_iam_policy_document.LambdaProcessDataExtractionQueue-assume-role-policy.json
}

data "aws_iam_policy_document" "LambdaProcessDataExtractionQueue-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

############################## Log Streams

resource "aws_iam_role_policy_attachment" "processDataExtractionQueueLogStreams-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaProcessDataExtractionQueue-Role.name
  policy_arn = aws_iam_policy.Circular-lambda-processDataExtractionQueue-logs-policy.arn
}

resource "aws_iam_policy" "Circular-lambda-processDataExtractionQueue-logs-policy" {
  name   = "Lambda_processDataExtractionQueue-log-streams-policy"
  policy = data.aws_iam_policy_document.Lambda_processDataExtractionQueue-log-streams-policy.json
}

data "aws_iam_policy_document" "Lambda_processDataExtractionQueue-log-streams-policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/processDataExtractionQueue:*",
    ]
  }
}

########################################Circular-DynamoDB

resource "aws_iam_role_policy_attachment" "processDataExtractionQueue-Dynamo-DB-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaProcessDataExtractionQueue-Role.name
  policy_arn = aws_iam_policy.Circular-lambda-processDataExtractionQueue-dynamodb-policy.arn
}

resource "aws_iam_policy" "Circular-lambda-processDataExtractionQueue-dynamodb-policy" {
  name   = "Lambda_processDataExtractionQueue-dynamoDb-policy"
  policy = data.aws_iam_policy_document.Lambda_processDataExtractionQueue-dynamoDb-policy.json
}

data "aws_iam_policy_document" "Lambda_processDataExtractionQueue-dynamoDb-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "dynamodb:PutItem",
    ]
    resources = [
      "arn:aws:dynamodb:us-east-1:${data.aws_caller_identity.current.account_id}:table/Data-Extraction-Results",
    ]
  }
}

########################################Textract

resource "aws_iam_role_policy_attachment" "processDataExtractionQueue-Textract-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaProcessDataExtractionQueue-Role.name
  policy_arn = aws_iam_policy.Circular-lambda-processDataExtractionQueue-textract-policy.arn
}

resource "aws_iam_policy" "Circular-lambda-processDataExtractionQueue-textract-policy" {
  name   = "Lambda_processDataExtractionQueue-textract-policy"
  policy = data.aws_iam_policy_document.Lambda_processDataExtractionQueue-textract-policy.json
}

data "aws_iam_policy_document" "Lambda_processDataExtractionQueue-textract-policy" {
  statement {
    effect = "Allow"
    actions = [
      "textract:DetectDocumentText",
    ]
    resources = [
      "*",
    ]
  }
}

#################################### Policies of AWS

resource "aws_iam_role_policy_attachment" "processDataExtractionQueueAWSLambdaSQSQueueExecutionRole-policy" {
  role       = aws_iam_role.Circular-LambdaProcessDataExtractionQueue-Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole"
}

resource "aws_iam_role_policy_attachment" "processDataExtractionQueueAmazonS3ReadOnlyAccess-policy" {
  role       = aws_iam_role.Circular-LambdaProcessDataExtractionQueue-Role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
}


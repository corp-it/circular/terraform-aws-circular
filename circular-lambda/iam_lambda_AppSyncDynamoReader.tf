################role policy for AppSyncDynamoReader

resource "aws_iam_role" "AppSyncDynamoReader" {
  name               = "lambda-AppSync-Dynamo-Reader-policy"
  assume_role_policy = data.aws_iam_policy_document.lambda_AppSyncDynamoReader_assume_role_policy.json
}

data "aws_iam_policy_document" "lambda_AppSyncDynamoReader_assume_role_policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

######################LambdaMicroserviceExecutionRole

resource "aws_iam_role_policy_attachment" "Lambda_Microservice_Execution_role_policy_attach" {
  role       = aws_iam_role.AppSyncDynamoReader.name
  policy_arn = aws_iam_policy.DynamoDBCircular-policy.arn
}

resource "aws_iam_policy" "DynamoDBCircular-policy" {
  name   = "Lambda_Microservice_Execution_Role"
  policy = data.aws_iam_policy_document.DynamoDBFullAccessCircular_policy.json
}

data "aws_iam_policy_document" "DynamoDBFullAccessCircular_policy" {
  statement {
    effect = "Allow"
    actions = [
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:Scan",
      "dynamodb:UpdateItem",
    ]
    resources = [
      "arn:aws:dynamodb:us-east-1:${data.aws_caller_identity.current.account_id}:table/*",
    ]
  }
}

######################LambdaBasicExecutionRole

resource "aws_iam_role_policy_attachment" "Lambda_Basic_ExecutionRole_role_policy_attach" {
  role       = aws_iam_role.AppSyncDynamoReader.name
  policy_arn = aws_iam_policy.Lambda_Basic_Execution_Role_Circular_policy.arn
}

resource "aws_iam_policy" "Lambda_Basic_Execution_Role_Circular_policy" {
  name   = "Lambda_Basic_Execution_Role"
  policy = data.aws_iam_policy_document.Circular_Lambda_Basic_Execution_Role_policy.json
}

data "aws_iam_policy_document" "Circular_Lambda_Basic_Execution_Role_policy" {
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}

####################Policies of AWS

resource "aws_iam_role_policy_attachment" "Amazon-DynamoDB-FullAccess-role-policy-attach" {
  role       = aws_iam_role.AppSyncDynamoReader.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "AWS-LambdaRole-role-policy-attach" {
  role       = aws_iam_role.AppSyncDynamoReader.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}


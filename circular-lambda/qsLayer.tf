

module "qsLayer" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v2.0.2"
  layer_name          = "QsModule"
  description         = "Qs for Node 14.x"
  s3_object_version   = var.s3_obj_qs
  s3_bucket           = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key              = "layers/qsModule.zip"
  compatible_runtimes = [
    "nodejs14.x",
  ]
}

variable "s3_obj_qs" {
}

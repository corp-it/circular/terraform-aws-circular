################role policy for Circular Basic execution

resource "aws_iam_role" "Circular_basic_execution" {
  name               = "Circular_basic_execution_rol"
  assume_role_policy = data.aws_iam_policy_document.lambda_Circular_basic_execution_rol_assume_role_policy.json
}

data "aws_iam_policy_document" "lambda_Circular_basic_execution_rol_assume_role_policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

#########################Cognito Adm Create User

resource "aws_iam_role_policy_attachment" "cognito_adm_create_user_policy_attach" {
  role       = aws_iam_role.Circular_basic_execution.name
  policy_arn = aws_iam_policy.cognito-adm-create-user-policy.arn
}

resource "aws_iam_policy" "cognito-adm-create-user-policy" {
  name   = "Cognito_Adm_Create_User_Circular_policy"
  policy = data.aws_iam_policy_document.Cognito_Adm_Create_User_Circular_policy.json
}

data "aws_iam_policy_document" "Cognito_Adm_Create_User_Circular_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "cognito-idp:AdminCreateUser",
    ]
    resources = [
      "*",
    ]
  }
}

#########################Lambda Invoke

resource "aws_iam_role_policy_attachment" "Lambda_Invoke_Circular_policy_attach" {
  role       = aws_iam_role.Circular_basic_execution.name
  policy_arn = aws_iam_policy.Lambda_Invoke_Circular_policy.arn
}

resource "aws_iam_policy" "Lambda_Invoke_Circular_policy" {
  name   = "Lambda_Invoke_Circular_policy"
  policy = data.aws_iam_policy_document.Lambda_Invoke_Circular_policy.json
}

data "aws_iam_policy_document" "Lambda_Invoke_Circular_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "lambda:InvokeFunction",
    ]
    resources = [
      "*",
    ]
  }
}

#########################Lambda Invoke

resource "aws_iam_role_policy_attachment" "Circular-oneClick_policy_attach" {
  role       = aws_iam_role.Circular_basic_execution.name
  policy_arn = aws_iam_policy.Circular-oneClick_policy.arn
}

resource "aws_iam_policy" "Circular-oneClick_policy" {
  name   = "Circular-oneClick_policy"
  policy = data.aws_iam_policy_document.Circular-oneClick-basic-execution-policy.json
}

data "aws_iam_policy_document" "Circular-oneClick-basic-execution-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "logs:PutLogEvents",
      "logs:CreateLogStream",
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:*:*:*",
    ]
  }
}


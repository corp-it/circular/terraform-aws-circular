#######################Pillow

module "lambda_layer_circular_Pillow" {
  source              = "corpit-consulting-public/lambda-layer-version-mod/aws"
  version             = "v2.0.2"
  layer_name          = var.layer_params_1["layer_name"]
  description         = var.layer_params_1["description"]
  s3_object_version   = var.s3_obj_pillow
  s3_bucket           = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key              = var.layer_params_1["s3_key"]
  compatible_runtimes = var.compatible_runtimes_1
}


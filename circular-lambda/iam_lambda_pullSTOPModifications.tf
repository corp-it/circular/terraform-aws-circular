################role policy for Lambda-pullSTOPModifications-assume-role-policy

resource "aws_iam_role" "Lambda-pullSTOPModifications-role-policy" {
  name               = "lambda-pullSTOPModifications-role"
  assume_role_policy = data.aws_iam_policy_document.Lambda-pullSTOPModifications-assume-role-policy.json
}

data "aws_iam_policy_document" "Lambda-pullSTOPModifications-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

############################################

resource "aws_iam_role_policy_attachment" "pullSTOPModifications-Lambda-DynamoCRUD-Circular-role-policy-attach" {
  role       = aws_iam_role.Lambda-pullSTOPModifications-role-policy.name
  policy_arn = aws_iam_policy.DynamoDB_Circular_policy_attach.arn
}

resource "aws_iam_policy" "DynamoDB_Circular_policy_attach" {
  name   = "DynamoDB_Circular_policy_attach"
  policy = data.aws_iam_policy_document.DynamoDB_Circular_json_policy.json
}

data "aws_iam_policy_document" "DynamoDB_Circular_json_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "dynamodb:UpdateItem",
      "dynamodb:Scan",
      "dynamodb:Query",
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
      "dynamodb:BatchWriteItem",
      "dynamodb:BatchGetItem",
    ]
    resources = [
      "*",
    ]
  }
}

##########################################

resource "aws_iam_role_policy_attachment" "pullSTOPModificationsLambdaBasicExecution-role-policy-attach" {
  role       = aws_iam_role.Lambda-pullSTOPModifications-role-policy.name
  policy_arn = aws_iam_policy.Circular-lambda-pullSTOPModifications-invoke-policy.arn
}

resource "aws_iam_policy" "Circular-lambda-pullSTOPModifications-invoke-policy" {
  name   = "Lambda-pullSTOPModifications-Invoke-Function-Circular-policy"
  policy = data.aws_iam_policy_document.pullSTOPModifications-Invoke-Function-Circular-policy.json
}

data "aws_iam_policy_document" "pullSTOPModifications-Invoke-Function-Circular-policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/pullSTOPModifications:*",
    ]
  }
}

##########################################

resource "aws_iam_role_policy_attachment" "Lambda-Basic-Execution-Circular-policy-attach" {
  role       = aws_iam_role.Lambda-pullSTOPModifications-role-policy.name
  policy_arn = aws_iam_policy.Circular-lambda-pullSTOPModifications-invoke-policy.arn
}

resource "aws_iam_policy" "Lambda-Basic-Execution-Circular-policy-attach" {
  name   = "Lambda-Basic-Circular-policy"
  policy = data.aws_iam_policy_document.Lambda-Basic-Execution-Circular-json-policy.json
}

data "aws_iam_policy_document" "Lambda-Basic-Execution-Circular-json-policy" {
  statement {
    sid    = ""
    effect = "Allow"
    actions = [
      "logs:PutLogEvents",
      "logs:CreateLogStream",
    ]
    resources = [
      "*",
    ]
  }
  statement {
    sid    = ""
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}


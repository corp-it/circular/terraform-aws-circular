data "terraform_remote_state" "circular_s3" {
  backend = "remote"
  config = {
    organization = "Circular-qa"
    workspaces = {
      name = "circular-s3-${var.workspace_name_prefix}"
    }
  }
}

data "terraform_remote_state" "circular-actors-lambda" {
  backend = "remote"
  config = {
    organization = "Circular-qa"
    workspaces = {
      name = "circular-actors-lambda-${var.workspace_name_prefix}"
    }
  }
}

data "terraform_remote_state" "circular-gw-lambda" {
  backend = "remote"
  config = {
    organization = "Circular-qa"
    workspaces = {
      name = "circular-gw-lambda-${var.workspace_name_prefix}"
    }
  }
}

data "terraform_remote_state" "circular_cognito" {
  backend = "remote"
  config = {
    organization = "Circular-qa"
    workspaces = {
      name = "circular-cognito-${var.workspace_name_prefix}"
    }
  }
}

data "terraform_remote_state" "circular_dynamodb" {
  backend = "remote"
  config = {
    organization = "Circular-qa"
    workspaces = {
      name = "circular-dynamodb-${var.workspace_name_prefix}"
    }
  }
}

data "terraform_remote_state" "circular_sqs" {
  backend = "remote"
  config = {
    organization = "Circular-qa"
    workspaces = {
      name = "circular-sqs-${var.workspace_name_prefix}"
    }
  }
}


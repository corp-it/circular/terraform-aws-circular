
module "updateLambdaArtifact" {
  source            = "corpit-consulting-public/lambda-function-mod/aws"
  version           = "v2.1.1"
  function_name     = "updateLambdaArtifact"
  role              = aws_iam_role.updateLambdaArtifactRole.arn
  memory_size       = "128"
  handler           = "index.handler"
  runtime           = "nodejs14.x"
  timeout           = "40"
  mode              = "PassThrough"
  s3_object_version = var.s3_obj_updateLambdaArtifact
  s3_bucket         = data.terraform_remote_state.circular_s3.outputs.s3_name
  s3_key            = "updateLambdaArtifact.zip"
  environment = {
    variables = {
      terraformApiUrl   = "https://app.terraform.io/"
      terraformApiKey   = var.terraformKey
      artifactBucket    = data.terraform_remote_state.circular_s3.outputs.s3_name
    }
  }
  layers = ["${module.axiosLayer.layer_arn}:${module.axiosLayer.version}"]
}


resource "aws_iam_role" "updateLambdaArtifactRole" {
  name               = "updateLambdaArtifactRole"
  assume_role_policy = data.aws_iam_policy_document.updateLambdaArtifactAssumeRolPolicy.json
}

data "aws_iam_policy_document" "updateLambdaArtifactAssumeRolPolicy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

resource "aws_iam_role_policy_attachment" "updateLambdaArtifactPolicy-role-policy-attach" {
  role       = aws_iam_role.updateLambdaArtifactRole.name
  policy_arn = aws_iam_policy.updateLambdaArtifactPolicy.arn
}

resource "aws_iam_policy" "updateLambdaArtifactPolicy" {
  name = "updateLambdaArtifactPolicy"
  policy = data.aws_iam_policy_document.updateLambdaArtifactPolicyDocument.json
}

data "aws_iam_policy_document" "updateLambdaArtifactPolicyDocument" {
  statement {
    effect = "Allow"
    actions = ["s3:Get*", "s3:List*"]    
    resources = ["*"]
  }
  statement {
    effect = "Allow"
    actions = ["logs:CreateLogStream", "logs:PutLogEvents"]
    resources = ["*"]
  }
  statement {
    effect = "Allow"
    actions = ["lambda:UpdateFunctionCode"]    
    resources = ["*"]
  }
}


variable "s3_obj_updateLambdaArtifact" {
}

variable "terraformKey" {
}

####################For forwardMessagesToBot

module "lambda_mapping_dynamodb_forwardMessagesToBot" {
  source            = "juanConversalab/lambda-event-source-mapping/aws"
  version           = "0.2.0"
  batch_size        = var.mapping_params_1["batch_size"]
  event_source_arn  = data.terraform_remote_state.circular_dynamodb.outputs.messagesTable_stream
  function_name     = module.forwardMessagesToBot.arn
  starting_position = var.mapping_params_1["starting_position"]
  enabled           = var.mapping_params_1["enabled"]
  has_dynamodb      = "true"
}
#####################For notifyNewMessage

module "lambda_mapping_dynamodb_notifyNewMessage" {
  source            = "juanConversalab/lambda-event-source-mapping/aws"
  version           = "0.2.0"
  batch_size        = var.mapping_params_2["batch_size"]
  event_source_arn  = data.terraform_remote_state.circular_dynamodb.outputs.messagesTable_stream
  function_name     = module.notifyNewMessage.arn
  starting_position = var.mapping_params_2["starting_position"]
  enabled           = var.mapping_params_2["enabled"]
  has_dynamodb      = "true"
}

#####################For readOffer

module "lambda_mapping_dynamodb_readOffer" {
  source            = "juanConversalab/lambda-event-source-mapping/aws"
  version           = "0.2.0"
  batch_size        = var.mapping_params_3["batch_size"]
  event_source_arn  = data.terraform_remote_state.circular_dynamodb.outputs.messagesTable_stream
  function_name     = module.readOffer.arn
  starting_position = var.mapping_params_3["starting_position"]
  enabled           = var.mapping_params_3["enabled"]
  has_dynamodb      = "true"
}

#####################updateRTOAndRUTA

module "lambda_updateRTOAndRUTA_sqs" {
  source            = "juanConversalab/lambda-event-source-mapping/aws"
  version           = "0.2.0"
  batch_size       = var.mapping_params_4["batch_size"]
  event_source_arn = data.terraform_remote_state.circular_sqs.outputs.RtoAndRutaUpdate_arn
  function_name    = module.updateRTOAndRUTA.id
  enabled          = var.mapping_params_4["enabled"]
  has_sqs          = "true"
}

#####################UpdateAvatarSQS

module "lambda_UpdateAvatar_sqs" {
  source            = "juanConversalab/lambda-event-source-mapping/aws"
  version           = "0.2.0"
  batch_size       = var.mapping_params_6["batch_size"]
  event_source_arn = data.terraform_remote_state.circular_sqs.outputs.UpdateAvatar_arn
  function_name    = module.updateAvatarFromEC2.id
  enabled          = var.mapping_params_6["enabled"]
  has_sqs          = "true"
}

#####################processMassMessagesQueue

module "lambda_processMassMessagesQueue_sqs" {
  source            = "juanConversalab/lambda-event-source-mapping/aws"
  version           = "0.2.0"
  batch_size       = var.mapping_params_7["batch_size"]
  event_source_arn = data.terraform_remote_state.circular_sqs.outputs.MassiveMessages_arn
  function_name    = module.processMassMessagesQueue.id
  enabled          = var.mapping_params_7["enabled"]
  has_sqs          = "true"
}

#####################processDataExtractionQueue

module "lambda_processDataExtractionQueue_sqs" {
  source            = "juanConversalab/lambda-event-source-mapping/aws"
  version           = "0.2.0"
  batch_size       = var.mapping_params_8["batch_size"]
  event_source_arn = data.terraform_remote_state.circular_sqs.outputs.DataExtraction_arn
  function_name    = module.processDataExtractionQueue.id
  enabled          = var.mapping_params_8["enabled"]
  has_sqs          = "true"
}
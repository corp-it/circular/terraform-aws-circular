#####################forwardMessagesToBot

variable "mapping_params_1" {
  type = map(string)
  default = {
    batch_size        = "100"
    starting_position = "LATEST"
    enabled           = "true"
  }
}

#####################notifyNewMessage

variable "mapping_params_2" {
  type = map(string)
  default = {
    batch_size        = "100"
    starting_position = "LATEST"
    enabled           = "true"
  }
}

#####################readOffer

variable "mapping_params_3" {
  type = map(string)
  default = {
    batch_size        = "100"
    starting_position = "LATEST"
    enabled           = "true"
  }
}

#####################updateRTOAndRUTA

variable "mapping_params_4" {
  type = map(string)
  default = {
    batch_size        = "1"
    enabled           = "true"
  }
}

#####################UpdateAvatarSQS

variable "mapping_params_6" {
  type = map(string)
  default = {
    batch_size        = "1"
    enabled           = "true"
  }
}

#####################processMassMessagesQueue

variable "mapping_params_7" {
  type = map(string)
  default = {
    batch_size        = "10"
    enabled           = "true"
  }
}

#####################processDataExtractionQueue

variable "mapping_params_8" {
  type = map(string)
  default = {
    batch_size        = "1"
    enabled           = "true"
  }
}
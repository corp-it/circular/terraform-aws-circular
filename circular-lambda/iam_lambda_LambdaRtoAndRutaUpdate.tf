################role policy for LambdaRtoAndRutaUpdate

resource "aws_iam_role" "Circular-LambdaRtoAndRutaUpdate-Role" {
  name               = "Circular-LambdaRtoAndRutaUpdate-Rol"
  assume_role_policy = data.aws_iam_policy_document.LambdaRtoAndRutaUpdate-assume-role-policy.json
}

data "aws_iam_policy_document" "LambdaRtoAndRutaUpdate-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

######################lambdaInvoke

resource "aws_iam_role_policy_attachment" "lambdaInvoke-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaRtoAndRutaUpdate-Role.name
  policy_arn = aws_iam_policy.lambdainvoke-Circular-policy.arn
}

resource "aws_iam_policy" "lambdainvoke-Circular-policy" {
  name   = "InvokeFunction-Circular-policy"
  policy = data.aws_iam_policy_document.InvokeFunction-Circular-policy.json
}

data "aws_iam_policy_document" "InvokeFunction-Circular-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "lambda:InvokeFunction",
    ]
    resources = [
      "*",
    ]
  }
}

##############################

resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecution-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaRtoAndRutaUpdate-Role.name
  policy_arn = aws_iam_policy.Circular-lambdainvoke-policy.arn
}

resource "aws_iam_policy" "Circular-lambdainvoke-policy" {
  name   = "Lambda_InvokeFunction-Circular-policy"
  policy = data.aws_iam_policy_document.Invoke-Function-Circular-policy.json
}

data "aws_iam_policy_document" "Invoke-Function-Circular-policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/updateAvatar:*",
    ]
  }
}

############################

resource "aws_iam_role_policy_attachment" "LambdaMicroserviceExecutionDynamodb-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaRtoAndRutaUpdate-Role.name
  policy_arn = aws_iam_policy.Lambda_Microservice_Execution-policy.arn
}

resource "aws_iam_policy" "Lambda_Microservice_Execution-policy" {
  name   = "Lambda_Microservice_Execution"
  policy = data.aws_iam_policy_document.Lambda_Microservice_Execution.json
}

data "aws_iam_policy_document" "Lambda_Microservice_Execution" {
  statement {
    effect = "Allow"
    actions = [
      "dynamodb:UpdateItem",
      "dynamodb:Scan",
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
    ]
    resources = [
      "arn:aws:dynamodb:us-east-1:${data.aws_caller_identity.current.account_id}:table/*",
    ]
  }
}

#############################policies of AWS

resource "aws_iam_role_policy_attachment" "AWSLambdaSQSQueueExecutionRole-policy" {
  role       = aws_iam_role.Circular-LambdaRtoAndRutaUpdate-Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole"
}


################role policy for forwardMessagesToBot

resource "aws_iam_role" "forwardMessagesToBot-Lambda-Role" {
  name               = "Circular-forwardMessagesToBot-Lambda-Rol-policy"
  assume_role_policy = data.aws_iam_policy_document.forwardMessagesToBot-Lambda-assume-role-policy.json
}

data "aws_iam_policy_document" "forwardMessagesToBot-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

#################################forwardMessagesToBot

resource "aws_iam_role_policy_attachment" "Circular-forwardMessagesToBot-role-policy" {
  role       = aws_iam_role.forwardMessagesToBot-Lambda-Role.name
  policy_arn = aws_iam_policy.Circular-forwardMessagesToBot-policy.arn
}

resource "aws_iam_policy" "Circular-forwardMessagesToBot-policy" {
  name   = "Circular-forwardMessagesToBot-policy-policy"
  policy = data.aws_iam_policy_document.Circular-forwardMessagesToBot-policy.json
}

data "aws_iam_policy_document" "Circular-forwardMessagesToBot-policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/forwardMessagesToBot:*",
    ]
  }
}

#################################forwardMessagesToBot-DynamoRead

resource "aws_iam_role_policy_attachment" "Circular-forwardMessagesToBot-DynamoRead-role-policy" {
  role       = aws_iam_role.forwardMessagesToBot-Lambda-Role.name
  policy_arn = aws_iam_policy.Circular-forwardMessagesToBot-DynamoRead-policy.arn
}

resource "aws_iam_policy" "Circular-forwardMessagesToBot-DynamoRead-policy" {
  name   = "Circular-forwardMessagesToBot-DynamoRead-policy-policy"
  policy = data.aws_iam_policy_document.Circular-forwardMessagesToBot-DynamoRead-policy.json
}

data "aws_iam_policy_document" "Circular-forwardMessagesToBot-DynamoRead-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "dynamodb:Query",
    ]
    resources = [
      "*",
    ]
  }
}

##################################Policy of AWS

resource "aws_iam_role_policy_attachment" "Circular-AWSLambdaInvocation-DynamoDB-role-policy" {
  role       = aws_iam_role.forwardMessagesToBot-Lambda-Role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaInvocation-DynamoDB"
}


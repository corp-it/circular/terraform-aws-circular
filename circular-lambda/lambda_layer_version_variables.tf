###########################Pillow

variable "layer_params_1" {
  type = map(string)
  default = {
    layer_name  = "Pillow"
    description = "Pillow for Python v7.2.0"
    s3_bucket   = "circular-lambda-artifacts"
    s3_key      = "layers/Pillow.zip"
  }
}

variable "compatible_runtimes_1" {
  type = list(string)
  default = [
    "python3.6",
    "python3.7",
    "python3.8",
  ]
}


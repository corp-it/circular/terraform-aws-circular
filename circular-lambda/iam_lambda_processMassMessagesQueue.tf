################role policy for Lambda processMassMessagesQueue

resource "aws_iam_role" "Circular-LambdaProcessMassMessagesQueue-Role" {
  name               = "Circular-LambdaProcessMassMessagesQueue-Rol"
  assume_role_policy = data.aws_iam_policy_document.LambdaProcessMassMessagesQueue-assume-role-policy.json
}

data "aws_iam_policy_document" "LambdaProcessMassMessagesQueue-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

############################## Log Streams

resource "aws_iam_role_policy_attachment" "processMassMessagesQueueLogStreams-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaProcessMassMessagesQueue-Role.name
  policy_arn = aws_iam_policy.Circular-lambda-processMassMessagesQueue-logs-policy.arn
}

resource "aws_iam_policy" "Circular-lambda-processMassMessagesQueue-logs-policy" {
  name   = "Lambda_processMassMessagesQueue-log-streams-policy"
  policy = data.aws_iam_policy_document.Lambda_processMassMessagesQueue-log-streams-policy.json
}

data "aws_iam_policy_document" "Lambda_processMassMessagesQueue-log-streams-policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/processMassMessagesQueue:*",
    ]
  }
}

########################################Circular-DynamoDB

resource "aws_iam_role_policy_attachment" "processMassMessagesQueue-Dynamo-DB-role-policy-attach" {
  role       = aws_iam_role.Circular-LambdaProcessMassMessagesQueue-Role.name
  policy_arn = aws_iam_policy.Circular-lambda-processMassMessagesQueue-dynamodb-policy.arn
}

resource "aws_iam_policy" "Circular-lambda-processMassMessagesQueue-dynamodb-policy" {
  name   = "Lambda_processMassMessagesQueue-dynamoDb-policy"
  policy = data.aws_iam_policy_document.Lambda_processMassMessagesQueue-dynamoDb-policy.json
}

data "aws_iam_policy_document" "Lambda_processMassMessagesQueue-dynamoDb-policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "dynamodb:BatchWriteItem",
    ]
    resources = [
      "*",
    ]
  }
}

#############################policies of AWS
resource "aws_iam_role_policy_attachment" "processMassMessagesQueueAWSLambdaSQSQueueExecutionRole-policy" {
  role       = aws_iam_role.Circular-LambdaProcessMassMessagesQueue-Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole"
}


################role policy for readOffer

resource "aws_iam_role" "Circular-readOffer-Lambda-Role" {
  name               = "Circular-readOffer-Lambda-Rol-policy"
  assume_role_policy = data.aws_iam_policy_document.readOffer-Lambda-assume-role-policy.json
}

data "aws_iam_policy_document" "readOffer-Lambda-assume-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

###########################Lambda-Basic-Execution-Role-readOffer

resource "aws_iam_role_policy_attachment" "Circular-readOffer-role-policy" {
  role       = aws_iam_role.Circular-readOffer-Lambda-Role.name
  policy_arn = aws_iam_policy.Circular-readOffer-policy.arn
}

resource "aws_iam_policy" "Circular-readOffer-policy" {
  name   = "Circular-readOffer-policy"
  policy = data.aws_iam_policy_document.Circular-readOffer-policy.json
}

data "aws_iam_policy_document" "Circular-readOffer-policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/readOffer:*",
    ]
  }
}

###################################Policy od AWS 

resource "aws_iam_role_policy_attachment" "AWSLambdaInvocation-DynamoDB-attach" {
  role       = aws_iam_role.Circular-readOffer-Lambda-Role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaInvocation-DynamoDB"
}


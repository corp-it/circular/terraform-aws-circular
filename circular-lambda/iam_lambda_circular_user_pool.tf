################role policy for Circular cognito user pool

resource "aws_iam_role" "CognitoCircularUserPool" {
  name               = "CognitoCircularUserPool-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_AppSyncDynamoReader_assume_role_policy.json
}

data "aws_iam_policy_document" "CognitoCircularUserPool_assume_role_policy" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

############################## Cognito User Pool Set Password

resource "aws_iam_role_policy_attachment" "Cognito_User_Pool_set_Password_role_policy_attach" {
  role       = aws_iam_role.CognitoCircularUserPool.name
  policy_arn = aws_iam_policy.CognitoUserPoolsetPassword-policy.arn
}

resource "aws_iam_policy" "CognitoUserPoolsetPassword-policy" {
  name   = "Cognito_User_Pool_set_Password_policy"
  policy = data.aws_iam_policy_document.Cognito_User_Pool_set_Password_policy.json
}

data "aws_iam_policy_document" "Cognito_User_Pool_set_Password_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "cognito-idp:AdminSetUserPassword",
    ]
    # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
    # force an interpolation expression to be interpreted as a list by wrapping it
    # in an extra set of list brackets. That form was supported for compatibility in
    # v0.11, but is no longer supported in Terraform v0.12.
    #
    # If the expression in the following list itself returns a list, remove the
    # brackets to avoid interpretation as a list of lists. If the expression
    # returns a single list item then leave it as-is and remove this TODO comment.
    resources = [
      data.terraform_remote_state.circular_cognito.outputs.user_pool_arn,
    ]
  }
}

################################## BasicExecutionCircularLambda

resource "aws_iam_role_policy_attachment" "BasicExecutionCircularLambda_role_policy_attach" {
  role       = aws_iam_role.CognitoCircularUserPool.name
  policy_arn = aws_iam_policy.BasicExecutionCircularLambda-policy.arn
}

resource "aws_iam_policy" "BasicExecutionCircularLambda-policy" {
  name   = "BasicExecutionCircularLambda_policy"
  policy = data.aws_iam_policy_document.BasicExecutionCircularLambda_policy.json
}

data "aws_iam_policy_document" "BasicExecutionCircularLambda_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/ProcessAppSyncNewMessage:*",
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/ProcessAppSyncNewMessageTest:*",
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/*:*",
    ]
  }
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
    ]
    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}

####################################CognitoCircularUserPool

resource "aws_iam_role_policy_attachment" "Cognito_Circular_User_Pool_role_policy_attach" {
  role       = aws_iam_role.CognitoCircularUserPool.name
  policy_arn = aws_iam_policy.Cognito-Circular-User-Pool-policy.arn
}

resource "aws_iam_policy" "Cognito-Circular-User-Pool-policy" {
  name   = "CognitoCircularUserPool_policy"
  policy = data.aws_iam_policy_document.Cognito-Circular-User-Pool_policy.json
}

data "aws_iam_policy_document" "Cognito-Circular-User-Pool_policy" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    actions = [
      "cognito-idp:AdminGetDevice",
      "cognito-idp:AdminCreateUser",
      "cognito-idp:ListIdentityProviders",
      "cognito-idp:AdminSetUserSettings",
      "cognito-idp:GetIdentityProviderByIdentifier",
      "cognito-idp:GetUICustomization",
      "cognito-idp:AdminGetUser",
      "cognito-idp:ListUserPoolClients",
      "cognito-idp:ListUsersInGroup",
      "cognito-idp:DescribeUserPool",
      "cognito-idp:AdminListUserAuthEvents",
      "cognito-idp:ListGroups",
      "cognito-idp:ListResourceServers",
      "cognito-idp:AdminListDevices",
      "cognito-idp:DescribeIdentityProvider",
      "cognito-idp:DescribeResourceServer",
      "cognito-idp:GetUserAttributeVerificationCode",
      "cognito-idp:DescribeUserImportJob",
      "cognito-idp:DescribeUserPoolClient",
      "cognito-idp:GetSigningCertificate",
      "cognito-idp:GetUser",
      "cognito-idp:SetUserSettings",
      "cognito-idp:GetCSVHeader",
      "cognito-idp:ListDevices",
      "cognito-idp:GetUserPoolMfaConfig",
      "cognito-idp:GetGroup",
      "cognito-idp:DescribeRiskConfiguration",
      "cognito-idp:GetDevice",
      "cognito-idp:UpdateUserAttributes",
      "cognito-idp:DescribeUserPoolDomain",
      "cognito-idp:ChangePassword",
      "cognito-idp:AdminListGroupsForUser",
      "cognito-idp:ListUserImportJobs",
      "cognito-idp:ListUsers",
    ]
    # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
    # force an interpolation expression to be interpreted as a list by wrapping it
    # in an extra set of list brackets. That form was supported for compatibility in
    # v0.11, but is no longer supported in Terraform v0.12.
    #
    # If the expression in the following list itself returns a list, remove the
    # brackets to avoid interpretation as a list of lists. If the expression
    # returns a single list item then leave it as-is and remove this TODO comment.
    resources = [
      data.terraform_remote_state.circular_cognito.outputs.user_pool_arn,
    ]
  }
  statement {
    sid    = "VisualEditor1"
    effect = "Allow"
    actions = [
      "cognito-idp:ListUserPools",
    ]
    resources = [
      "*",
    ]
  }
}

###############################

resource "aws_iam_role_policy_attachment" "AWSLambdaRole" {
  role       = aws_iam_role.CognitoCircularUserPool.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}


# terraform-aws-circular

### Descripcion

En este documento vamos a explicar paso por paso la configuracion de los casos de usos mas comunes para los desarrolladores y su implementacion con Terraform Cloud, tambien los requisitos necesarios.

## Pre-Requisitos

* Para comenzar a usar Terraform Cloud, necesita una cuenta de [Terraform Cloud](https://app.terraform.io/signup/account).

* Seguir los pasos para configurar Tf-Cloud en [TF-Cloud-config](./documentation/tf_cloud_config.md) Es otra seccion de este `README.md` 

### Estructuras de Repositorios

* Utilizamos una estructura de git flow en lugar de un solo branch `master`, este flujo de trabajo usa dos branches para registrar el historial del proyecto.
* Se recomienda crear un branch para cada cambio que se desea implementar en el codigo, ingrese en [Estructura de git](./documentation/estructura_git.md)

### Estructuras de directorios 

* Utilizamos un sistema de directorios en el cual los separamos por servicios para facilitar la division de workspace en terraform cloud
* tambien se dividieron las funciones lambdas y tablas dynamodb por micro-servicio ej: 
    * api gw utiliza lambdas para interpolar en su swagger file por medio de los stage
    * el mismo sistema de directorios se utiliza en el versionador de codigo
* Con la estructuras de directorios que usamos, dividimos los workspace por servicios y utilizamos un archivo `data.tf` con la configuracion de un remote backend para poder interpolar los servicios ente si.
   * Ejemplo de `data.tf`

```hcl

data "terraform_remote_state" "circular_vpc" {
  backend = "remote"
  config = {
    organization = "Circular-qa"
    workspaces   = {
      name = "circular-vpc-${var.workspace_name_prefix}"
    }
  }
}


```
* Si se desea agregar otro bloque data, solo debe copiar el bloque y cambiar las configuraciones con las configuraciones del servicio que se desea interpolar
* se debe agregar un output del servicio que desea utilizar al archivo `output.tf` 
* Ejemplo:
`output.tf`

```hcl
output "vpc_id" {
  value = "${module.circular-vpc.id}"
}
```

* La forma de interpolar el data es :

`vpc_id = "${data.terraform_remote_state.circular_vpc.user_pool_id}"`
* Segun el output o modulo que se desea utilizar

## Caso de uso

* Para cada cambios en los servicios se debe de crear un nuevo feature brach para pushear los nuevos cambios.
* Despues de haber subido los cambios, crear un Merge Request y seleccionar a uno o mas usuarios como review para aprobar el Merge Request.
* Los siguientes casos de uso muestran como agregar nuevos recursos o modificar recursos existente.
  * Caso de uso para [Lambdas](./documentation/add_and_mod_lambda.md)
  * Caso de uso para [Dynamodb](./documentation/add_and_mod_dynamodb.md)
  * Caso de uso para [Api Gateway](./documentation/add_and_mod_api_gw.md)
  * Caso de uso para [AppSync](./documentation/add_and_mod_appsync.md)
  * Caso de uso para [SQS](./documentation/add_and_mod_sqs.md)
  * Caso de uso para [Lambda Mapping Trigger](./documentation/add_and_mod_lambda_mapping.md)
  * Caso de uso cambio de [AMI ID ASG](./documentation/change_ami_id.md)
#######################RDS for Circular
module "aurora_db" {
  source                          = "corpit-consulting-public/aurora/aws"
  version                         = "v3.3.3"
  name                            = "${var.aurora_params["name"]}"
  envname                         = "${var.aurora_params["envname"]}"
  envtype                         = "${var.aurora_params["envtype"]}"
  snapshot_identifier             = "${var.snapshot_identifier}"
  subnets                         = "${data.terraform_remote_state.circular_vpc.vpc_private_subnets}"
  azs                             = "${var.aurora_azs}"
  replica_count                   = "${var.replica_count}"
  security_groups                 = ["${module.sg_aurora.this_security_group_id}"]
  instance_type                   = "${var.instance_type}"
  enabled                         = "${var.aurora_params["enabled"]}"
  username                        = "${var.username}"
  password                        = "${var.password}"
  skip_final_snapshot             = "${var.aurora_params["skip_final_snapshot"]}"
  storage_encrypted               = "${var.storage_encrypted}"
  apply_immediately               = "${var.aurora_params["apply_immediately"]}"
  monitoring_interval             = "${var.aurora_params["monitoring_interval"]}"
  db_parameter_group_name         = "${aws_db_parameter_group.aurora_db_56_parameter_group.id}"
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.aurora_cluster_56_parameter_group.id}"
  tags                            = "${var.aurora_tags}"
  iam_roles                       = ["${aws_iam_role.AuroraQueryIntoS3.arn}"]
}

resource "aws_db_parameter_group" "aurora_db_56_parameter_group" {
  name         = "circular-parameter-group-aurora-db"
  family       = "aurora5.6"
} 

resource "aws_rds_cluster_parameter_group" "aurora_cluster_56_parameter_group" {
  name   = "circular-aurora-cluster-parameter-group"
  family = "aurora5.6"
  parameter {
    name  = "aurora_select_into_s3_role"
    value = "${aws_iam_role.AuroraQueryIntoS3.arn}"
    apply_method = "immediate"
  }
}


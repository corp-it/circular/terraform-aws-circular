variable sg_aurora_params {
    type = "map"
    default = {
      name        = "aurora-sg"
      description = "security group for aurora"
    }
}

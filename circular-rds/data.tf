data "terraform_remote_state" "circular_vpc" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-vpc-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "circular_ec2" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "circular-ec2-${var.workspace_name_prefix}"
       }
    }
}

data "terraform_remote_state" "phpMyAdmin" {
    backend = "remote"
    config = {
       organization = "Circular-qa"
       workspaces   = {
          name = "phpMyAdmin-ec2-${var.workspace_name_prefix}"
       }
    }
}


module sg_aurora {
  source      = "corpit-consulting-public/security-group/aws"
  version     = "v2.17.0"
  name        = "${var.sg_aurora_params["name"]}"
  description = "${var.sg_aurora_params["description"]}"
  vpc_id      = "${data.terraform_remote_state.circular_vpc.vpc_id}"
  computed_ingress_with_source_security_group_id = [
    {
      source_security_group_id = "${data.terraform_remote_state.circular_ec2.asg_sg}"
      rule = "mysql-tcp"
    },{ 
      source_security_group_id = "${data.terraform_remote_state.phpMyAdmin.php_sg_id}"
      rule = "mysql-tcp"
    }   
  ]
  number_of_computed_ingress_with_source_security_group_id = 2
}

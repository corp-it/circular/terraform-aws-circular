variable global_tags {
    default = {
      "Owner" = "Cliente"
    }
}

variable asg_global_tags {
    default = [
      {
        "key" = "Owner"
        "value" = "Cliente"
        "propagate_at_launch" = "true"
      },
    ]
}

variable "workspace_name_prefix" {
}

variable "password" {
}

variable "username"{
}

variable "snapshot_identifier" {
}

variable "instance_type" {
}

variable "replica_count" {
}

variable "storage_encrypted" {
}

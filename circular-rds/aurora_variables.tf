######################RDS for Circular
variable aurora_params {
  type    = "map"
  default = {
    name                      = "circular_db"
    envname                   = "circular-db"
    envtype                   = "test"
    replica_count             = "1"
    instance_type             = "db.t3.medium"
    skip_final_snapshot       = "true"
#    storage_encrypted         = "false"
    apply_immediately         = "true"
    monitoring_interval       = "10"
    enabled                   = "true"
    snapshot_identifier       = "arn:aws:rds:us-east-1:294510482337:snapshot:circular-migracion"
  }
}

##variable "aurora_azs" {
##    description = "List of supported AZs"
##    type = "list"
##    default = ["us-east-1a", "us-east-1b", "us-east-1c"]
##}

variable "aurora_azs" {
    description = "List of supported AZs"
    type = "list"
}

variable "aurora_tags" {
  type    = "map"
  default = {
      "Business"      = "Conversalab",
      "workload-type" = "other",
  }
}

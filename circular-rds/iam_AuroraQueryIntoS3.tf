resource "aws_iam_role" "AuroraQueryIntoS3" {
  name = "AuroraQueryIntoS3"
  description = "Permite a Aurora hacer SELECT INTO OUTFILE S3"
  assume_role_policy = "${data.aws_iam_policy_document.AuroraQueryIntoS3_assume_role_policy.json}"
}

data "aws_iam_policy_document" "AuroraQueryIntoS3_assume_role_policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = [
        "rds.amazonaws.com",
      ]
    }
    actions = [
      "sts:AssumeRole",
    ]

  }
}

#####################
resource "aws_iam_role_policy_attachment" "AuroraSelectIntoS3-policy_attach" {
  role       = "${aws_iam_role.AuroraQueryIntoS3.name}"
  policy_arn = "${aws_iam_policy.AuroraSelectIntoS3-policy.arn}"
}

resource "aws_iam_policy" "AuroraSelectIntoS3-policy" {
  name    = "AuroraSelectIntoS3"
  policy  = "${data.aws_iam_policy_document.AuroraSelectIntoS3_policy.json}"
  description = "Permisos para que la DB aurora de Circular pueda ejecutar queries que se guarden en S3"
}

data "aws_iam_policy_document" "AuroraSelectIntoS3_policy" {
  statement {
    sid     = "VisualEditor0"
    effect  = "Allow"
    actions = [
             "s3:PutObject",
             "s3:GetObject",
             "s3:AbortMultipartUpload",
             "s3:ListBucket",
             "s3:DeleteObject",
             "s3:ListMultipartUploadParts"
    ]
  resources = [
          "arn:aws:s3:::*/*",
          "arn:aws:s3:::temporary-uploads-circular-prod"
  ]
 }
}



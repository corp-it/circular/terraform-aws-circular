# Terraform AWS Project Manager Template

This repository is a template that anybody can use to manage AWS Projects using Terraform. Read more about Terraform in here: <https://www.terraform.io/intro/index.html>

## Terraform Backend

We are using S3 as backend. Read more about Terraform Backends in here: <https://www.terraform.io/docs/backends/index.html>
In order to use this template there are some resources that you need to create in advance:

* S3 bucket - This will be used to store the terraform 'state' file. Suggested naming convention: `tf-<profile_name>-state`
* Dynamedb Table - This will be used for state locking. Suggested naming convention: `tf-<profile>-state-lock`

### Backend Required Resources Setup

After configuring your AWS credentials, use the AWS CLI to run these commands:

#### Create Versioned S3 Bucket

```bash
aws s3api create-bucket --bucket tf-<profile_name>-state --region us-east-1 --profile <aws_credentials>
aws s3api put-bucket-versioning --bucket tf-<projec_name>-state --versioning-configuration Status=Enabled --profile <aws_credentials>
```

#### Create DynamoDB Table

```bash
aws dynamodb create-table --table-name tf-<project_name>-state-lock --attribute-definitions AttributeName=LockID,AttributeType=S --key-schema AttributeName=LockID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=10 --profile <aws_credentials> --region <region>
```

## Setup
This AWS Manager assumes that you have AWS credentials setup.
Terraform Workspaces are used to define local variables, allowing developers to have different workspaces with different variables sets for example by environment.

#### locals.tf

```hcl
locals {
    ## Environment name taken out of Workspace name.
    env="${terraform.workspace}"

    ## Local Variables defined per Workspace
    local_variables = {
        ## AWS Profile that will be used to provision all Terraform resources.
        profile = "profile_name"
        ## Default Workspace
        default = {
            ## Region where the Terraform resources will be created
            region = "us-east-1"
        }
    }

    ## Global variables definition based on workspace.

    ## Profile
    profile = "${local.local_variables.profile}"

    ## Workspace Variables
    region = "${lookup(local.local_variables[local.env],"region")}"

    global_tags = {
        project = "example-project-name"
        finance = "included"
    }
}
```

#### config.tf

```hcl
terraform {
    backend "s3" {
        bucket = "myBucketName"
        ## Path where the state files will be stored
        key = "path/to/my/key"
        ## Region where the backend resources lives. This is NOT the region where the resources will be created.
        region = "us-east-1"
        dynamodb_table = "myDynamedbTable"
        profile = "${local.profile}"
    }
}

provider "aws" {
    region = "${local.region}"
    profile = "${local.profile}"
}
```
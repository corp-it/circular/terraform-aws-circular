variable global_tags {
    default = {
      "Owner" = "Cliente"
    }
}

variable asg_global_tags {
    default = [
      {
        "key" = "Owner"
        "value" = "Cliente"
        "propagate_at_launch" = "true"
      },
    ]
}

## These variables will be consumed out of Terraform Cloud Workspace Variables

variable "workspace_name_prefix" {
}

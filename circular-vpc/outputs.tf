output "vpc_id" {
  value = "${module.main_vpc.vpc_id}"
}

output "vpc_private_subnets" {
  value = "${module.main_vpc.private_subnets}"
}

output "vpc_public_subnets" {
  value = "${module.main_vpc.public_subnets}"
}

output "vpc_azs" {
  value = "${module.main_vpc.azs}"
}

output "vpc_public_subnets_cidr_blocks" {
  value = "${module.main_vpc.public_subnets_cidr_blocks}"
}

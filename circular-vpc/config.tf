terraform {
    required_version = ">= 0.11.14"
    backend "remote" {
        hostname = "app.terraform.io"
        organization = "Circular-qa"
        workspaces {
            prefix = "circular-vpc-"
        }
    }    
}

provider "aws" {
    version = "v2.70.0"
    region  = "${local.region}"
   # region  = "us-east-1"
   # profile = "tfcloud-dev"
}

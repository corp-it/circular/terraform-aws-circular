variable main_vpc_params {
    description = "Parameters for setting up Example VPC"
    type = "map"
    default = {
        name_prefix = "circular-vpc-"
        cidr = "172.30.0.0/16"
       
        enable_nat_gateway = true
        enable_vpn_gateway = false
        enable_dns_hostnames = true
	
	      single_nat_gateway = true

        map_public_ip_on_launch = false

    }
}

variable main_vpc_azs {
    description = "List of supported AZs"
    type = "list"
    default = ["us-east-1a", "us-east-1b"]
}

variable main_vpc_subnet_cidrs {
    description = "CIDR blocks assigned for Public and Prive Subnets"
    default = { 
       private_subnets = ["172.30.1.0/24","172.30.3.0/24"]
       public_subnets  = ["172.30.0.0/24","172.30.2.0/24"]
    }
}

variable main_vpc_tags {
    description = "Tags assigned to main VPC"
    type = "map"
    default = {
        Name = "circular-VPC"
    }
}

variable main_vpc_resource_tags {
    description = "Maps of tags to be used in VPC related resources"
    default    = {
        public_subnet_tags = {
            Name = "Public subnet"
        }
        public_route_table_tags = {
            Name = "public"
        }
        private_subnet_tags = {
            Name = "Private subnet"
        }
        private_route_table_tags = {
            Name = "private"
        }
        igw_tags = {
            Name = "Transit IGW"
        }
    }
}

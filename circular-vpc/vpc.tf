module "main_vpc" {
    source                   = "terraform-aws-modules/vpc/aws"
    version                  = "v1.59.0"
    name                     = "${var.main_vpc_params["name_prefix"]}${local.env}"
    cidr                     = "${var.main_vpc_params["cidr"]}"

    azs                      = "${var.main_vpc_azs}"

    public_subnets           = "${var.main_vpc_subnet_cidrs["public_subnets"]}"
    public_subnet_tags       = "${var.main_vpc_resource_tags["public_subnet_tags"]}"
    public_route_table_tags  = "${var.main_vpc_resource_tags["public_route_table_tags"]}"

    private_subnets          = "${var.main_vpc_subnet_cidrs["private_subnets"]}"
    private_subnet_tags      = "${var.main_vpc_resource_tags["private_subnet_tags"]}"
    private_route_table_tags = "${var.main_vpc_resource_tags["private_route_table_tags"]}"

    enable_nat_gateway       = "${var.main_vpc_params["enable_nat_gateway"]}"
   # enable_vpn_gateway      = "${var.main_vpc_params["enable_vpn_gateway"]}"
    enable_dns_hostnames     = "${var.main_vpc_params["enable_dns_hostnames"]}"
    single_nat_gateway       = "${var.main_vpc_params["single_nat_gateway"]}"
	

   # map_public_ip_on_launch = "${var.main_vpc_params["map_public_ip_on_launch"]}"

   # igw_tags                = "${var.main_vpc_resource_tags["igw_tags"]}"

   # tags = "${merge(local.global_tags,var.main_vpc_tags)}"
}
